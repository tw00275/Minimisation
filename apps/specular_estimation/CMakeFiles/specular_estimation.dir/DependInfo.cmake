# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/PFMAccess.cpp" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/PFMAccess.cpp.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/PhotometricStereo.cc" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/PhotometricStereo.cc.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/objloader.cpp" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/objloader.cpp.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/shader.cpp" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/shader.cpp.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/specular_estimation.cc" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/specular_estimation.cc.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/texture.cpp" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/texture.cpp.o"
  "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/vboindexer.cpp" "/home/thomas/Documents/Minimisation/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/vboindexer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GFLAGS_IS_A_DLL=0"
  "GLFW_DLL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "/usr/include/OpenEXR"
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
