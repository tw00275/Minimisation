#pragma once
#ifndef CERES_H
#define CERES_H

//#include "/user/HS222/tw00275/ceres-bin/include/ceres/ceres.h"
#include "ceres/ceres.h"
#include "glog/logging.h"

#include <iostream>
#include <fstream>

using namespace ceres;

/*using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;*/

void specularMinimisation(double& Roughness, double& Metallic, double& Gain, double& Bias, double LightDistance, double LightIntensity, std::string imageName, double residualValue, cv::Vec3d residual, double totalResidual, cv::Mat residualImage, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, double width, int height, int numberOfLights, GLuint RoughnessID, GLuint MetallicID, GLuint LightDistanceID, GLuint LightIntensityID, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, glm::mat4 ModelMatrix, glm::mat4 MVP, glm::mat4 ViewMatrix, glm::mat4 depthBiasMVP, std::vector<glm::vec3> lightInvDirs, std::vector<cv::Mat> textureImages, bool synthetic, double RoughnessSynthetic, double MetallicSynthetic);
void outputToFile(std::string outputFileName, double roughness, double metallic, double lightDistance, double lightIntensity, double gain, double bias, double resiudal);

class MyScalarCostFunctor {
	public:
		MyScalarCostFunctor(std::string outputFileName, double residualValue, cv::Vec3d residual, double totalResidual, cv::Mat residualImage, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, int numberOfLights, GLuint RoughnessID, GLuint MetallicID, GLuint LightDistanceID, GLuint LightIntensityID, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, glm::mat4 ModelMatrix, glm::mat4 MVP, glm::mat4 ViewMatrix, glm::mat4 depthBiasMVP, std::vector<glm::vec3> lightInvDirs, std::vector<cv::Mat> textureImages, double lightDistance, double lightIntensity):outputFileName_(outputFileName), residualValue_(residualValue), residual_(residual), totalResidual_(totalResidual), residualImage_(residualImage), depthProjectionMatrix_(depthProjectionMatrix), depthViewMatrix_(depthViewMatrix), width_(width), height_(height), numberOfLights_(numberOfLights), RoughnessID_(RoughnessID), MetallicID_(MetallicID), LightDistanceID_(LightDistanceID), LightIntensityID_(LightIntensityID), programID_(programID), ModelMatrixID_(ModelMatrixID), ViewMatrixID_(ViewMatrixID), DepthBiasID_(DepthBiasID), lightInvDirID_(lightInvDirID), Texture_(Texture), TextureID_(TextureID), depthTexture_(depthTexture), ShadowMapID_(ShadowMapID), vertexbuffer_(vertexbuffer), uvbuffer_(uvbuffer), normalbuffer_(normalbuffer), elementbuffer_(elementbuffer), indices_(indices), MatrixID_(MatrixID), ModelMatrix_(ModelMatrix), MVP_(MVP), ViewMatrix_(ViewMatrix), depthBiasMVP_(depthBiasMVP), lightInvDirs_(lightInvDirs), textureImages_(textureImages), lightDistance_(lightDistance), lightIntensity_(lightIntensity){}

		bool operator()(const double* const Roughness, const double* const Metallic, const double* const Gain, const double* const Bias, double* residualValue) const {
			
			double roughness  = double(Roughness[0]);
			double metallic   = double(Metallic[0]);
			double gain 	  = double(Gain[0]);
			double bias       = double(Bias[0]);
			
			double sum = 0;
			
			for (int i = 0; i < numberOfLights_; i++) {
				
				// Render the polygons
				renderPolygons(width_, height_, programID_, lightInvDirs_[i], MatrixID_, ModelMatrixID_, ViewMatrixID_, DepthBiasID_, lightInvDirID_, Texture_, TextureID_, depthTexture_, ShadowMapID_, vertexbuffer_, uvbuffer_, normalbuffer_, elementbuffer_, indices_, MVP_, ModelMatrix_, ViewMatrix_, depthBiasMVP_, RoughnessID_, roughness, MetallicID_, metallic, LightDistanceID_, lightDistance_, LightIntensityID_, lightIntensity_);
				//renderPolygons(width_, height_, programID_, lightInvDirs_[i], MatrixID_, ModelMatrixID_, ViewMatrixID_, DepthBiasID_, lightInvDirID_, Texture_, TextureID_, depthTexture_, ShadowMapID_, vertexbuffer_, uvbuffer_, normalbuffer_, elementbuffer_, indices_, MVP_, ModelMatrix_, ViewMatrix_, depthBiasMVP_, RoughnessID_, roughness, MetallicID_, metallic, LightDistanceID_, gain, LightIntensityID_, bias);
				
				cv::Mat model = readPixels(height_, width_);
				cv::Mat photograph = textureImages_[i];
				
				model.convertTo(model, CV_64FC3, gain, bias);
				//model.convertTo(model, CV_64FC3);
				photograph.convertTo(photograph, CV_64FC3);
				
				// Calculate the residual
				sum += meanSquaredError(model, photograph);
				//structuralSimilarityIndex(i, height_, width_, textureImages_, sum, gain, bias);
				
				// Swap buffers
				glfwSwapBuffers(window);
				glfwPollEvents();
			}

			//std::cout << "Specular Intensity = " << roughness << ", Specular Power = " << metallic << ", Residual = " << (sum/numberOfLights_) << ", SSD = " << (sum/numberOfLights_)*(sum/numberOfLights_) << std::endl;
			//std::cout << roughness << "\t" << metallic << "\t" << gain << "\t" << bias << "\t" << (sum/numberOfLights_) << std::endl;

			outputToFile(outputFileName_, roughness, metallic, lightDistance_, lightIntensity_, gain, bias, (sum/numberOfLights_));
			
			residualValue[0] = (sum/numberOfLights_);
			return true;
		}

	private:
		int width_;
		int height_;
		GLuint programID_;
		std::vector<glm::vec3> lightInvDirs_;
		GLuint MatrixID_;
		GLuint ModelMatrixID_;
		GLuint ViewMatrixID_;
		GLuint DepthBiasID_;
		GLuint lightInvDirID_;
		GLuint Texture_;
		GLuint TextureID_;
		GLuint depthTexture_;
		GLuint ShadowMapID_;
		GLuint vertexbuffer_;
		GLuint uvbuffer_;
		GLuint normalbuffer_;
		GLuint elementbuffer_;
		std::vector<unsigned int> indices_;
		glm::mat4 MVP_;
		glm::mat4 ModelMatrix_;
		glm::mat4 ViewMatrix_;
		glm::mat4 depthBiasMVP_;
		GLuint RoughnessID_;
		GLuint MetallicID_;
		GLuint LightDistanceID_;
		GLuint LightIntensityID_;
		
		double residualValue_;
		cv::Vec3d residual_;
		std::vector<cv::Vec3d> residuals_;
		double totalResidual_;
		cv::Mat residualImage_;
		glm::mat4 depthProjectionMatrix_;
		glm::mat4 depthViewMatrix_;
		int numberOfLights_;
		double lightDistance_;
		double lightIntensity_;
		
		std::vector<cv::Mat> textureImages_;
		std::string outputFileName_;
};

void specularMinimisation(double& Roughness, double& Metallic, double& Gain, double& Bias, double LightDistance, double LightIntensity, std::string imageName, double residualValue, cv::Vec3d residual, double totalResidual, cv::Mat residualImage, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, int numberOfLights, GLuint RoughnessID, GLuint MetallicID, GLuint LightDistanceID, GLuint LightIntensityID, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, glm::mat4 ModelMatrix, glm::mat4 MVP, glm::mat4 ViewMatrix, glm::mat4 depthBiasMVP, std::vector<glm::vec3> lightInvDirs, std::vector<cv::Mat> textureImages, bool synthetic, double RoughnessSynthetic, double MetallicSynthetic) {
	
	// The variable to solve for with its initial value. It will be mutated in place by the solver.
	const double initialRoughness = Roughness;
	const double initialMetallic  = Metallic;
	const double initialGain 	  = Gain;
	const double initialBias      = Bias;

	// Create the file name of the log with the initial parameters
	std::ostringstream stm;
	//stm << imageName << " " << Roughness << " " << Metallic << " " << Gain << " " << Bias << ".txt";
	stm << imageName << " " << Roughness << " " << Metallic << " " << Gain << " " << Bias << ".txt";
	std::string outputFileName = stm.str();
	std::ofstream outputFile(outputFileName, std::ios::trunc); // Erase the previous contents of the file
	//outputFile << "Roughness" << "\t" << "Metallic" << "\t" << "Light Distance" << "\t" << "Light Intensity" << "\t" << "Gain" << "\t" << "Bias" << "\t" << "Resiudal" << std::endl;
	outputFile << "Roughness" << "\t" << "Metallic" << "\t" << "Gain" << "\t" << "Bias" << "\t" << "Resiudal" << std::endl;

	// Build the problem.
	ceres::Problem problem;

	// Set up the only cost function (also known as residual). This uses auto-differentiation to obtain the derivative (jacobian).
	// AutoDiffCostFunction<CostFunctor, (Dimensions of Residual), (Dimensions of Variables)>
	ceres::CostFunction* cost_function = new NumericDiffCostFunction<MyScalarCostFunctor, CENTRAL, 1, 1, 1, 1, 1>(new MyScalarCostFunctor(outputFileName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightIntensityID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, textureImages, LightDistance, LightIntensity));
	std::vector<ceres::ResidualBlockId> residual_block_ids;
	ceres::ResidualBlockId block_id = problem.AddResidualBlock(cost_function, NULL, &Roughness, &Metallic, &Gain, &Bias);
	residual_block_ids.push_back(block_id);

	problem.SetParameterLowerBound(&Roughness, 0, 0);
	problem.SetParameterUpperBound(&Roughness, 0, 1);
	problem.SetParameterLowerBound(&Metallic, 0, 0);
	problem.SetParameterUpperBound(&Metallic, 0, 1);
	problem.SetParameterLowerBound(&Gain, 0, 0);
	//problem.SetParameterLowerBound(&Bias, 0, 0);
	//problem.SetParameterBlockConstant(&Roughness);
	//problem.SetParameterBlockConstant(&Metallic);
	//problem.SetParameterBlockConstant(&Gain);
	//problem.SetParameterBlockConstant(&Bias);

	/*problem.SetParameterLowerBound(&Roughness, 0, 0);
	problem.SetParameterUpperBound(&Roughness, 0, 1);
	problem.SetParameterLowerBound(&Metallic, 0, 0.01);
	problem.SetParameterLowerBound(&Gain, 0, 0);
	//problem.SetParameterLowerBound(&Bias, 0, 0);
	//problem.SetParameterBlockConstant(&Roughness);
	//problem.SetParameterBlockConstant(&Metallic);
	//problem.SetParameterBlockConstant(&Gain);
	//problem.SetParameterBlockConstant(&Bias);*/
	
	// Run the solver
	Solver::Options options;
	options.minimizer_progress_to_stdout = true;
	// Set minimum step size
	/*
	double Solver::Options::line_search_sufficient_function_decrease
	Default: 1e-4

	Solving the line search problem exactly is computationally prohibitive. Fortunately, line search based optimization algorithms can still guarantee convergence if instead of an exact solution, the line search algorithm returns a solution which decreases the value of the objective function sufficiently. More precisely, we are looking for a step size s.t.

	\[f(\text{step_size}) \le f(0) + \text{sufficient_decrease} * [f'(0) * \text{step_size}]\]
	This condition is known as the Armijo condition.

	double Solver::Options::max_line_search_step_contraction
	Default: 1e-3

	In each iteration of the line search,

	\[\text{new_step_size} >= \text{max_line_search_step_contraction} * \text{step_size}\]
	Note that by definition, for contraction:

	\[0 < \text{max_step_contraction} < \text{min_step_contraction} < 1\]
	double Solver::Options::min_line_search_step_contraction
	Default: 0.6

	In each iteration of the line search,

	\[\text{new_step_size} <= \text{min_line_search_step_contraction} * \text{step_size}\]
	Note that by definition, for contraction:

	\[0 < \text{max_step_contraction} < \text{min_step_contraction} < 1\]
	*/
	
	Solver::Summary summary;
	Solve(options, &problem, &summary);
	
	ceres::Problem::EvaluateOptions evaluateOptions;
	evaluateOptions.residual_blocks = residual_block_ids;
	double total_cost = 0.0;
	std::vector<double> residuals;
	problem.Evaluate(evaluateOptions, &total_cost, &residuals, nullptr, nullptr);
	//for (auto i = residuals.begin(); i != residuals.end(); ++i)
    //	std::cout << *i << ' ';

	std::cout << summary.BriefReport() << std::endl;
	//std::cout << summary.FullReport() << std::endl;
	std::cout << "Roughness: " << initialRoughness << " -> " << Roughness << std::endl;
	std::cout << "Metallic: "  << initialMetallic  << " -> " << Metallic  << std::endl;
	std::cout << "Gain: "      << initialGain	   << " -> " << Gain      << std::endl;
	std::cout << "Bias: "      << initialBias      << " -> " << Bias      << std::endl;

	//double gain = 1.0, bias = 0.0;

	for (int i = 0; i < numberOfLights; i++) {
		std::ostringstream stm2;
		stm2 << imageName << " " << Roughness << " " << Metallic << " " << Gain << " " << Bias << " " << i << ".png";
		std::string outputImageName = stm2.str();

		renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity);
		//renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, Gain, LightIntensityID, Bias);

		cv::Mat model = readPixels(height, width);
		model.convertTo(model, CV_8UC3, Gain, Bias);
		//model.convertTo(model, CV_8UC3);

		cv::imwrite(outputImageName, model);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	//outputFile.close();
	std::ostringstream stm3;
	if (synthetic)
		stm3 << imageName << " " << initialRoughness << " " << initialMetallic << " " << RoughnessSynthetic << " " << MetallicSynthetic << " " << Roughness << " " << Metallic << " " << Gain << " " << Bias << ".txt";
	else
		stm3 << imageName << " " << initialRoughness << " " << initialMetallic << " " << Roughness << " " << Metallic << " " << Gain << " " << Bias << ".txt";
	
	std::string outputFileName2 = stm3.str();
	int rename = std::rename(outputFileName.c_str(), outputFileName2.c_str());
	/*if (std::rename(outputFileName2, outputFileName)) {
        std::perror("Error renaming");
    }*/
}

void outputToFile(std::string outputFileName, double roughness, double metallic, double lightDistance, double lightIntensity, double gain, double bias, double resiudal) {
	std::ofstream outputFile(outputFileName, std::ios::app);
	if (outputFile.is_open()) {
		//outputFile << roughness << "\t" << metallic << "\t" << lightDistance << "\t" << lightIntensity << "\t" << gain << "\t" << bias << "\t" << resiudal << std::endl;
		outputFile << roughness << "\t" << metallic << "\t" << gain << "\t" << bias << "\t" << resiudal << std::endl;
		outputFile.close();
	}
}

#endif
