#define GLM_ENABLE_EXPERIMENTAL

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <ctime>
#include <cmath>
#include <stdlib.h>

//#include </user/HS222/tw00275/opencv-bin/include/opencv2/opencv.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/core.hpp>		// Core contains the definitions of the basic building blocks of the OpenCV library
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/highgui.hpp>	// HighGUI contains the functions for input and output operations
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>		// Core contains the definitions of the basic building blocks of the OpenCV library
#include <opencv2/highgui.hpp>	// HighGUI contains the functions for input and output operations
#include <opencv2/imgproc.hpp>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
//#include </user/HS222/tw00275/glm-bin/include/glm/glm.hpp>
//#include </user/HS222/tw00275/glm-bin/include/glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "shader.h"
#include "texture.h"
#include "objloader.h"
#include "vboindexer.h"

#include "OpenCV.h"
#include "Charuco.h"
#include "OpenGL.h"
#include "Ceres.h"
#include "Macbeth.h"

#include "PhotometricStereo.h"
#include "PFMAccess.h"
