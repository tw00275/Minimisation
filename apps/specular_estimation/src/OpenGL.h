#pragma once
#ifndef OPENGL_H
#define OPENGL_H

// Include GLEW. Always include it before gl.h and glfw.h
#include <GL/glew.h>

// Include GLFW to handle the window and the user input
#include <GLFW/glfw3.h>

// Include GLM, a library for 3D mathematics
//#include </user/HS222/tw00275/glm-bin/include/glm/glm.hpp>
//#include </user/HS222/tw00275/glm-bin/include/glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//using namespace glm;

#include "shader.h"
#include "texture.h"
#include "objloader.h"
#include "vboindexer.h"
//#include <common/controls.hpp>

//#include </user/HS222/tw00275/glm-bin/include/glm/gtc/quaternion.hpp>
//#include </user/HS222/tw00275/glm-bin/include/glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

//#include <windows.h>	// Header File For Windows
#include <GL/gl.h>		// Header File For The OpenGL32 Library
#include <GL/glu.h>		// Header File For The GLu32 Library

//#include "/user/HS222/tw00275/ceres-bin/include/ceres/ceres.h"
#include "ceres/ceres.h"
#include "glog/logging.h"

using ceres::AutoDiffCostFunction;
using ceres::CostFunction;
using ceres::Problem;
using ceres::Solver;
using ceres::Solve;

void computeMatricesFromInputs(int windowWidth, int windowHeight, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, float mouseSpeed, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::vec3& lightInvDir, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, bool& perspectiveProjection, bool& shadowControl, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, bool& calculateResidual);
void computeMatricesFromLights(int windowWidth, int windowHeight, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::vec3& lightInvDir, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, bool& perspectiveProjection, cv::Mat lightDirections, int& lightNumber, int numberOfLights, bool& calculateResidual, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
void createSphere(int lats, int longs);
void createMesh(cv::Mat Z, cv::Mat normals, std::vector<glm::vec3>& out_vertices, std::vector<glm::vec2> & out_uvs, std::vector<glm::vec3> & out_normals, std::vector<unsigned int> & out_indices);
GLuint loadMat(cv::Mat cv_texture);
void initialiseOpenGL(cv::Mat cv_depth, cv::Mat cv_normals, cv::Mat cv_texture, cv::Mat lightDirections, int& windowWidth, int& windowHeight, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, GLuint& programID, GLuint& MatrixID, GLuint& ModelMatrixID, GLuint& ViewMatrixID, GLuint& DepthBiasID, GLuint& lightInvDirID, GLuint& Texture, GLuint& TextureID, GLuint& depthTexture, GLuint& ShadowMapID, GLuint& vertexbuffer, GLuint& uvbuffer, GLuint& normalbuffer, GLuint& elementbuffer, std::vector<unsigned int> & indices, GLuint& depthProgramID, GLuint& quad_programID, GLuint& FramebufferName, GLuint& quad_vertexbuffer, GLuint& VertexArrayID, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, int numberOfLights, bool calculateResidual, glm::mat4& depthMVP, glm::mat4& depthModelMatrix, glm::mat4& MVP, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::mat4& ModelMatrix, glm::mat4& depthBiasMVP, glm::mat4& biasMatrix);
//void openGL(int& lightNumber, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector< cv::Mat > textureImages, GLuint programID, GLuint MatrixID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint depthProgramID, GLuint quad_programID, GLuint FramebufferName, GLuint quad_vertexbuffer, GLuint VertexArrayID, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, bool& calculateResidual);
void terminateOpenGL(GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, GLuint programID, GLuint depthProgramID, GLuint quad_programID, GLuint Texture, GLuint FramebufferName, GLuint depthTexture, GLuint quad_vertexbuffer, GLuint VertexArrayID);
void renderPolygons(int windowWidth, int windowHeight, GLuint programID, glm::vec3 lightInvDir, GLuint MatrixID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, glm::mat4 MVP, glm::mat4 ModelMatrix, glm::mat4 ViewMatrix, glm::mat4 depthBiasMVP, GLuint RoughnessID, double Roughness, GLuint MetallicID, double Metallic, GLuint DistanceID, double Distance, GLuint LightPowerID, double LightPower);
void renderShadows(GLuint FramebufferName, int shadowResolution, GLuint depthProgramID, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, GLuint depthMatrixID, GLuint vertexbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, glm::mat4 depthModelMatrix, glm::mat4 depthMVP);
void renderShadowMap(GLuint quad_programID, GLuint depthTexture, GLuint texID, GLuint quad_vertexbuffer);
cv::Mat readPixels(int windowHeight, int windowWidth);
//void meanSquaredError(int imageNumber, int windowHeight, int windowWidth, std::string modelPath, std::vector < cv::Mat > textureImages, cv::Mat& residual, cv::Vec3d& residuals, double& sum, double Roughness, double Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
double meanSquaredError(cv::Mat model, cv::Mat photograph);
void structuralSimilarityIndex(int imageNumber, int windowHeight, int windowWidth, std::vector<cv::Mat> textureImages, double& sum, double gain, int bias);
double returnResidual(int imageNumber, int windowHeight, int windowWidth, std::vector<cv::Mat> textureImages, cv::Mat& residual, cv::Vec3d& residuals, double Roughness, double Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
//void computeResiduals(cv::Vec3d& residual, std::vector< cv::Vec3d >& residuals, double& totalResidual, cv::Mat& residualImage, bool calculateResidual, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector<cv::Mat> textureImages, int lightNumber, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath);
void viewModel(cv::Vec3d& residual, double& totalResidual, cv::Mat& residualImage, bool calculateResidual, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector<cv::Mat> textureImages, int lightNumber, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath);
void RenderSynthetic(glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector<cv::Mat>& textureImages, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath);
void checkForErrors(std::string stage, bool printIfOK);
void calibrateLights(int& windowWidth, int& windowHeight, cv::Mat cv_texture);

void initialiseOpenGL(cv::Mat cv_depth, cv::Mat cv_normals, cv::Mat cv_texture, cv::Mat lightDirections, int& windowWidth, int& windowHeight, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, GLuint& programID, GLuint& MatrixID, GLuint& ModelMatrixID, GLuint& ViewMatrixID, GLuint& DepthBiasID, GLuint& lightInvDirID, GLuint& Texture, GLuint& TextureID, GLuint& depthTexture, GLuint& ShadowMapID, GLuint& vertexbuffer, GLuint& uvbuffer, GLuint& normalbuffer, GLuint& elementbuffer, std::vector<unsigned int> & indices, GLuint& depthProgramID, GLuint& quad_programID, GLuint& FramebufferName, GLuint& quad_vertexbuffer, GLuint& VertexArrayID, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, int numberOfLights, bool calculateResidual, glm::mat4& depthMVP, glm::mat4& depthModelMatrix, glm::mat4& MVP, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::mat4& ModelMatrix, glm::mat4& depthBiasMVP, glm::mat4& biasMatrix) {
	// Initialise the GLFW library first
	if (!glfwInit()) {
		std::cerr << "Failed to initialise GLFW.\n";
		getchar();
		return;
	}
	std::cout << "Initialised GLFW.\n";

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Only needed for macOS
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Disable old OpenGL

	// Set the shadow resolution to 4096x4096
	int shadowResolution = 4096;

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(windowWidth, windowHeight, "Window", NULL, NULL);
	if (window == NULL) {
		std::cerr << "Failed to open GLFW window. Older Intel GPUs are not OpenGL 3.3 compatible.\n";
		getchar();
		glfwTerminate();
		return;
	}
	glfwMakeContextCurrent(window);
	std::cout << "Opened GLFW window.\n";

	// On macOS with a retina screen it will be windowWidth*2 and windowHeight*2, so the actual framebuffer size is:
	glfwGetFramebufferSize(window, &windowWidth, &windowHeight);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		std::cerr << "Failed to initialise GLEW.\n";
		getchar();
		glfwTerminate();
		return;
	}
	std::cout << "Initialised GLEW.\n";

	// Ensure that the escape key being pressed can be captured
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	//glfwSetCursorPos(window, windowWidth / 2, windowHeight / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test, which stores fragments in the Z-buffer
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it is closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which do not have a normal facing towards the camera
	glEnable(GL_CULL_FACE);

	// Create a Vertex Array Object (VAO) and set it as the current one
	//GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile the GLSL program from the shaders
	depthProgramID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/DepthRTT.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/DepthRTT.fragmentshader");

	// Get a handle for the "MVP" uniform
	GLuint depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");

	// Load the texture
	//GLuint Texture = loadDDS("C:/Users/tdwal/Documents/repos/OpenGLTutorials/tutorial16_shadowmaps/uvmap.DDS");
	Texture = loadMat(cv_texture);
	std::cout << "Texture loaded.\n\n";

	// Read the .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	//std::vector<unsigned int> indices;
	//bool res = loadOBJ("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/sphere.obj", vertices, uvs, normals);
	createMesh(cv_depth, cv_normals, vertices, uvs, normals, indices);
	//createSphere(100, 100);
	std::cout << "\nModel created.\n\n";

	// Load it into a VBO
	//GLuint vertexbuffer; // This will identify the vertex buffer
	glGenBuffers(1, &vertexbuffer); // Generate 1 buffer, and put the resulting identifier in vertexbuffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer); // The following commands will talk about the 'vertexbuffer' buffer
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW); // Give the vertices to OpenGL

	//GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

	//GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	//GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);


	// ---------------------------------------------
	// Render to Texture - specific code begins here
	// ---------------------------------------------

	// The framebuffer, which regroups 0, 1 or more textures, and 0 or 1 depth buffers
	FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// Depth texture. Slower than a depth buffer, but can be sampled later in the shader
	//GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT16, shadowResolution, shadowResolution, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	// No colour output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);

	// Always check if there are any errors in the framebuffer
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return;

	// The quad's FBO. Used only for visualising the shadowmap.
	static const GLfloat g_quad_vertex_buffer_data[] = {
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
	};

	//GLuint quad_vertexbuffer;
	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile the GLSL program from the shaders
	quad_programID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/Passthrough.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/SimpleTexture.fragmentshader");
	//std::cout << "Shaders loaded.\n";
	GLuint texID = glGetUniformLocation(quad_programID, "texture");
	//std::cout << "Uniform Location of texture found.\n";

	// Create and compile the GLSL program from the shaders
	programID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/ShadowMapping.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/ShadowMapping.fragmentshader");
	
	// Get a handle for the "myTextureSampler" uniform
	TextureID = glGetUniformLocation(programID, "myTextureSampler");
	//SpecularID = glGetUniformLocation(programID, "mySpecularSampler");
	
	RoughnessID = glGetUniformLocation(programID, "roughness");
	MetallicID = glGetUniformLocation(programID, "metallic");
	DistanceID = glGetUniformLocation(programID, "distance");
	LightPowerID = glGetUniformLocation(programID, "lightPower");

	// Get a handle for the "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");
	DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");
	ShadowMapID = glGetUniformLocation(programID, "shadowMap");

	// Get a handle for the "LightPosition" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");

	// Compute the MVP matrix from the light's point of view
	//lightInvDir = glm::vec3(0, 0, 1);
	depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 1.0f, 100.0f);
	depthViewMatrix = glm::lookAt(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	// Initial position: on +Z
	position = glm::vec3(0, 0, 1);
	// Initial horizontal angle: toward -Z
	horizontalAngle = M_PI;
	// Initial vertical angle: none
	verticalAngle = 0.0f;
	// Initial Field of View
	FoV = 60.0f;
	// Mouse Speed
	float mouseSpeed = 0.005f;
	
	depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
	bool perspectiveProjection = false;
	int lightNumber = 0;

	// Compute the MVP matrix
	computeMatricesFromLights(windowWidth, windowHeight, position, horizontalAngle, verticalAngle, FoV, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, lightDirections, lightNumber, numberOfLights, calculateResidual, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
	
	MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
	depthBiasMVP = biasMatrix * depthMVP;
}

void terminateOpenGL(GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, GLuint programID, GLuint depthProgramID, GLuint quad_programID, GLuint Texture, GLuint FramebufferName, GLuint depthTexture, GLuint quad_vertexbuffer, GLuint VertexArrayID) {
	
	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteProgram(depthProgramID);
	glDeleteProgram(quad_programID);
	glDeleteTextures(1, &Texture);

	glDeleteFramebuffers(1, &FramebufferName);
	glDeleteTextures(1, &depthTexture);
	glDeleteBuffers(1, &quad_vertexbuffer);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}

void renderShadows(GLuint FramebufferName, int shadowResolution, GLuint depthProgramID, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, GLuint depthMatrixID, GLuint vertexbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, glm::mat4 depthModelMatrix, glm::mat4 depthMVP) {
	// Render to the framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
	glViewport(0, 0, shadowResolution, shadowResolution); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	// The shadows don't use bias in the shader, but instead are drawn from back faces, which are already separated from the front faces by a small distance (if the geometry is made this way)
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use the shader
	glUseProgram(depthProgramID);

	// Send the transformation to the currently bound shader, in the "MVP" uniform
	glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

	// 1st attribute buffer: vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,			// attribute 0. Must be 0 to match the layout in the shader.
		3,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalized?
		0,			// stride
		(void*)0	// array buffer offset
	);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles
	glDrawElements(
		GL_TRIANGLES,				// mode
		GLsizei(indices.size()),	// count
		GL_UNSIGNED_INT,			// type
		(void*)0					// element array buffer offset
	);
	glDisableVertexAttribArray(0);
}

// Render the shadowmap (for debug only)
void renderShadowMap(GLuint quad_programID, GLuint depthTexture, GLuint texID, GLuint quad_vertexbuffer) {

	// Render only on a corner of the window (or we we won't see the real rendering...)
	glViewport(0, 0, 512, 512);

	// Use our shader
	glUseProgram(quad_programID);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	// Set our "renderedTexture" sampler to use Texture Unit 0
	glUniform1i(texID, 0);

	// 1st attribute buffer: vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glVertexAttribPointer(
		0,			// attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalized?
		0,			// stride
		(void*)0	// array buffer offset
	);

	// Draw the triangle
	// GL_COMPARE_R_TO_TEXTURE must be disabled
	//glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles
	glDisableVertexAttribArray(0);
}

void renderPolygons(int windowWidth, int windowHeight, GLuint programID, glm::vec3 lightInvDir, GLuint MatrixID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, glm::mat4 MVP, glm::mat4 ModelMatrix, glm::mat4 ViewMatrix, glm::mat4 depthBiasMVP, GLuint RoughnessID, double Roughness, GLuint MetallicID, double Metallic, GLuint DistanceID, double Distance, GLuint LightPowerID, double LightPower) {

	// Render to the screen
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, windowWidth, windowHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use the shader
	glUseProgram(programID);
	
	// Send the transformation to the currently bound shader, in the "MVP" uniform
	glUniformMatrix4fv(MatrixID,      1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
	glUniformMatrix4fv(ViewMatrixID,  1, GL_FALSE, &ViewMatrix[0][0]);
	glUniformMatrix4fv(DepthBiasID,   1, GL_FALSE, &depthBiasMVP[0][0]);
	
	glUniform1f(RoughnessID, Roughness);
	glUniform1f(MetallicID,     Metallic);
	glUniform1f(DistanceID,          Distance);
	glUniform1f(LightPowerID,        LightPower);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Bind the texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Texture);
	// Set the "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(TextureID, 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glUniform1i(ShadowMapID, 1);

	// 1st attribute buffer: vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,			// attribute
		3,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalized?
		0,			// stride
		(void*)0	// array buffer offset
	);

	// 2nd attribute buffer: UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glVertexAttribPointer(
		1,			// attribute
		2,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalized?
		0,			// stride
		(void*)0	// array buffer offset
	);

	// 3rd attribute buffer: normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		2,			// attribute
		3,			// size
		GL_FLOAT,	// type
		GL_FALSE,	// normalized?
		0,			// stride
		(void*)0	// array buffer offset
	);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles
	glDrawElements(
		GL_TRIANGLES,				// mode
		GLsizei(indices.size()),	// count
		GL_UNSIGNED_INT,			// type
		(void*)0					// element array buffer offset
	);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

}

/*void createSpecularMap(std::vector < cv::Mat > modelImages, cv::Mat lightDirections, cv::Mat& specularMap) {
	// Initialise the specular map Mat with 127 in the blue channel (BGR)
	cv::Mat specularMap(modelImages[0].rows, modelImages[0].cols, CV_8UC3, cv::Scalar(127, 0, 0));
	cv::Mat aboveAndLeft(modelImages[0].rows, modelImages[0].cols, CV_8UC3, cv::Scalar::all(0));
	cv::Mat belowAndRight(modelImages[0].rows, modelImages[0].cols, CV_8UC3, cv::Scalar::all(0));

	/*cv::Mat upperLUT(1, 256, CV_8UC3, cv::Scalar::all(0));
	for (int u = 128; u < 256; u++) {
		upperLUT.at<uchar>[1](1, u) = (u - 128) * 2; // Green goes between 128 and 255
		upperLUT.at<uchar>[2](1, u) = (u - 128) * 2; // Red goes between 128 and 255
	}

	cv::Mat lowerLUT(1, 256, CV_8UC3, cv::Scalar::all(0));
	for (int u = 0; u < 128; u++) {
		lowerLUT.at<uchar>[1](1, u) = (128 - u) * 2; // Green goes between 0 and 127
		lowerLUT.at<uchar>[2](1, u) = (128 - u) * 2; // Red goes between 0 and 127
	}*/

	/*for (int i = 0; i < lightDirections.rows; i++) {
		cv::Mat temp(modelImages[0].rows, modelImages[0].cols, CV_8UC3);
		cv::Scalar lightDir(lightDirections.at<float>(i, 1), -lightDirections.at<float>(i, 0));

		//aboveAndLeft = modelImages[i] * lightDir[0];
		cv::Mat above = modelImages[i] * lightDir[1];
		cv::Mat left = modelImages[i] * lightDir[0];
		std::vector < cv::Mat > inputArrays;
		inputArrays.push_back(specularMap);
		inputArrays.push_back(left);
		inputArrays.push_back(above);
		cv::merge(inputArrays, aboveAndLeft);
	}
}*/

/*void meanSquaredError(int imageNumber, int windowHeight, int windowWidth, std::string modelPath, std::vector <cv::Mat> textureImages, cv::Mat& residual, cv::Vec3d& residuals, double& sum, double Roughness, double Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower) {
	
	// Create an empty Mat the same size as the image
	cv::Mat temp = cv::Mat(windowHeight, windowWidth, CV_8UC3);
	cv::Mat sumOfSquaredDifferences = cv::Mat(windowHeight, windowWidth, CV_16UC3);
	
	// Read the image into the Mat as an 8-bit colour image
	glReadPixels(0, 0, windowWidth, windowHeight, GL_BGR, GL_UNSIGNED_BYTE, temp.data);
	
	// 3D models have the origin in the bottom-left corner, while images have the origin in the top-left corner. The image is flipped to convert between the two.
	cv::flip(temp, temp, 0);
	
	//temp = temp(cv::Rect(1, 1, windowWidth-2, windowHeight-2));
	//copyMakeBorder(temp, temp, 1, 1, 1, 1, cv::BORDER_REPLICATE);
	
	//cv::Mat texture = textureImages[imageNumber];
	//cv::cvtColor(texture, texture, CV_BGR2GRAY);
	//texture.convertTo(texture, CV_8UC3);

	// Calculate the absolute differences between the model and the photograph
	cv::absdiff(temp, textureImages[imageNumber], residual);
	
	// Square each element
	multiply(residual, residual, sumOfSquaredDifferences);

	cv::cvtColor(residual, residual, CV_BGR2GRAY);
	
	std::ostringstream stm;
	stm << imageNumber;
	std::string number = stm.str();
	
	// Display the residual
	//cv::namedWindow("Model", CV_WINDOW_NORMAL);
	//cv::imshow("Model", temp);
	//cv::namedWindow("Sum of Squared Differences", CV_WINDOW_NORMAL);
	//cv::imshow("Sum of Squared Differences", sumOfSquaredDifferences);
	cv::imwrite(modelPath + "model." + number + ".png", temp);
	cv::imwrite(modelPath + "residual." + number + ".png", residual);
	
	// Crop a 1-pixel border around the residual
	sumOfSquaredDifferences = sumOfSquaredDifferences(cv::Rect(1, 1, windowWidth-2, windowHeight-2));
	
	sum = (cv::sum(residual)[0] + cv::sum(residual)[1] + cv::sum(residual)[2])/(3 * (windowHeight-2) * (windowWidth-2));
	//sum = (cv::sum(sumOfSquaredDifferences)[0] + cv::sum(sumOfSquaredDifferences)[1] + cv::sum(sumOfSquaredDifferences)[2])/(windowHeight-2 * windowWidth-2);
	//sum = (cv::sum(sumOfSquaredDifferences)[0] + cv::sum(sumOfSquaredDifferences)[1] + cv::sum(sumOfSquaredDifferences)[2]);
	residuals = cv::Vec3d(cv::sum(sumOfSquaredDifferences)[0], cv::sum(sumOfSquaredDifferences)[1], cv::sum(sumOfSquaredDifferences)[2]);
	
	
	
	// Use the matchTemplate function to calculate the sum of squared differences
	//matchTemplate(temp, textureImages[imageNumber], residual, 1);
	
	//sum = cv::sum(residual)[0];
	
	
	//std::cout << "Image " << imageNumber << ", SSD per pixel: R = " << cv::sum(sumOfSquaredDifferences)[2]/(windowHeight * windowWidth) << ", G = " << cv::sum(sumOfSquaredDifferences)[1]/(windowHeight * windowWidth) << ", B = " << cv::sum(sumOfSquaredDifferences)[0]/(windowHeight * windowWidth) << ", specular intensity = " << Roughness << ", specular power = " << Metallic << std::endl;
	//std::cout << "Image " << imageNumber << ", average sum of squared differences: R = " << sum << ", specular intensity = " << Roughness << ", specular power = " << Metallic << std::endl;
}*/

cv::Mat readPixels(int windowHeight, int windowWidth) {
	// Create an empty Mat the same size as the image
	cv::Mat model = cv::Mat(windowHeight, windowWidth, CV_8UC3);

	// Read the image into the Mat as an 8-bit colour image
	glReadPixels(0, 0, windowWidth, windowHeight, GL_BGR, GL_UNSIGNED_BYTE, model.data);

	// 3D models have the origin in the bottom-left corner, while images have the origin in the top-left corner. The image is flipped to convert between the two.
	cv::flip(model, model, 0);

	return model;
}

double meanSquaredError(cv::Mat model, cv::Mat photograph) {
	
	// Create an empty Mat the same size as the image
	cv::Mat residual = cv::Mat(model.size(), CV_64FC3);
	//cv::Mat squaredDifferences = cv::Mat(model.size(), CV_64FC3);

	// Calculate the absolute differences between the model and the photograph
	//photograph.convertTo(photograph, CV_64FC3);
	cv::absdiff(model, photograph, residual);
	//cv::absdiff(model, textureImage, residual);
	
	// Square each element
	multiply(residual, residual, residual);
	//multiply(residual, residual, squaredDifferences);
	
	// Crop a 1-pixel border around the residual
	residual = residual(cv::Rect(1, 1, (model.cols)-2, (model.rows)-2));
	//squaredDifferences = squaredDifferences(cv::Rect(1, 1, windowWidth-2, windowHeight-2));
	
	return (cv::sum(residual)[0] + cv::sum(residual)[1] + cv::sum(residual)[2])/(3 * ((model.cols)-2) * ((model.rows)-2));
	//sum += (cv::sum(squaredDifferences)[0] + cv::sum(squaredDifferences)[1] + cv::sum(squaredDifferences)[2])/(3 * (windowHeight-2) * (windowWidth-2));
	//sum = (cv::sum(squaredDifferences)[0] + cv::sum(squaredDifferences)[1] + cv::sum(squaredDifferences)[2])/(windowHeight-2 * windowWidth-2);
	//sum += (cv::sum(squaredDifferences)[0] + cv::sum(squaredDifferences)[1] + cv::sum(squaredDifferences)[2]);
}

void structuralSimilarityIndex(int imageNumber, int windowHeight, int windowWidth, std::vector<cv::Mat> textureImages, double& sum, double gain, int bias) {
	
	// Create an empty Mat the same size as the image
	cv::Mat model = readPixels(windowHeight, windowWidth);
	cv::Mat residual = cv::Mat(windowHeight, windowWidth, CV_64FC3);
	cv::Mat sumOfSquaredDifferences = cv::Mat(windowHeight, windowWidth, CV_64FC3);
	
	model.convertTo(model, CV_64FC3, gain, bias);

	sum += 3 - getMSSIM(model, textureImages[imageNumber]);
}

double returnResidual(int imageNumber, int windowHeight, int windowWidth, std::vector <cv::Mat> textureImages, cv::Mat& residual, cv::Vec3d& residuals, double Roughness, double Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower) {
		
	// Create an empty Mat the same size as the image
	cv::Mat temp = cv::Mat(windowHeight, windowWidth, CV_8UC3);
	
	// Read the image into the Mat as an 8-bit colour image
	glReadPixels(0, 0, windowWidth, windowHeight, GL_BGR, GL_UNSIGNED_BYTE, temp.data);
	
	// 3D models have the origin in the bottom-left corner, while images have the origin in the top-left corner. The image is flipped to convert between the two.
	cv::flip(temp, temp, 0);
	
	
	
	// Calculate the absolute differences between the model and the photograph
	cv::absdiff(temp, textureImages[imageNumber], residual);
	
	// Square each element
	multiply(residual, residual, temp);
	
	// Display the residual
	//cv::imshow("Original", temp);
	//cv::imshow("Residual", residual);
	
	// 
	double sum = (cv::sum(temp)[0] + cv::sum(temp)[1] + cv::sum(temp)[2])/(windowHeight * windowWidth);
	residuals = cv::Vec3d(cv::sum(temp)[0], cv::sum(temp)[1], cv::sum(temp)[2]);
	
	
	/*
	// Use the matchTemplate function to calculate the sum of squared differences
	matchTemplate(temp, textureImages[imageNumber], residual, 1);
	
	sum = cv::sum(residual)[0];
	*/
	
	std::cout << "Average sum of squared differences for image " << imageNumber << " = " << sum << ", specular intensity = " << Roughness << ", specular power = " << Metallic << std::endl << std::endl;
	
	return sum;
}
/*
void computeResiduals(cv::Vec3d& residual, std::vector< cv::Vec3d >& residuals, double& totalResidual, cv::Mat& residualImage, bool calculateResidual, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector< cv::Mat > textureImages, int lightNumber, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, , GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPowerGLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath) {
	do {
		// Render the shadows
		glm::mat4 depthModelMatrix = glm::mat4(1.0);
		glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
		//renderShadows(FramebufferName, shadowResolution, depthProgramID, depthProjectionMatrix, depthViewMatrix, depthMatrixID, vertexbuffer, elementbuffer, indices, depthModelMatrix, depthMVP);

		// Compute the MVP matrix from keyboard and mouse input
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
		bool perspectiveProjection, shadowControl;
		//computeMatricesFromInputs(windowWidth, windowHeight, position, horizontalAngle, verticalAngle, FoV, mouseSpeed, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, shadowControl, RoughnessID, Roughness, MetallicID, Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
		computeMatricesFromLights(width, height, position, horizontalAngle, verticalAngle, FoV, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection = false, lightDirections, lightNumber, numberOfLights, calculateResidual, RoughnessID, Roughness, MetallicID, Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
		);

		depthBiasMVP = biasMatrix * depthMVP;
		
			
		// Render the polygons
		renderPolygons(width, height, programID, lightInvDir, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
		
		// Optionally render the shadowmap (for debug only)
		//void renderShadowMap(GLuint quad_programID, GLuint depthTexture, GLuint texID, GLuint quad_vertexbuffer);
		
		
		
		//totalResidual = returnResidual(lightNumber, height, width, textureImages, residualImage, residuals, Roughness, Metallic, DistanceID, Distance, LightPowerID, LightPower, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
		meanSquaredError(lightNumber, height, width, modelPath, textureImages, residualImage, residual, totalResidual, Roughness, Metallic, DistanceID, Distance, LightPowerID, LightPower, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower);
		residuals.push_back(residual);
		
		// Set up the only cost function (also known as residual). This uses auto-differentiation to obtain the derivative (jacobian).
		// AutoDiffCostFunction<CostFunctor, (Dimensions of Residual), (Dimensions of Variables)>
		//CostFunction* cost_function = new AutoDiffCostFunction<CostFunctor, 1, 2>(new CostFunctor);
		//problem.AddResidualBlock(cost_function, NULL, &Roughness, &Metallic, &Distance, &LightPower);
		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
		
		
		
		//TODO: Save model image and residual
		
		
		//CostFunction* cost_function = new AutoDiffCostFunction<CostFunctor, 1, 1>(new CostFunctor);
		//problem.AddResidualBlock(cost_function, NULL, &x);
		
		lightNumber++;
	} while (lightNumber < numberOfLights);
	//} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0); // Check if the ESC key was pressed or the window was closed
	
	lightNumber = 0;
}
*/
void viewModel(cv::Vec3d& residual, double& totalResidual, cv::Mat& residualImage, bool calculateResidual, glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector<cv::Mat> textureImages, int lightNumber, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath) {
	do {
		// Render the shadows
		glm::mat4 depthModelMatrix = glm::mat4(1.0);
		glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
		//renderShadows(FramebufferName, shadowResolution, depthProgramID, depthProjectionMatrix, depthViewMatrix, depthMatrixID, vertexbuffer, elementbuffer, indices, depthModelMatrix, depthMVP);

		// Compute the MVP matrix from keyboard and mouse input
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
		bool perspectiveProjection = false, shadowControl;
		//float mouseSpeed = 0.005f;
		//computeMatricesFromInputs(width, height, position, horizontalAngle, verticalAngle, FoV, mouseSpeed, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, shadowControl, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPowercalculateResidual);
		computeMatricesFromLights(width, height, position, horizontalAngle, verticalAngle, FoV, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, lightDirections, lightNumber, numberOfLights, calculateResidual, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
		);

		depthBiasMVP = biasMatrix * depthMVP;
		
			
		// Render the polygons
		renderPolygons(width, height, programID, lightInvDir, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
		
		// Optionally render the shadowmap (for debug only)
		//void renderShadowMap(GLuint quad_programID, GLuint depthTexture, GLuint texID, GLuint quad_vertexbuffer);
				
		/*if (calculateResidual) {
			double sum, gain = 0, bias = 0;
			sum = meanSquaredError(lightNumber, height, width, textureImages, sum, gain, bias);
			//computeResiduals(residual, residuals, totalResidual, residualImage, calculateResidual, depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, textureImages, lightNumber, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, modelPath);
			std::cout << "Light number: " << lightNumber << ", per-pixel residual = " << totalResidual << ", specular intensity = " << Roughness << ", specular power = " << Metallic << std::endl;
		}*/
		
		
		/*
		// TODO: Check if the viewpoint has been changed
		if (calculateResidual) {
			// Compute the residual image and sum of differences
			cv::Mat residualImage;
			meanSquaredError(lightNumber, height, width, textureImages, residualImage, residuals, residual, Roughness, Metallic, Distance, LightPower);
			
			specularMinimisation(Roughness, Metallic, Distance, LightPower, residual);
			calculateResidual = false;
		}
		*/
		
		//totalResidual = returnResidual(lightNumber, height, width, textureImages, residualImage, residuals, Roughness, Metallic, Distance, LightPower);
		//meanSquaredError(lightNumber, height, width, textureImages, residualImage, residual, totalResidual, Roughness, Metallic, Distance, LightPower);
		//residuals.push_back(residual);
		
		// Set up the only cost function (also known as residual). This uses auto-differentiation to obtain the derivative (jacobian).
		// AutoDiffCostFunction<CostFunctor, (Dimensions of Residual), (Dimensions of Variables)>
		//CostFunction* cost_function = new AutoDiffCostFunction<CostFunctor, 1, 2>(new CostFunctor);
		//problem.AddResidualBlock(cost_function, NULL, &Roughness, &Metallic, &Distance, &LightPower);
		
		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0); // Check if the ESC key was pressed or the window was closed
}

void RenderSynthetic(glm::mat4 depthProjectionMatrix, glm::mat4 depthViewMatrix, int width, int height, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::vec3& lightInvDir, cv::Mat lightDirections, std::vector<cv::Mat>& textureImages, int numberOfLights, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, GLuint programID, GLuint ModelMatrixID, GLuint ViewMatrixID, GLuint DepthBiasID, GLuint lightInvDirID, GLuint Texture, GLuint TextureID, GLuint depthTexture, GLuint ShadowMapID, GLuint vertexbuffer, GLuint uvbuffer, GLuint normalbuffer, GLuint elementbuffer, std::vector<unsigned int> indices, GLuint MatrixID, std::string modelPath) {
	for(int lightNumber = 0; lightNumber < numberOfLights; lightNumber++) {
		// Render the shadows
		glm::mat4 depthModelMatrix = glm::mat4(1.0);
		glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;
		
		// Compute the MVP matrix from keyboard and mouse input
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
		bool perspectiveProjection = false, shadowControl, calculateResidual = false;
		computeMatricesFromLights(width, height, position, horizontalAngle, verticalAngle, FoV, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, lightDirections, lightNumber, numberOfLights, calculateResidual, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
		MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;

		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0,
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
		);

		depthBiasMVP = biasMatrix * depthMVP;
		
		// Render the polygons
		renderPolygons(width, height, programID, lightInvDir, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
		
		// Create an empty Mat the same size as the image
		cv::Mat temp = cv::Mat(height, width, CV_8UC3);
		
		// Read the image into the Mat as an 8-bit colour image
		glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, temp.data);
		
		// 3D models have the origin in the bottom-left corner, while images have the origin in the top-left corner. The image is flipped to convert between the two.
		cv::flip(temp, temp, 0);

		//temp = addNoise(temp, 0.1);
		//temp = AddGaussianNoise_Opencv(temp, 0, 0.001);
		textureImages.push_back(temp);

		std::ostringstream a, b, c, d, e;
		a << lightNumber;
		b << Roughness;
		c << Metallic;
		d << Distance;
		e << LightPower;
		std::string imageNumber = a.str();
		std::string roughness   = b.str();
		std::string metallic    = c.str();
		std::string distance    = d.str();
		std::string lightPower  = e.str();

		//cv::imwrite(modelPath + "Roughness " + roughness + " Metallic " + metallic + " Distance " + distance + " Light Power " + lightPower + " " + imageNumber + ".png", temp);
		//cv::imwrite(modelPath + "Roughness " + roughness + " Metallic " + metallic + "." + imageNumber + ".png", temp);
		cv::imwrite(modelPath + " " + roughness + " " + metallic + " " + imageNumber + ".png", temp);
		//cv::imwrite(modelPath + imageNumber + ".png", temp);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	} //while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0); // Check if the ESC key was pressed or the window was closed
}

void computeMatricesFromLights(int windowWidth, int windowHeight, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::vec3& lightInvDir, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, bool& perspectiveProjection, cv::Mat lightDirections, int& lightNumber, int numberOfLights, bool& calculateResidual, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower) {
	
	calculateResidual = false;
	
	// Direction: Convert Spherical coordinates to Cartesian coordinates
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - M_PI / 2.0f),
		0,
		cos(horizontalAngle - M_PI / 2.0f)
	);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);
	
	float modifier = 1.0f;
	
	// Control modifiers
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		modifier = 0.1f;
	} else if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
		modifier = 0.01f;
	}
	
	
	// Change light direction
	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		if (glfwGetKey(window, GLFW_KEY_1) == GLFW_RELEASE) {
			if (lightNumber > 0) {
				lightNumber--;
			}
			else {
				lightNumber = numberOfLights - 1;
			}
			calculateResidual = true;
		}
	}
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		if (glfwGetKey(window, GLFW_KEY_2) == GLFW_RELEASE) {
			if (lightNumber < numberOfLights - 1) {
				lightNumber++;
			}
			else {
				lightNumber = 0;
			}
			calculateResidual = true;
		}
	}

	//lightInvDir = glm::vec3(lightDirections.at<float>(lightNumber, 0)+0.5, lightDirections.at<float>(lightNumber, 2), 0.5-lightDirections.at<float>(lightNumber, 1));
	lightInvDir = glm::vec3(lightDirections.at<float>(lightNumber, 0), lightDirections.at<float>(lightNumber, 1), lightDirections.at<float>(lightNumber, 2));
	
	
	// Modify specular power
	if (glfwGetKey(window, GLFW_KEY_7) == GLFW_PRESS) {
		//if (glfwGetKey(window, GLFW_KEY_7) == GLFW_RELEASE) {
			if (Metallic > 0.1*modifier) {
				Metallic -= 0.1*modifier;
				calculateResidual = true;
			}
		//}
	}
	else if (glfwGetKey(window, GLFW_KEY_8) == GLFW_PRESS) {
		//if (glfwGetKey(window, GLFW_KEY_8) == GLFW_RELEASE) {
			Metallic += 0.1*modifier;
			calculateResidual = true;
		//}
	}
	
	// Modify specular intensity
	if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS) {
		//if (glfwGetKey(window, GLFW_KEY_9) == GLFW_RELEASE) {
		if (Roughness > 0.1*modifier) {
			Roughness -= 0.1*modifier;
			calculateResidual = true;
		}
		//}
	}
	else if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS) {
		//if (glfwGetKey(window, GLFW_KEY_0) == GLFW_RELEASE) {
			Roughness += 0.1*modifier;
			calculateResidual = true;
		//}
	}

	float aspectRatio = float(windowWidth) / float(windowHeight);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		perspectiveProjection = true;
		calculateResidual = true;
	}
	else if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		perspectiveProjection = false;
		calculateResidual = true;
	}

	if (perspectiveProjection) // Projection matrix: 60° Field of View, 16:9 aspect ratio, display range: 0.1 units <-> 100 units
		ProjectionMatrix = glm::perspective(glm::radians(FoV), aspectRatio, 0.1f, 100.0f);
	else
		ProjectionMatrix = glm::ortho(-0.5f * aspectRatio, 0.5f * aspectRatio, -0.5f, 0.5f, 0.0f, 10.0f); // In world coordinates

	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,				// Camera is here
		position + direction,	// and looks here: at the same position, plus "direction"
		up						// Head is up (set to 0, -1, 0 to look upside-down)
	);


	/*if (shadowControl == true) {
		lightInvDir = position;
		//depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		depthViewMatrix = ViewMatrix;

		//depthProjectionMatrix = ProjectionMatrix;
		if (perspectiveProjection) // Projection matrix: 60° Field of View, 1:1 aspect ratio, display range: 0.1 units <-> 100 units
			depthProjectionMatrix = glm::perspective(glm::radians(FoV), aspectRatio, 0.1f, 100.0f);
		else
			depthProjectionMatrix = glm::ortho(-0.5f * aspectRatio, 0.5f * aspectRatio, -0.5f, 0.5f, 0.0f, 10.0f); // In world coordinates
	}*/
}

void computeMatricesFromInputs(int windowWidth, int windowHeight, glm::vec3& position, float& horizontalAngle, float& verticalAngle, float& FoV, float mouseSpeed, glm::mat4& ProjectionMatrix, glm::mat4& ViewMatrix, glm::vec3& lightInvDir, glm::mat4& depthProjectionMatrix, glm::mat4& depthViewMatrix, bool& perspectiveProjection, bool& shadowControl, GLuint& RoughnessID, double& Roughness, GLuint& MetallicID, double& Metallic, GLuint& DistanceID, double& Distance, GLuint& LightPowerID, double& LightPower, bool& calculateResidual) {

	calculateResidual = false;

	// Set the base movement speed
	float speed = 1.0f; // 1 unit per second

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	// Get the mouse position
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	// Reset the mouse position to the centre of the window for the next frame
	glfwSetCursorPos(window, windowWidth / 2, windowHeight / 2);

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(windowWidth / 2 - xpos);
	verticalAngle += mouseSpeed * float(windowHeight / 2 - ypos);

	// Direction: Convert Spherical coordinates to Cartesian coordinates
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - M_PI / 2.0f),
		0,
		cos(horizontalAngle - M_PI / 2.0f)
	);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);


	// Speed up movement
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		speed *= 2.5;
	}

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {			// Move forward
		position += direction * deltaTime * speed;
	}
	else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {	// Move backward
		position -= direction * deltaTime * speed;
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {			// Strafe right
		position += right * deltaTime * speed;
	}
	else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {	// Strafe left
		position -= right * deltaTime * speed;
	}

	if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {		// Move upward
		position += glm::vec3(0, 1, 0) * deltaTime * speed;
	}
	else if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) { // Move downward
		position -= glm::vec3(0, 1, 0) * deltaTime * speed;
	}

	// Change the FoV
	if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS) {
		FoV -= 1;
	} else if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
		FoV += 1;
	} else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
		FoV = 60.0f;
	}
	
	float aspectRatio = float(windowWidth) / float(windowHeight);

	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		perspectiveProjection = true;
	} else if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		perspectiveProjection = false;
	}

	if (perspectiveProjection) // Projection matrix: 60° Field of View, 16:9 aspect ratio, display range: 0.1 units <-> 100 units
		ProjectionMatrix = glm::perspective(glm::radians(FoV), aspectRatio, 0.1f, 100.0f);
	else
		ProjectionMatrix = glm::ortho(-0.5f * aspectRatio, 0.5f * aspectRatio, -0.5f, 0.5f, 0.0f, 10.0f); // In world coordinates

	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,				// Camera is here
		position + direction,	// and looks here: at the same position, plus "direction"
		up						// Head is up (set to 0, -1, 0 to look upside-down)
	);

	if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
		shadowControl = true;
	}
	else if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS) {
		shadowControl = false;
	}

	/*
	if (shadowControl == true) {
		lightInvDir = position;
		//depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
		depthViewMatrix = ViewMatrix;

		//depthProjectionMatrix = ProjectionMatrix;
		if (perspectiveProjection) // Projection matrix: 60° Field of View, 1:1 aspect ratio, display range: 0.1 units <-> 100 units
			depthProjectionMatrix = glm::perspective(glm::radians(FoV), aspectRatio, 0.1f, 100.0f);
		else
			depthProjectionMatrix = glm::ortho(-0.5f * aspectRatio, 0.5f * aspectRatio, -0.5f, 0.5f, 0.0f, 10.0f); // In world coordinates
	}
	*/
	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}

// http://www.cburch.com/cs/490/sched/feb8/index.html
void createSphere(int lats, int longs) {
	int i, j;
	for(i = 0; i <= lats; i++) {
		double lat0 = M_PI * (-0.5 + (double) (i - 1) / lats);
		double z0  = sin(lat0);
		double zr0 =  cos(lat0);
    
		double lat1 = M_PI * (-0.5 + (double) i / lats);
		double z1 = sin(lat1);
		double zr1 = cos(lat1);
    
		glBegin(GL_QUAD_STRIP);
		for(j = 0; j <= longs; j++) {
			double lng = 2 * M_PI * (double) (j - 1) / longs;
			double x = cos(lng);
			double y = sin(lng);
    
			glNormal3f(x * zr0, y * zr0, z0);
			glVertex3f(x * zr0, y * zr0, z0);
			glNormal3f(x * zr1, y * zr1, z1);
			glVertex3f(x * zr1, y * zr1, z1);
		}
		glEnd();
	}
}

// https://stackoverflow.com/a/39521983
/*void createSphere(int lats, int longs) {
	const float PI = 3.141592f;
	GLfloat x, y, z, alpha, beta; // Storage for coordinates and angles        
	GLfloat radius = 60.0f;
	int gradation = 20;

	for (alpha = 0.0; alpha < GL_PI; alpha += PI/gradation)
	{        
		glBegin(GL_TRIANGLE_STRIP);
		for (beta = 0.0; beta < 2.01*GL_PI; beta += PI/gradation)            
		{            
			x = radius*cos(beta)*sin(alpha);
			y = radius*sin(beta)*sin(alpha);
			z = radius*cos(alpha);
			glVertex3f(x, y, z);
			x = radius*cos(beta)*sin(alpha + PI/gradation);
			y = radius*sin(beta)*sin(alpha + PI/gradation);
			z = radius*cos(alpha + PI/gradation);            
			glVertex3f(x, y, z);            
		}        
		glEnd();
	}
}*/

void createMesh(cv::Mat cv_depth, cv::Mat cv_normals, std::vector<glm::vec3> & out_vertices, std::vector<glm::vec2> & out_uvs, std::vector<glm::vec3> & out_normals, std::vector<unsigned int> & out_indices) {
	
	std::cout << "Width: " << cv_normals.cols << ", Height: " << cv_normals.rows << std::endl;
	std::cout << "Creating mesh from depth map and normals:\n\n";

	flip(cv_normals, cv_normals, 0);
	flip(cv_depth, cv_depth, 0);

	int percentage = 0;

	for (int y = 0; y <= cv_normals.rows; y++) {
		for (int x = 0; x <= cv_normals.cols; x++) {
			out_vertices.push_back(glm::vec3(float(x - (cv_normals.cols / 2)) / cv_normals.rows, float(y - (cv_normals.rows / 2)) / cv_normals.rows, 0));
			//out_vertices.push_back(glm::vec3(float(x - (cv_normals.cols / 2)) / cv_normals.rows, float(y - (cv_normals.rows / 2)) / cv_normals.rows, -cv_depth.at<float>(y, x) / (cv_normals.rows * 10)));
			out_uvs.push_back(glm::vec2(float(x) / cv_normals.cols, 1.0f - (float(y) / cv_normals.rows)));
			/*
			if (y == cv_normals.rows)
				y--;
			if (x == cv_normals.cols)
				x--;
			
			out_normals.push_back(glm::vec3(cv_normals.at<cv::Vec3f>(y, x)[1], -cv_normals.at<cv::Vec3f>(y, x)[0], cv_normals.at<cv::Vec3f>(y, x)[2]));
			*/
			
			if (y == cv_normals.rows) {
				out_normals.push_back(glm::vec3(cv_normals.at<cv::Vec3f>(y-1, x)[1], -cv_normals.at<cv::Vec3f>(y-1, x)[0], cv_normals.at<cv::Vec3f>(y-1, x)[2]));
			} else if (x == cv_normals.cols) {
				out_normals.push_back(glm::vec3(cv_normals.at<cv::Vec3f>(y, x-1)[1], -cv_normals.at<cv::Vec3f>(y, x-1)[0], cv_normals.at<cv::Vec3f>(y, x-1)[2]));
			} else if (y == cv_normals.rows && x == cv_normals.cols) {
				out_normals.push_back(glm::vec3(cv_normals.at<cv::Vec3f>(y-1, x-1)[1], -cv_normals.at<cv::Vec3f>(y-1, x-1)[0], cv_normals.at<cv::Vec3f>(y-1, x-1)[2]));
			} else {
				out_normals.push_back(glm::vec3(cv_normals.at<cv::Vec3f>(y, x)[1], -cv_normals.at<cv::Vec3f>(y, x)[0], cv_normals.at<cv::Vec3f>(y, x)[2]));
				//out_normals.push_back(glm::vec3(0, 0, 1));
			}
			
			
		}
		/*
		if (((y * 100) / cv_normals.rows) > percentage) {
			percentage++;
			std::cout << percentage << "%\n";
		}
		*/
	}

	std::cout << "Mesh created with " << cv_normals.cols * cv_normals.rows * 2 << " polygons.\n\n";

	std::cout << "Indexing vertices:\n\n";

	percentage = 0;

	for (int y = 0; y < cv_normals.rows; y++) {
		for (int x = 0; x < cv_normals.cols; x++) {
			out_indices.push_back(int((y * cv_normals.cols) + x));
			out_indices.push_back(int((y * cv_normals.cols) + x + 1));
			out_indices.push_back(int(((y + 1) * cv_normals.cols) + x + 1));

			out_indices.push_back(int(((y + 1) * cv_normals.cols) + x + 1));
			out_indices.push_back(int(((y + 1) * cv_normals.cols) + x));
			out_indices.push_back(int((y * cv_normals.cols) + x));
		}
		/*
		if (((y * 100) / (cv_normals.rows - 1)) > percentage) {
			percentage++;
			std::cout << percentage << "%\n";
		}
		*/
	}

	std::cout << "Vertices indexed.\n";
}

GLuint loadMat(cv::Mat cv_texture) {

	// Actual RGB data
	unsigned char * data = cv_texture.data;
	unsigned int width  = cv_texture.cols;
	unsigned int height = cv_texture.rows;

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture: all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free this version
	//delete[] data;

	// Nearest neighbour filtering
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 

	// Trilinear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	// This requires mipmaps. Generate them automatically.
	glGenerateMipmap(GL_TEXTURE_2D);

	// Return the ID of the texture we just created
	return textureID;
}

// From https://codeyarns.com/2015/06/16/how-to-rotate-point-in-opencv/ 
cv::Mat rotatePoints(cv::Mat point, cv::Vec3d rvec) {
	cv::Mat rotatedPoints, pointsTransposed;

	cv::Mat rx = (cv::Mat_<float>(3, 3) << 1, 0, 0, 0, cos(rvec[0]), -sin(rvec[0]), 0, sin(rvec[0]), cos(rvec[0]));
	cv::Mat ry = (cv::Mat_<float>(3, 3) << cos(rvec[0]), 0, sin(rvec[0]), 0, 1, 0, -sin(rvec[0]), 0, cos(rvec[0]));
	cv::Mat rz = (cv::Mat_<float>(3, 3) << cos(rvec[0]), -sin(rvec[0]), 0, sin(rvec[0]), cos(rvec[0]), 0, 0, 0, 1);

	cv::transpose(point, pointsTransposed);

	rotatedPoints = rx * ry * rz * pointsTransposed;

	cv::transpose(rotatedPoints, point);

	return point;
}

/**
 * @param stage
 * @param printIfOK
 */
void checkForErrors(std::string stage, bool printIfOK) {
	bool retVal = false;
	const char * errorString = NULL;

	switch( glGetError() ) {
		case GL_NO_ERROR:
		retVal = true;
		errorString = "NO ERROR";
		break;

		case GL_INVALID_ENUM:
		errorString = "GL_INVALID_ENUM";
		break;

		case GL_INVALID_VALUE:
		errorString = "GL_INVALID_VALUE";
		break;

		case GL_INVALID_OPERATION:
		errorString = "GL_INVALID_OPERATION";
		break;

		case GL_STACK_OVERFLOW:
		errorString = "GL_STACK_OVERFLOW";
		break;

		case GL_STACK_UNDERFLOW:
		errorString = "GL_STACK_UNDERFLOW";
		break;

		case GL_OUT_OF_MEMORY:
		errorString = "GL_OUT_OF_MEMORY";
		break;

		case GL_INVALID_FRAMEBUFFER_OPERATION:
		errorString = "GL_INVALID_FRAMEBUFFER_OPERATION";
		break;

		case GL_TABLE_TOO_LARGE:
		errorString = "GL_TABLE_TOO_LARGE";
		break;

		default:
		errorString = "UNKNOWN";
		break;
	}

	if( !retVal ) {
		std::stringstream ss;
		ss << "ERROR at " << stage  << ": " << errorString;
		//Logger::log(ss.str(), Logger::ERROR);
		std::cout << ss.str() << std::endl;
	}
	else {
		if(printIfOK) {
			std::stringstream ss;
			ss << "NO ERROR at " << stage;
			//Logger::log(ss.str());
			std::cout << ss.str() << std::endl;
		}
	}
}

void calibrateLights(int& windowWidth, int& windowHeight, cv::Mat cv_texture) {
	// Initialise the GLFW library first
	if (!glfwInit()) {
		std::cerr << "Failed to initialise GLFW.\n";
		getchar();
		return;
	}
	std::cout << "Initialised GLFW.\n";

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Only needed for macOS
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Disable old OpenGL

	// Set the shadow resolution to 4096x4096
	int shadowResolution = 4096;

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(windowWidth, windowHeight, "Window", NULL, NULL);
	if (window == NULL) {
		std::cerr << "Failed to open GLFW window. Older Intel GPUs are not OpenGL 3.3 compatible.\n";
		getchar();
		glfwTerminate();
		return;
	}
	glfwMakeContextCurrent(window);
	std::cout << "Opened GLFW window.\n";

	// On macOS with a retina screen it will be windowWidth*2 and windowHeight*2, so the actual framebuffer size is:
	glfwGetFramebufferSize(window, &windowWidth, &windowHeight);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		std::cerr << "Failed to initialise GLEW.\n";
		getchar();
		glfwTerminate();
		return;
	}
	std::cout << "Initialised GLEW.\n";

	// Ensure that the escape key being pressed can be captured
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	// Hide the mouse and enable unlimited mouvement
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set the mouse at the center of the screen
	glfwPollEvents();
	//glfwSetCursorPos(window, windowWidth / 2, windowHeight / 2);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test, which stores fragments in the Z-buffer
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it is closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which do not have a normal facing towards the camera
	glEnable(GL_CULL_FACE);

	// Create a Vertex Array Object (VAO) and set it as the current one
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile the GLSL program from the shaders
	GLuint depthProgramID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/DepthRTT.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/DepthRTT.fragmentshader");

	// Get a handle for the "MVP" uniform
	GLuint depthMatrixID = glGetUniformLocation(depthProgramID, "depthMVP");
	
	// Load the texture
	//GLuint Texture = loadDDS("uvmap.DDS");
	GLuint Texture = loadMat(cv_texture);
	std::cout << "Texture loaded.\n\n";
	
	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/sphere.obj", vertices, uvs, normals);
	std::cout << "\nModel created.\n\n";

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	indexVBO(vertices, uvs, normals, indices, indexed_vertices, indexed_uvs, indexed_normals);

	// Load it into a VBO
	GLuint vertexbuffer; // This will identify the vertex buffer
	glGenBuffers(1, &vertexbuffer); // Generate 1 buffer, and put the resulting identifier in vertexbuffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer); // The following commands will talk about the 'vertexbuffer' buffer
	glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW); // Give the vertices to OpenGL

	GLuint uvbuffer;
	glGenBuffers(1, &uvbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_uvs.size() * sizeof(glm::vec2), &indexed_uvs[0], GL_STATIC_DRAW);

	GLuint normalbuffer;
	glGenBuffers(1, &normalbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);


	// ---------------------------------------------
	// Render to Texture - specific code begins here
	// ---------------------------------------------

	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	GLuint FramebufferName = 0;
	glGenFramebuffers(1, &FramebufferName);
	glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);

	// Depth texture. Slower than a depth buffer, but you can sample it later in your shader
	GLuint depthTexture;
	glGenTextures(1, &depthTexture);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0,GL_DEPTH_COMPONENT16, 1024, 1024, 0,GL_DEPTH_COMPONENT, GL_FLOAT, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		 
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTexture, 0);

	// No color output in the bound framebuffer, only depth.
	glDrawBuffer(GL_NONE);

	// Always check that our framebuffer is ok
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		return;

	
	// The quad's FBO. Used only for visualizing the shadowmap.
	static const GLfloat g_quad_vertex_buffer_data[] = { 
		-1.0f, -1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
	};

	GLuint quad_vertexbuffer;
	glGenBuffers(1, &quad_vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);

	// Create and compile our GLSL program from the shaders
	GLuint quad_programID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/Passthrough.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/SimpleTexture.fragmentshader");
	//std::cout << "Shaders loaded.\n";
	GLuint texID = glGetUniformLocation(quad_programID, "texture");
	//std::cout << "Uniform Location of texture found.\n";

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders("/home/thomas/Documents/Minimisation/apps/specular_estimation/src/ShadowMapping.vertexshader", "/home/thomas/Documents/Minimisation/apps/specular_estimation/src/ShadowMapping.fragmentshader");
	
	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");
	
	GLuint RoughnessID = glGetUniformLocation(programID, "roughness");
	GLuint MetallicID = glGetUniformLocation(programID, "metallic");
	GLuint DistanceID = glGetUniformLocation(programID, "distance");
	GLuint LightPowerID = glGetUniformLocation(programID, "lightPower");
	
	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");
	GLuint ViewMatrixID = glGetUniformLocation(programID, "V");
	GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
	GLuint DepthBiasID = glGetUniformLocation(programID, "DepthBiasMVP");
	GLuint ShadowMapID = glGetUniformLocation(programID, "shadowMap");
	
	// Get a handle for our "LightPosition" uniform
	GLuint lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");

	// Compute the MVP matrix from the light's point of view
	//lightInvDir = glm::vec3(0, 0, 1);
	//depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 1.0f, 100.0f);
	//depthViewMatrix = glm::lookAt(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));

	// Initial position: on +Z
	glm::vec3 position = glm::vec3(0, 0, 1);
	// Initial horizontal angle: toward -Z
	float horizontalAngle = M_PI;
	// Initial vertical angle: none
	float verticalAngle = 0.0f;
	// Initial Field of View
	float FoV = 60.0f;
	// Mouse Speed
	float mouseSpeed = 0.005f;
		
	bool perspectiveProjection = false, shadowControl = false, calculateResidual = false;
	int lightNumber = 0;

	glm::mat4 ViewMatrix, ProjectionMatrix;

	double Roughness = 0.5, Metallic = 2, Distance = 1.0, LightPower = 1.0;

	do {
		// Render to our framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
		glViewport(0, 0, windowWidth, windowHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		// We don't use bias in the shader, but instead we draw back faces, 
		// which are already separated from the front faces by a small distance 
		// (if your geometry is made this way)
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(depthProgramID);

		glm::vec3 lightInvDir = glm::vec3(0.5f,2,2);

		// Compute the MVP matrix from the light's point of view
		glm::mat4 depthProjectionMatrix = glm::ortho<float>(-10,10,-10,10,-10,20);
		glm::mat4 depthViewMatrix = glm::lookAt(lightInvDir, glm::vec3(0,0,0), glm::vec3(0,1,0));
		// or, for spot light :
		//glm::vec3 lightPos(5, 20, 20);
		//glm::mat4 depthProjectionMatrix = glm::perspective<float>(45.0f, 1.0f, 2.0f, 50.0f);
		//glm::mat4 depthViewMatrix = glm::lookAt(lightPos, lightPos-lightInvDir, glm::vec3(0,1,0));

		glm::mat4 depthModelMatrix = glm::mat4(1.0);
		glm::mat4 depthMVP = depthProjectionMatrix * depthViewMatrix * depthModelMatrix;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(depthMatrixID, 1, GL_FALSE, &depthMVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,  // The attribute we want to configure
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);



		// Render to the screen
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0,0,windowWidth,windowHeight); // Render on the whole framebuffer, complete from the lower left corner to the upper right

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		

		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs(windowWidth, windowHeight, position, horizontalAngle, verticalAngle, FoV, mouseSpeed, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, shadowControl, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower, calculateResidual);
		//glm::mat4 ProjectionMatrix = getProjectionMatrix();
		//glm::mat4 ViewMatrix = getViewMatrix();
		//computeMatricesFromLights(windowWidth, windowHeight, position, horizontalAngle, verticalAngle, FoV, ProjectionMatrix, ViewMatrix, lightInvDir, depthProjectionMatrix, depthViewMatrix, perspectiveProjection, lightDirections, lightNumber, numberOfLights, calculateResidual, RoughnessID, Roughness, MetallicID, Metallic, DistanceID, Distance, LightPowerID, LightPower);
		//ViewMatrix = glm::lookAt(glm::vec3(14,6,4), glm::vec3(0,1,0), glm::vec3(0,1,0));
		glm::mat4 ModelMatrix = glm::mat4(1.0);
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
		
		glm::mat4 biasMatrix(
			0.5, 0.0, 0.0, 0.0, 
			0.0, 0.5, 0.0, 0.0,
			0.0, 0.0, 0.5, 0.0,
			0.5, 0.5, 0.5, 1.0
		);

		glm::mat4 depthBiasMVP = biasMatrix*depthMVP;

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
		glUniformMatrix4fv(DepthBiasID, 1, GL_FALSE, &depthBiasMVP[0][0]);

		glUniform1f(RoughnessID, Roughness);
		glUniform1f(MetallicID,     Metallic);
		glUniform1f(DistanceID,          Distance);
		glUniform1f(LightPowerID,        LightPower);

		glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, Texture);
		// Set our "myTextureSampler" sampler to use Texture Unit 0
		glUniform1i(TextureID, 0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, depthTexture);
		glUniform1i(ShadowMapID, 1);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Index buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			indices.size(),    // count
			GL_UNSIGNED_SHORT, // type
			(void*)0           // element array buffer offset
		);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);


		// Optionally render the shadowmap (for debug only)

		// Render only on a corner of the window (or we we won't see the real rendering...)
		glViewport(0,0,512,512);

		// Use our shader
		glUseProgram(quad_programID);

		// Bind our texture in Texture Unit 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, depthTexture);
		// Set our "renderedTexture" sampler to use Texture Unit 0
		glUniform1i(texID, 0);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// Draw the triangle !
		// You have to disable GL_COMPARE_R_TO_TEXTURE above in order to see anything !
		//glDrawArrays(GL_TRIANGLES, 0, 6); // 2*3 indices starting at 0 -> 2 triangles
		glDisableVertexAttribArray(0);


		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &uvbuffer);
	glDeleteBuffers(1, &normalbuffer);
	glDeleteBuffers(1, &elementbuffer);
	glDeleteProgram(programID);
	glDeleteProgram(depthProgramID);
	glDeleteProgram(quad_programID);
	glDeleteTextures(1, &Texture);

	glDeleteFramebuffers(1, &FramebufferName);
	glDeleteTextures(1, &depthTexture);
	glDeleteBuffers(1, &quad_vertexbuffer);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();
}

#endif
