#include "specular_estimation.h"

void specularEstimation(std::string imageName, std::string calibration, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic, bool synthetic, bool denseSample);
std::vector<cv::Mat> charucoImageAlignment(std::string materialPath, std::string charucoPath, int imageScale, int numberOfLights, cv::Mat& perspectiveTransform);
/*void renderSynthetic(std::string imageName, std::string calibration, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic);
void denseRealSample(std::string imageName, std::string calibration, double Roughness, double Metallic);
void denseSyntheticSample(std::string imageName, std::string calibration, double Roughness, double Metallic);*/

int main(int argc, char** argv) {
	
	if (argc < 2) {
		std::cerr << "Enter the name of the object followed by the starting values for the roughness and metallicness.\n";
		return -1;
	}

	// Required for the Ceres solver
	google::InitGoogleLogging(argv[0]);
	
	// Create strings to store the model folder name and calibration folder name
	// By default, the name of the chrome sphere calibration images folder is "chrome". This will be overwritten if a second argument is provided.
	std::string imageName, calibration = "chrome";
	
	// Read the name of the model folder
	if (argc >= 2) {
		// Read the file name from the command line argument and convert it from a char array to a string
		const char *argument1 = argv[1];
		imageName = argument1;
	}

	double Roughness = 0.5, Metallic = 0.5, RoughnessSynthetic = 0.5, MetallicSynthetic = 0.5;
	bool synthetic = false, denseSample = false;

	if (argc >= 4) {
		const char *argument2 = argv[2], *argument3 = argv[3];
		std::stringstream RoughnessValue, MetallicValue;
		RoughnessValue << argument2;
		MetallicValue  << argument3;
		RoughnessValue >> Roughness;
		MetallicValue  >> Metallic;
	}
	if (argc >= 6) {
		synthetic = true;
		const char *argument4 = argv[4], *argument5 = argv[5];
		std::stringstream RoughnessSyntheticValue, MetallicSyntheticValue;
		RoughnessSyntheticValue << argument4;
		MetallicSyntheticValue  << argument5;
		RoughnessSyntheticValue >> RoughnessSynthetic;
		MetallicSyntheticValue  >> MetallicSynthetic;
	}

	specularEstimation(imageName, calibration, Roughness, Metallic, RoughnessSynthetic, MetallicSynthetic, synthetic, denseSample);

	return 0;
}

void specularEstimation(std::string imageName, std::string calibration, double Roughness, double Metallic, double RoughnessSynthetic = 0.5, double MetallicSynthetic = 0.5, bool synthetic = false, bool denseSample = false) {

	// Define the paths for the model images, calibration images and the albedo
	const std::string imagesPath	  = "/home/thomas/Documents/";
	const std::string folderPath	  = "2018-04-18";
	const std::string materialPath    = imagesPath + folderPath + "/" + imageName   + "/" + imageName   + ".";
	const std::string calibrationPath = imagesPath + folderPath + "/" + calibration + "/" + calibration + ".";
	const std::string charucoPath     = imagesPath + folderPath + "/charuco/charuco.";
	const std::string macbethPath	  = imagesPath + folderPath + "/macbeth/macbeth.";
	const std::string albedoPath      = materialPath + "albedo.png";
	const std::string normalPath      = materialPath + "normal.png";

	double LightDistance = 1.0, LightIntensity = 1.0, Gain = 1.0, Bias = 0.0;
	int numberOfLights = 6, width = 2091, height = 1626;

	cv::Mat residualImage, albedo, normalMap, heightMap;
	std::vector<cv::Mat> materialImages, calibrationImages;
	cv::Mat lightDirections = (cv::Mat_<float>(6,3) << 0.447712, 0.138562, 0.883377, 0.228758, -0.106536, 0.967636, 0.1, 0.0705882, 0.99248, 0.000653595, -0.0718954, 0.997412, -0.139216, -0.12549, 0.982279, -0.494771, 0.115033, 0.861376);

	if (!synthetic) {
		
		int originalWidth = width, originalHeight = height, imageScale = 1;
		width  /= imageScale;
		height /= imageScale;

		cv::Mat perspectiveTransform;
		materialImages = charucoImageAlignment(materialPath, charucoPath, imageScale, numberOfLights, perspectiveTransform);
		
		/*
		//std::cout << "Loading material images.\n";
		materialImages = loadMaterialImages(materialPath, originalWidth, originalHeight, width, height, imageScale);
		//std::cout << "Loading light source calibration images.\n";
		calibrationImages = loadImages(calibrationPath);
		numberOfLights = calibrationImages.size();
		*/

		//phoSte::photometricStereo PhotometricStereo(numberOfLights, materialPath, calibrationPath, charucoPath, macbethPath, imageName, calibration, 0);
    	phoSte::photometricStereo PhotometricStereo(numberOfLights, imageScale, materialImages, calibrationPath, macbethPath, imageName, calibration, 0);

		//PhotometricStereo.readImage(materialPath, calibrationPath, macbethPath);
		//cv::Mat calibrationMask = loadCalibrationMask(calibrationPath, height, width);
		//calibrationBoundingBox = getBoundingBox(calibrationMask);
		PhotometricStereo.getLightInformation(0);
		std::cout << "Got light information\n";
		PhotometricStereo.getPixelNormAndAlbedo(1);
		std::cout << "Got Normal and Albedo\n";

		normalMap = PhotometricStereo.outputNormalImage(1);
		std::cout << "Got Normal Map\n";
		cv::imshow("Normal Map", normalMap);
		//cv::waitKey(0);
		
		albedo = PhotometricStereo.outputAlbedoImage(1);
		std::cout << "Got Albedo\n";
		
		//cv::Mat normalAlbedo = PhotometricStereo.outputNormalWithAlbedo(1);
		//cv::imshow("Normal with Albedo", normalAlbedo);
		//cv::Mat heightMap = PhotometricStereo.getHeightMap(2, -0.1);
		//cv::imshow("Height Map", heightMap);
		//cv::imwrite(materialPath + "normal", result);
		heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
		std::cout << "Got Height Map\n";
		//albedo = cv::Mat::zeros(height, width, CV_8U);
		//std::cout << "Got Albedo\n";
		//cv::waitKey(0);

		//normalMap.convertTo(normalMap, CV_8UC1);
		//normalMap = convertImageToNormals(normalMap);
		
		cv::cvtColor(albedo, albedo, CV_GRAY2BGR);
		albedo.convertTo(albedo, CV_8UC3);
		cv::imshow("Albedo", albedo);
		cv::waitKey(0);

		width  = albedo.cols;
		height = albedo.rows;
	} else {
		albedo = cv::imread(albedoPath), normalMap = cv::imread(normalPath), heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
		normalMap = convertImageToNormals(normalMap);
		normalMap.convertTo(normalMap, CV_32FC3);
	}

	glm::vec3 position, lightInvDir;
	glm::mat4 depthProjectionMatrix, depthViewMatrix, depthModelMatrix = glm::mat4(1.0), depthMVP, ModelMatrix = glm::mat4(1.0), MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);
	float horizontalAngle, verticalAngle, FoV;
	GLuint programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, MetallicID, LightDistanceID, LightIntensityID;
	std::vector<unsigned int> indices;
	cv::Vec3d residual;
	int lightNumber = 0;
	bool calculateResidual = false, perspectiveProjection, shadowControl;
	double totalResidual, residualValue;

	


	std::vector<glm::vec3> lightInvDirs;
	for (int i = 0; i < numberOfLights; i++) {
		//lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), -lightDirections.at<float>(i, 2), lightDirections.at<float>(i, 1)));
		lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), lightDirections.at<float>(i, 1), lightDirections.at<float>(i, 2)));
	}
	
	//lightInvDir = glm::vec3(lightDirections.at<float>(0, 0), -lightDirections.at<float>(0, 2), lightDirections.at<float>(0, 1));
	lightInvDir = glm::vec3(lightDirections.at<float>(0, 0), lightDirections.at<float>(0, 1), lightDirections.at<float>(0, 2));

	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	if (synthetic)
		RenderSynthetic(depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, numberOfLights, RoughnessID, RoughnessSynthetic, MetallicID, MetallicSynthetic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);

	if (!denseSample) {
		specularMinimisation(Roughness, Metallic, Gain, Bias, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightIntensityID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages, synthetic, RoughnessSynthetic, MetallicSynthetic);
	} else {
		std::string outputFileName = imageName + ".txt";
		std::ofstream outputFile(outputFileName, std::ios::app);
		
		for (double roughness = 0; roughness <= 1; roughness+=0.01){
			for (double metallic = 0; metallic <= 1; metallic+=0.01){

				double residualValue = 0, sum = 0;
				
				for(int i = 0; i < numberOfLights; i++) {

					// Render the polygons
					renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, roughness, MetallicID, metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity);
					
					cv::Mat model = readPixels(height, width);
					cv::Mat photograph = materialImages[i];

					model.convertTo(model, CV_64FC3, Gain, Bias);
					photograph.convertTo(photograph, CV_64FC3);

					sum = meanSquaredError(model, photograph);
					
					// Swap buffers
					glfwSwapBuffers(window);
					glfwPollEvents();
				}

				residualValue = (sum/numberOfLights);

				//std::cout << "Specular Intensity = " << roughness_ << ", Specular Power = " << metallic_ << ", Residual = " << (sum/numberOfLights_) << ", SSD = " << (sum/numberOfLights_)*(sum/numberOfLights_) << std::endl;
				//if (outputFile.is_open()) {
					outputFile << roughness << "\t" << metallic << "\t" << residualValue << std::endl;
					//outputFile.close();
				//}
			}
			std::cout << roughness*100 << "\%" << std::endl;
		}

		outputFile.close();
	}

	//viewModel(residual, totalResidual, residualImage, calculateResidual, depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, lightNumber, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);
	
	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);

	return;
}

std::vector<cv::Mat> charucoImageAlignment(std::string materialPath, std::string charucoPath, int imageScale, int numberOfLights, cv::Mat& perspectiveTransform) {
	cv::Mat cameraMatrix, distortionCoefficients;
	calibrateCamera(charucoPath, cameraMatrix, distortionCoefficients);
	std::cout << "Camera parameters obtained.\n";
	
	std::vector<cv::Mat> materialImages = loadImages(materialPath, imageScale);
	materialImages = charucoAlignment(materialImages, numberOfLights, perspectiveTransform, cameraMatrix, distortionCoefficients);
	std::cout << "ChArUco alignment successful.\n";

	return materialImages;
}

/*
void renderSynthetic(std::string imageName, std::string calibration, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic) {

	// Define the paths for the model images, calibration images and the albedo
	const std::string imagesPath	  = "/home/thomas/Documents/";
	const std::string folderPath	  = "2018-09-05";
	const std::string materialPath       = imagesPath + folderPath + "/" + imageName + "/" + imageName + ".";
	const std::string calibrationPath = imagesPath + folderPath + "/" + calibration + "/" + calibration + ".";
	const std::string macbethPath	  = imagesPath + folderPath + "/macbeth/macbeth.";
	const std::string albedoPath      = materialPath + "albedo.png";
	const std::string normalPath      = materialPath + "normal.png";

	double LightDistance = 0.5, LightIntensity = 0.5;
	int numberOfLights = 6, width = 1092, height = 728;

	glm::vec3 position, lightInvDir;
	glm::mat4 depthProjectionMatrix, depthViewMatrix, depthModelMatrix = glm::mat4(1.0), depthMVP, ModelMatrix = glm::mat4(1.0), MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);
	float horizontalAngle, verticalAngle, FoV;
	GLuint programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, MetallicID, LightDistanceID, LightIntensityID;
	std::vector<unsigned int> indices;
	cv::Vec3d residual;
	int lightNumber = 0;
	bool calculateResidual = false, perspectiveProjection, shadowControl;
	double totalResidual, residualValue;
	cv::Mat residualImage, albedo = cv::imread(albedoPath), normalMap = cv::imread(normalPath), heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
	std::vector<cv::Mat> materialImages;

	normalMap = convertImageToNormals(normalMap);
	normalMap.convertTo(normalMap, CV_32FC3);

	cv::Mat lightDirections = (cv::Mat_<float>(6,3) << 0.447712, 0.138562, 0.883377, 0.228758, -0.106536, 0.967636, 0.1, 0.0705882, 0.99248, 0.000653595, -0.0718954, 0.997412, -0.139216, -0.12549, 0.982279, -0.494771, 0.115033, 0.861376);
	
	std::vector<glm::vec3> lightInvDirs;
	for (int i = 0; i < numberOfLights; i++)
		lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), -lightDirections.at<float>(i, 2), lightDirections.at<float>(i, 1)));

	lightInvDir = glm::vec3(lightDirections.at<float>(0, 0), -lightDirections.at<float>(0, 2), lightDirections.at<float>(0, 1));
	//lightInvDir = glm::vec3(0, 0, 1);

	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	RenderSynthetic(depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, numberOfLights, RoughnessID, RoughnessSynthetic, MetallicID, MetallicSynthetic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);
	
	specularMinimisation(Roughness, Metallic, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightIntensityID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages);

	//viewModel(residual, totalResidual, residualImage, calculateResidual, depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, lightNumber, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);
	
	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);
	
	return;
}

void denseRealSample(std::string imageName, std::string calibration, double Roughness, double Metallic) {

	// Define the paths for the model images, calibration images and the albedo
	const std::string imagesPath	  = "/home/thomas/Documents/";
	const std::string folderPath	  = "2018-08-31";
	const std::string materialPath       = imagesPath + folderPath + "/" + imageName   + "/" + imageName   + ".";
	const std::string calibrationPath = imagesPath + folderPath + "/" + calibration + "/" + calibration + ".";
	const std::string macbethPath	  = imagesPath + folderPath + "/macbeth/macbeth.";

	double LightDistance = 0.5, LightIntensity = 0.5;
	int numberOfLights = 6;

	std::vector<cv::Mat> materialImages;
	for (int i = 0; i < numberOfLights; i++) {
		std::ostringstream stm;
		stm << i;
		std::string indexString = stm.str();

		cv::Mat model = cv::imread(materialPath + indexString + ".png");
		materialImages.push_back(model);
	}

	int width  = materialImages[0].cols;
	int height = materialImages[0].rows;
	
	phoSte::photometricStereo A(numberOfLights, materialPath, calibrationPath, macbethPath, imageName, calibration, 0);
    A.readImage(materialPath, calibrationPath, macbethPath);
	//cv::Mat calibrationMask = loadCalibrationMask(calibrationPath, height, width);
	//calibrationBoundingBox = getBoundingBox(calibrationMask);
    A.getLightInformation(0);
	std::cout << "Got light information\n";
    A.getPixelNormAndAlbedo(1);
	std::cout << "Got Normal and Albedo\n";

    cv::Mat normalMap = A.outputNormalImage(1);
    cv::imshow("Normal Map", normalMap);
    cv::Mat albedo = A.outputAlbedoImage(1);
    cv::imshow("Albedo", albedo);
    cv::Mat normalAlbedo = A.outputNormalWithAlbedo(1);
    cv::imshow("Normal with Albedo", normalAlbedo);
    //cv::Mat heightMap = A.getHeightMap(2, -0.1);
    //cv::imshow("Height Map", heightMap);
    //cv::imwrite(materialPath + "normal", result);
	cv::Mat heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
    //cv::waitKey(0);


	
	cv::cvtColor(albedo, albedo, CV_GRAY2BGR);
	albedo.convertTo(albedo, CV_8UC3);


	glm::vec3 position, lightInvDir;
	glm::mat4 depthProjectionMatrix, depthViewMatrix, depthModelMatrix = glm::mat4(1.0), depthMVP, ModelMatrix = glm::mat4(1.0), MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);
	float horizontalAngle, verticalAngle, FoV;
	GLuint programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, MetallicID, LightDistanceID, LightIntensityID;
	std::vector<unsigned int> indices;
	cv::Vec3d residual;
	int lightNumber = 0;
	bool calculateResidual = false, perspectiveProjection, shadowControl;
	double totalResidual, residualValue;
	cv::Mat residualImage;

	//cv::Mat lightDirectionsInverted;
	cv::Mat lightDirections = (cv::Mat_<float>(6,3) << 0.447712, 0.138562, 0.883377, 0.228758, -0.106536, 0.967636, 0.1, 0.0705882, 0.99248, 0.000653595, -0.0718954, 0.997412, -0.139216, -0.12549, 0.982279, -0.494771, 0.115033, 0.861376);
	//cv::invert(lightDirections, lightDirectionsInverted, cv::DECOMP_SVD);

	std::vector<glm::vec3> lightInvDirs;
	for (int i = 0; i < numberOfLights; i++)
		lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), -lightDirections.at<float>(i, 2), lightDirections.at<float>(i, 1)));
	
	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	std::string outputFileName = imageName + ".txt";
	std::ofstream outputFile(outputFileName, std::ios::app);
	//std::cout << "Writing this to a file.\n";
	

	for (double roughness = 0; roughness <= 1; roughness+=0.01){
		for (double metallic = 0; metallic <= 1; metallic+=0.01){

			double residualValue = 0, sum = 0, gain = 1.0, bias = 0.0;
			
			for(int i = 0; i < numberOfLights; i++) {

				// Render the polygons
				renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, roughness, MetallicID, metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity);
				
				cv::Mat model = readPixels(height, width);
				cv::Mat photograph = materialImages[i];

				model.convertTo(model, CV_64FC3, gain, bias);
				photograph.convertTo(photograph, CV_64FC3);

				sum = meanSquaredError(model, photograph);
				
				// Swap buffers
				glfwSwapBuffers(window);
				glfwPollEvents();
			}

			residualValue = (sum/numberOfLights);

			//std::cout << "Specular Intensity = " << roughness_ << ", Specular Power = " << metallic_ << ", Residual = " << (sum/numberOfLights_) << ", SSD = " << (sum/numberOfLights_)*(sum/numberOfLights_) << std::endl;
			//if (outputFile.is_open()) {
				outputFile << roughness << "\t" << metallic << "\t" << residualValue << std::endl;
				//outputFile.close();
			//}
		}
		std::cout << roughness*100 << "\%" << std::endl;
	}

	outputFile.close();

	//specularMinimisation(Roughness, Metallic, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages);

	//viewModel(residual, totalResidual, residualImage, calculateResidual, depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, lightNumber, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);
	
	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);

	return;
}

void denseSyntheticSample(std::string imageName, std::string calibration, double Roughness, double Metallic) {

	// Define the paths for the model images, calibration images and the albedo
	const std::string imagesPath	  = "/home/thomas/Documents/";
	const std::string folderPath	  = "2018-09-05";
	const std::string materialPath       = imagesPath + folderPath + "/" + imageName   + "/" + imageName   + ".";
	const std::string calibrationPath = imagesPath + folderPath + "/" + calibration + "/" + calibration + ".";
	const std::string macbethPath	  = imagesPath + folderPath + "/macbeth/macbeth.";
	const std::string albedoPath      = materialPath + "albedo.png";
	const std::string normalPath      = materialPath + "normal.png";

	double LightDistance = 0.5, LightIntensity = 0.5;
	int numberOfLights = 6, width = 1092, height = 728;

	glm::vec3 position, lightInvDir;
	glm::mat4 depthProjectionMatrix, depthViewMatrix, depthModelMatrix = glm::mat4(1.0), depthMVP, ModelMatrix = glm::mat4(1.0), MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix;
	glm::mat4 biasMatrix(
		0.5, 0.0, 0.0, 0.0,
		0.0, 0.5, 0.0, 0.0,
		0.0, 0.0, 0.5, 0.0,
		0.5, 0.5, 0.5, 1.0
	);
	float horizontalAngle, verticalAngle, FoV;
	GLuint programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, MetallicID, LightDistanceID, LightIntensityID;
	std::vector<unsigned int> indices;
	cv::Vec3d residual;
	int lightNumber = 0;
	bool calculateResidual = false, perspectiveProjection, shadowControl;
	double totalResidual, residualValue;
	cv::Mat residualImage, albedo = cv::imread(albedoPath), normalMap = cv::imread(normalPath), heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
	std::vector<cv::Mat> materialImages;;

	normalMap = convertImageToNormals(normalMap);
	normalMap.convertTo(normalMap, CV_32FC3);

	//cv::Mat lightDirectionsInverted;
	cv::Mat lightDirections = (cv::Mat_<float>(6,3) << 0.447712, 0.138562, 0.883377, 0.228758, -0.106536, 0.967636, 0.1, 0.0705882, 0.99248, 0.000653595, -0.0718954, 0.997412, -0.139216, -0.12549, 0.982279, -0.494771, 0.115033, 0.861376);
	//cv::invert(lightDirections, lightDirectionsInverted, cv::DECOMP_SVD);

	std::vector<glm::vec3> lightInvDirs;
	for (int i = 0; i < numberOfLights; i++)
		lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), -lightDirections.at<float>(i, 2), lightDirections.at<float>(i, 1)));
	
	lightInvDir = glm::vec3(lightDirections.at<float>(0, 0), -lightDirections.at<float>(0, 2), lightDirections.at<float>(0, 1));

	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	RenderSynthetic(depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);

	std::string outputFileName = imageName + ".txt";
	std::ofstream outputFile(outputFileName, std::ios::app);
	//outputFile.open (imagesPath + folderPath + "/" + imageName + ".txt");
	//outputFile << "Writing this to a file.\n";
	

	for (double roughness = 0; roughness <= 1; roughness+=0.01){
		for (double metallic = 0; metallic <= 1; metallic+=0.01){

			double residualValue = 0, sum = 0, gain = 1.0, bias = 0.0;
			
			for(int i = 0; i < numberOfLights; i++) {

				// Render the polygons
				renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, roughness, MetallicID, metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity);
				
				cv::Mat model = readPixels(height, width);
				cv::Mat photograph = materialImages[i];

				model.convertTo(model, CV_64FC3, gain, bias);
				photograph.convertTo(photograph, CV_64FC3);

				sum = meanSquaredError(model, photograph);
				
				// Swap buffers
				glfwSwapBuffers(window);
				glfwPollEvents();
			}

			residualValue = (sum/numberOfLights);

			//std::cout << "Specular Intensity = " << roughness_ << ", Specular Power = " << metallic_ << ", Residual = " << (sum/numberOfLights_) << ", SSD = " << (sum/numberOfLights_)*(sum/numberOfLights_) << std::endl;
			//if (outputFile.is_open())
			outputFile << roughness << "\t" << metallic << "\t" << residualValue << std::endl;
		}
		std::cout << roughness*100 << "\%" << std::endl;
	}

	outputFile.close();

	//specularMinimisation(Roughness, Metallic, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages);

	//viewModel(residual, totalResidual, residualImage, calculateResidual, depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, lightNumber, numberOfLights, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);
	
	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);

	return;
}*/

/*
SpecularEstimation::SpecularEstimation(std::string imageName, std::string calibration, bool denseSample, double Roughness, double Metallic) {
	materialPath       = imagesPath + folderPath + "/" + imageName   + "/" + imageName   + ".";
	calibrationPath = imagesPath + folderPath + "/" + calibration + "/" + calibration + ".";
	
	PhotometricStereo();

	cv::cvtColor(albedo, albedo, CV_GRAY2BGR);
	albedo.convertTo(albedo, CV_8UC3);

	populateLightInvDirs();

	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	if (denseSample)
		DenseSample();
	else
		specularMinimisation(Roughness, Metallic, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightIntensityID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages);

	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);
}

SpecularEstimation::SpecularEstimation(std::string imageName, std::string calibration, bool denseSample, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic) {
	albedo    = cv::imread(albedoPath);
	heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
	normalMap = convertImageToNormals(cv::imread(normalPath));
	normalMap.convertTo(normalMap, CV_32FC3);

	populateLightInvDirs();

	initialiseOpenGL(heightMap, normalMap, albedo, lightDirections, width, height, depthProjectionMatrix, depthViewMatrix, position, horizontalAngle, verticalAngle, FoV, lightInvDir, programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, numberOfLights, calculateResidual, depthMVP, depthModelMatrix, MVP, ProjectionMatrix, ViewMatrix, ModelMatrix, depthBiasMVP, biasMatrix);
	
	RenderSynthetic(depthProjectionMatrix, depthViewMatrix, width, height, position, horizontalAngle, verticalAngle, FoV, lightInvDir, lightDirections, materialImages, numberOfLights, RoughnessID, RoughnessSynthetic, MetallicID, MetallicSynthetic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, materialPath);

	if (denseSample)
		DenseSample();
	else
		specularMinimisation(Roughness, Metallic, LightDistance, LightIntensity, imageName, residualValue, residual, totalResidual, residualImage, depthProjectionMatrix, depthViewMatrix, width, height, numberOfLights, RoughnessID, MetallicID, LightDistanceID, LightIntensityID, programID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MatrixID, ModelMatrix, MVP, ViewMatrix, depthBiasMVP, lightInvDirs, materialImages);

	terminateOpenGL(vertexbuffer, uvbuffer, normalbuffer, elementbuffer, programID, depthProgramID, quad_programID, Texture, FramebufferName, depthTexture, quad_vertexbuffer, VertexArrayID);
}

SpecularEstimation::~SpecularEstimation() {}

void SpecularEstimation::loadTextureImages() {
	for (int i = 0; i < numberOfLights; i++) {
		std::ostringstream stm;
		stm << i;
		std::string indexString = stm.str();

		cv::Mat model = cv::imread(materialPath + indexString + ".png");
		materialImages.push_back(model);
	}

	width  = materialImages[0].cols;
	height = materialImages[0].rows;
}

void SpecularEstimation::PhotometricStereo() {
	phoSte::photometricStereo A(numberOfLights, materialPath, calibrationPath, macbethPath, imageName, calibration, 0);

	A.readImage(materialPath, calibrationPath, macbethPath);
	//cv::Mat calibrationMask = loadCalibrationMask(calibrationPath, height, width);
	//calibrationBoundingBox = getBoundingBox(calibrationMask);
	A.getLightInformation(0);
	std::cout << "Got light information\n";
	A.getPixelNormAndAlbedo(1);
	std::cout << "Got Normal and Albedo\n";

	normalMap = A.outputNormalImage(1);
	cv::imshow("Normal Map", normalMap);
	albedo = A.outputAlbedoImage(1);
	cv::imshow("Albedo", albedo);
	normalAlbedo = A.outputNormalWithAlbedo(1);
	cv::imshow("Normal with Albedo", normalAlbedo);
	//cv::Mat heightMap = A.getHeightMap(2, -0.1);
	//cv::imshow("Height Map", heightMap);
	//cv::imwrite(materialPath + "normal", result);
	heightMap = cv::Mat::zeros(albedo.size(), CV_8U);
	//cv::waitKey(0);

	//normalMap.convertTo(normalMap, CV_8UC1);
	//normalMap = convertImageToNormals(normalMap);
}

void SpecularEstimation::populateLightInvDirs() {
	for (int i = 0; i < numberOfLights; i++)
		lightInvDirs.push_back(glm::vec3(lightDirections.at<float>(i, 0), -lightDirections.at<float>(i, 2), lightDirections.at<float>(i, 1)));
}

void SpecularEstimation::DenseSample() {
	std::ofstream results;
	results.open (imagesPath + folderPath + "/" + imageName + ".txt");

	for (Roughness = 0; Roughness <= 1; Roughness+=0.01){
		for (Metallic = 0; Metallic <= 1; Metallic+=0.01){

			double residualValue = 0, sum = 0, gain = 1.0, bias = 0.0;
					
			for(int i = 0; i < numberOfLights; i++) {

				// Render the polygons
				renderPolygons(width, height, programID, lightInvDirs[i], MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, indices, MVP, ModelMatrix, ViewMatrix, depthBiasMVP, RoughnessID, Roughness, MetallicID, Metallic, LightDistanceID, LightDistance, LightIntensityID, LightIntensity);
				
				cv::Mat model = readPixels(height, width);
				cv::Mat photograph = materialImages[i];

				model.convertTo(model, CV_64FC3, gain, bias);
				photograph.convertTo(photograph, CV_64FC3);

				sum = meanSquaredError(model, photograph);
						
				// Swap buffers
				glfwSwapBuffers(window);
				glfwPollEvents();
			}

			residualValue = (sum/numberOfLights);

			//std::cout << "Specular Intensity = " << roughness_ << ", Specular Power = " << metallic_ << ", Residual = " << (sum/numberOfLights_) << ", SSD = " << (sum/numberOfLights_)*(sum/numberOfLights_) << std::endl;
			results << Roughness << "\t" << Metallic << "\t" << residualValue << std::endl;
		}
	}

	results.close();
}
*/