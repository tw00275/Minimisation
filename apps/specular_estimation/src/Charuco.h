#pragma once
#ifndef CHARUCO_H
#define CHARUCO_H

//#include </user/HS222/tw00275/opencv-bin/include/opencv2/opencv.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/core.hpp> // Core contains the definitions of the basic building blocks of the OpenCV library
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/highgui.hpp> // HighGUI contains the functions for input and output operations
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/imgproc.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/tracking.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/aruco.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/aruco/charuco.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp> // Core contains the definitions of the basic building blocks of the OpenCV library
#include <opencv2/highgui.hpp> // HighGUI contains the functions for input and output operations
#include <opencv2/imgproc.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/aruco/charuco.hpp>
#include <opencv2/calib3d.hpp>

void charucoCalibration(int width, int height, cv::Ptr<cv::aruco::CharucoBoard>& charucoBoard, cv::Mat& charucoBoardImage, cv::Mat& cameraMatrix, cv::Mat& distortionCoefficients, const std::string sourcePath, const std::string folderPath);
void charucoAlignment(std::string charucoPath, std::vector<cv::Mat>& charucoImages, std::vector<cv::Mat> materialImages, int numberOfLights, int width, int height, cv::Mat& rotationMatrix, cv::Mat lightDirections, cv::Mat& lightDirectionsPerspective, cv::Mat cameraMatrix, cv::Mat distortionCoefficients);
void charucoAlignment(std::vector<cv::Mat>& charucoImages, std::vector<cv::Mat> materialImages, int numberOfLights, int width, int height, cv::Mat& rotationMatrix, cv::Mat& rotationVector, cv::Mat& perspectiveTransform, cv::Mat lightDirections, cv::Mat& lightDirectionsPerspective, std::vector<cv::Vec3d>& rvecs, std::vector<cv::Vec3d>& tvecs, cv::Ptr<cv::aruco::CharucoBoard> charucoBoard, cv::Mat charucoBoardImage, cv::Mat cameraMatrix, cv::Mat distortionCoefficients);
std::vector<cv::Mat> charucoAlignment(std::vector<cv::Mat> materialImages, int numberOfLights, cv::Mat& rotationMatrix, cv::Mat& rotationVector, cv::Mat& perspectiveTransform, std::vector<cv::Vec3d>& rvecs, std::vector<cv::Vec3d>& tvecs, cv::Mat cameraMatrix, cv::Mat distortionCoefficients);
std::vector<cv::Mat> charucoAlignment(std::vector<cv::Mat> materialImages, int numberOfLights, cv::Mat& perspectiveTransform, cv::Mat cameraMatrix, cv::Mat distortionCoefficients);
cv::Mat createArucoMarker(int dictionaryId, int markerId, int borderBits, int markerSize, bool showMarker);
void detectArucoMarkers(int dictionaryId, bool showRejected, bool estimatePose, float markerLength, cv::Mat image, cv::Mat cameraMatrix, cv::Mat distCoeffs);
void createArucoDictionary(std::vector< cv::Mat >& arucoMarkers, int dictionaryId);
cv::Mat drawCharuco(cv::Ptr<cv::aruco::CharucoBoard> board, int rows, int cols, bool saveCharuco, bool displayCharuco);
void calibrateCamera(int squaresX, int squaresY, float squareLength, float markerLength, int dictionaryId, std::vector<cv::Mat> charucoImages, cv::Mat& cameraMatrix, cv::Mat& distCoeffs);
void calibrateCamera(std::string charucoPath, cv::Mat& cameraMatrix, cv::Mat& distortionCoefficients);
void loadCharucoImages(std::vector<cv::Mat>& charucoImages, std::string charucoPath, std::string charucoName);
void undistortCharuco(cv::Ptr<cv::aruco::CharucoBoard> board, cv::Mat inputImage, cv::Mat boardImage, cv::Mat& undistortedCharuco, cv::Mat cameraMatrix, cv::Mat distortionCoefficients, cv::Vec3d& rvec, cv::Vec3d& tvec, cv::Mat& perspectiveTransform, bool displayUndistortedCharuco);
std::vector<cv::Mat> loadCharucoImages(std::string path, int imageScale);

void charucoCalibration(int width, int height, cv::Ptr<cv::aruco::CharucoBoard>& charucoBoard, cv::Mat& charucoBoardImage, cv::Mat& cameraMatrix, cv::Mat& distortionCoefficients, const std::string imagesPath, const std::string folderPath) {

	std::vector<cv::Mat> charucoImages;
	const std::string charucoPath = imagesPath + folderPath + "/charuco/";
	
	cameraMatrix           = cv::imread(charucoPath + "cameraMatrix.png",           cv::IMREAD_COLOR);
	distortionCoefficients = cv::imread(charucoPath + "distortionCoefficients.png", cv::IMREAD_COLOR);
	if (!cameraMatrix.data && !distortionCoefficients.data) {
	
		loadCharucoImages(charucoImages, charucoPath, "charuco");

		// Load a premade dictionary of ArUco markers
		int dictionaryId = 0;
		cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

		// Create a ChArUco board
		int xSquares = 11;
		int ySquares = 9;
		float squareSideLength = 0.04f;
		float markerSideLength = 0.02f;
		charucoBoard = cv::aruco::CharucoBoard::create(xSquares, ySquares, squareSideLength, markerSideLength, dictionary);
		
		//charucoBoardImage = drawCharuco(charucoBoard, height, ((height * xSquares) / ySquares), false, false);

		// Rotate ChArUco board
		//cv::Mat tempImage = drawCharuco(charucoBoard, ((width * ySquares) / xSquares), width, false, false);
		//cv::rotate(tempImage, charucoBoardImage, cv::ROTATE_90_COUNTERCLOCKWISE);

		// Calibrate the camera using the ChArUco board images to obtain the camera matrix and distortion coefficients
		calibrateCamera(xSquares, ySquares, squareSideLength, markerSideLength, dictionaryId, charucoImages, cameraMatrix, distortionCoefficients);
		//calibrateCamera(xSquares, ySquares, squareSideLength, markerSideLength, dictionaryId, materialImages, cameraMatrix, distortionCoefficients);

		cv::imwrite(charucoPath + "cameraMatrix.png", cameraMatrix);
		cv::imwrite(charucoPath + "distortionCoefficients.png", distortionCoefficients);
	}
}

void charucoAlignment(std::vector<cv::Mat>& charucoImages, std::vector<cv::Mat> materialImages, int numberOfLights, int width, int height, cv::Mat& rotationMatrix, cv::Mat& rotationVector, cv::Mat& perspectiveTransform, cv::Mat lightDirections, cv::Mat& lightDirectionsPerspective, std::vector<cv::Vec3d>& rvecs, std::vector<cv::Vec3d>& tvecs, cv::Ptr<cv::aruco::CharucoBoard> charucoBoard, cv::Mat charucoBoardImage, cv::Mat cameraMatrix, cv::Mat distortionCoefficients) {
	
	// Create the rotation and translation vectors for the current ChArUco board image
	// Brings the calibration pattern from the model coordinate space (in which object points are specified) to the world coordinate space, the real position of the calibration pattern
	cv::Vec3d rvec, tvec;


	for (int i = 0; i < materialImages.size(); i++) {

		cv::Mat undistortedCharuco;

		if ((i % numberOfLights) == 0) { // Only estimate the pose for the first image in each board position

			// Obtain the pose of the ChArUco boards
			undistortCharuco(charucoBoard, materialImages[i], charucoBoardImage, undistortedCharuco, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, false);


			/*glm::vec3 eulerAngles(rvec[0] / (2 * M_PI), rvec[1] / (2 * M_PI), rvec[2] / (2 * M_PI));
			glm::quat quaternion = glm::quat(eulerAngles);
			glm::mat4 RotationMatrix = glm::toMat4(quaternion);

			std::cout << "Euler angle: x: " << eulerAngles[0] << " y: " << eulerAngles[1] << " z: " << eulerAngles[2] << std::endl;
			std::cout << "Rotation matrix: " << RotationMatrix[0][0] << RotationMatrix[0][1] << RotationMatrix[0][2] << RotationMatrix[0][3] << std::endl <<
				RotationMatrix[1][0] << RotationMatrix[1][1] << RotationMatrix[1][2] << RotationMatrix[1][3] << std::endl <<
				RotationMatrix[2][0] << RotationMatrix[2][1] << RotationMatrix[2][2] << RotationMatrix[2][3] << std::endl <<
				RotationMatrix[3][0] << RotationMatrix[3][1] << RotationMatrix[3][2] << RotationMatrix[3][3] << std::endl;*/


			// x: Red: Roll, y: Green: Pitch, z: Blue: Yaw
			// A full rotation is denoted by a value of 2 pi. A ChArUco board laying flat would have an x rotation of 0, while if it was standing upright it would have an x rotation of pi.
			std::cout << "rvec " << i + 1 << ": x = " << rvec[0] - M_PI << ", y = " << rvec[1] << ", z = " << rvec[2] << std::endl;

			// x: Horizontal displacement, y: Vertical displacement, z: Displacement into the screen
			std::cout << "tvec " << i + 1 << ": x = " << tvec[0] << ", y = " << tvec[1] << ", z = " << tvec[2] << std::endl << std::endl;

			std::cout << "Rotation " << i + 1 << ": x = " << (rvec[0] - M_PI) / (2 * M_PI) << ", y = " << rvec[1] / (2 * M_PI) << ", z = " << rvec[2] / (2 * M_PI) << std::endl << std::endl;

			//std::cout << "Perspective transformation matrix:" << std::endl;
			//std::cout << "[" << perspectiveTransform.at<double>(0,0) << ", " << perspectiveTransform.at<double>(1,0) << ", " << perspectiveTransform.at<double>(2,0) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,1) << ", " << perspectiveTransform.at<double>(1,1) << ", " << perspectiveTransform.at<double>(2,1) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,2) << ", " << perspectiveTransform.at<double>(1,2) << ", " << perspectiveTransform.at<double>(2,2) << "]" << std::endl << std::endl;

			//cv::Mat rotationVector;
			//cv::Rodrigues(perspectiveTransform, rotationVector);

			//std::cout << "Rotation vector " << i+1 << ": x = " << rotationVector.at<double>(0) << ", y = " << rotationVector.at<double>(1) << ", z = " << rotationVector.at<double>(2) << std::endl << std::endl;

			rotationVector = (cv::Mat_<double>(1, 3) << rvec[0], rvec[1], rvec[2]);
			cv::Rodrigues(rotationVector, rotationMatrix);

			std::cout << "[" << rotationMatrix.at<double>(0, 0) << ", " << rotationMatrix.at<double>(1, 0) << ", " << rotationMatrix.at<double>(2, 0) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 1) << ", " << rotationMatrix.at<double>(1, 1) << ", " << rotationMatrix.at<double>(2, 1) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 2) << ", " << rotationMatrix.at<double>(1, 2) << ", " << rotationMatrix.at<double>(2, 2) << "]" << std::endl << std::endl;
				
				

		} else {
			//estimatePoseFromCharuco(charucoBoard, materialImages[i], boardImage, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, true);

			cv::Mat estimatedPose = materialImages[i].clone();
			cv::aruco::drawAxis(estimatedPose, cameraMatrix, distortionCoefficients, rvec, tvec, 0.44f);
			//cv::imshow("Estimated Pose", estimatedPose);

			cv::warpPerspective(materialImages[i], undistortedCharuco, perspectiveTransform, charucoBoardImage.size());
			//cv::imshow("Undistorted ChArUco", materialImages[i]);
		}

		rvecs.push_back(rvec);
		tvecs.push_back(tvec);
		charucoImages.push_back(undistortedCharuco);

		i++;

		//cv::waitKey(0);
	}

}

std::vector<cv::Mat> charucoAlignment(std::vector<cv::Mat> materialImages, int numberOfLights, cv::Mat& rotationMatrix, cv::Mat& rotationVector, cv::Mat& perspectiveTransform, std::vector<cv::Vec3d>& rvecs, std::vector<cv::Vec3d>& tvecs, cv::Mat cameraMatrix, cv::Mat distortionCoefficients) {
	
	std::vector<cv::Mat> charucoImages;

	// Load a premade dictionary of ArUco markers
	int dictionaryId = 0;
	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));
	// Create a ChArUco board
	int xSquares = 11;
	int ySquares = 9;
	int height = materialImages[0].rows;
	int width  = ((height * xSquares) / ySquares);
	float squareSideLength = 0.04f;
	float markerSideLength = 0.02f;
	cv::Ptr<cv::aruco::CharucoBoard> charucoBoard = cv::aruco::CharucoBoard::create(xSquares, ySquares, squareSideLength, markerSideLength, dictionary);
	cv::Mat charucoBoardImage = drawCharuco(charucoBoard, height, width, false, false);

	// Create the rotation and translation vectors for the current ChArUco board image
	// Brings the calibration pattern from the model coordinate space (in which object points are specified) to the world coordinate space, the real position of the calibration pattern
	cv::Vec3d rvec, tvec;

	for (int i = 0; i < materialImages.size(); i++) {

		cv::Mat undistortedCharuco;

		if ((i % numberOfLights) == 0) { // Only estimate the pose for the first image in each board position

			// Obtain the pose of the ChArUco boards
			undistortCharuco(charucoBoard, materialImages[i], charucoBoardImage, undistortedCharuco, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, false);


			/*glm::vec3 eulerAngles(rvec[0] / (2 * M_PI), rvec[1] / (2 * M_PI), rvec[2] / (2 * M_PI));
			glm::quat quaternion = glm::quat(eulerAngles);
			glm::mat4 RotationMatrix = glm::toMat4(quaternion);

			std::cout << "Euler angle: x: " << eulerAngles[0] << " y: " << eulerAngles[1] << " z: " << eulerAngles[2] << std::endl;
			std::cout << "Rotation matrix: " << RotationMatrix[0][0] << RotationMatrix[0][1] << RotationMatrix[0][2] << RotationMatrix[0][3] << std::endl <<
				RotationMatrix[1][0] << RotationMatrix[1][1] << RotationMatrix[1][2] << RotationMatrix[1][3] << std::endl <<
				RotationMatrix[2][0] << RotationMatrix[2][1] << RotationMatrix[2][2] << RotationMatrix[2][3] << std::endl <<
				RotationMatrix[3][0] << RotationMatrix[3][1] << RotationMatrix[3][2] << RotationMatrix[3][3] << std::endl;*/


			// x: Red: Roll, y: Green: Pitch, z: Blue: Yaw
			// A full rotation is denoted by a value of 2 pi. A ChArUco board laying flat would have an x rotation of 0, while if it was standing upright it would have an x rotation of pi.
			std::cout << "rvec " << i + 1 << ": x = " << rvec[0] - M_PI << ", y = " << rvec[1] << ", z = " << rvec[2] << std::endl;

			// x: Horizontal displacement, y: Vertical displacement, z: Displacement into the screen
			std::cout << "tvec " << i + 1 << ": x = " << tvec[0] << ", y = " << tvec[1] << ", z = " << tvec[2] << std::endl << std::endl;

			std::cout << "Rotation " << i + 1 << ": x = " << (rvec[0] - M_PI) / (2 * M_PI) << ", y = " << rvec[1] / (2 * M_PI) << ", z = " << rvec[2] / (2 * M_PI) << std::endl << std::endl;

			//std::cout << "Perspective transformation matrix:" << std::endl;
			//std::cout << "[" << perspectiveTransform.at<double>(0,0) << ", " << perspectiveTransform.at<double>(1,0) << ", " << perspectiveTransform.at<double>(2,0) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,1) << ", " << perspectiveTransform.at<double>(1,1) << ", " << perspectiveTransform.at<double>(2,1) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,2) << ", " << perspectiveTransform.at<double>(1,2) << ", " << perspectiveTransform.at<double>(2,2) << "]" << std::endl << std::endl;

			//cv::Mat rotationVector;
			//cv::Rodrigues(perspectiveTransform, rotationVector);

			//std::cout << "Rotation vector " << i+1 << ": x = " << rotationVector.at<double>(0) << ", y = " << rotationVector.at<double>(1) << ", z = " << rotationVector.at<double>(2) << std::endl << std::endl;

			rotationVector = (cv::Mat_<double>(1, 3) << rvec[0], rvec[1], rvec[2]);
			cv::Rodrigues(rotationVector, rotationMatrix);

			std::cout << "[" << rotationMatrix.at<double>(0, 0) << ", " << rotationMatrix.at<double>(1, 0) << ", " << rotationMatrix.at<double>(2, 0) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 1) << ", " << rotationMatrix.at<double>(1, 1) << ", " << rotationMatrix.at<double>(2, 1) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 2) << ", " << rotationMatrix.at<double>(1, 2) << ", " << rotationMatrix.at<double>(2, 2) << "]" << std::endl << std::endl;
				
				

		} else {
			//estimatePoseFromCharuco(charucoBoard, materialImages[i], boardImage, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, true);

			cv::Mat estimatedPose = materialImages[i].clone();
			cv::aruco::drawAxis(estimatedPose, cameraMatrix, distortionCoefficients, rvec, tvec, 0.44f);
			//cv::imshow("Estimated Pose", estimatedPose);

			cv::warpPerspective(materialImages[i], undistortedCharuco, perspectiveTransform, charucoBoardImage.size());
			//cv::imshow("Undistorted ChArUco", materialImages[i]);
		}

		rvecs.push_back(rvec);
		tvecs.push_back(tvec);
		charucoImages.push_back(undistortedCharuco);

		i++;

		//cv::waitKey(0);
	}

	return charucoImages;
}

std::vector<cv::Mat> charucoAlignment(std::vector<cv::Mat> materialImages, int numberOfLights, cv::Mat& perspectiveTransform, cv::Mat cameraMatrix, cv::Mat distortionCoefficients) {

	std::vector<cv::Mat> charucoImages;

	// Load a premade dictionary of ArUco markers
	int dictionaryId = 0;
	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));
	// Create a ChArUco board
	int xSquares = 11;
	int ySquares = 9;
	int height = materialImages[0].rows;
	int width  = ((height * xSquares) / ySquares);
	float squareSideLength = 0.04f;
	float markerSideLength = 0.02f;
	cv::Ptr<cv::aruco::CharucoBoard> charucoBoard = cv::aruco::CharucoBoard::create(xSquares, ySquares, squareSideLength, markerSideLength, dictionary);
	cv::Mat charucoBoardImage = drawCharuco(charucoBoard, height, width, false, false), rotationMatrix, rotationVector;
	//cv::imwrite(imagesPath + "ChArUco board.png", charucoBoardImage);
	
	// Create the rotation and translation vectors for the current ChArUco board image
	// Brings the calibration pattern from the model coordinate space (in which object points are specified) to the world coordinate space, the real position of the calibration pattern
	cv::Vec3d rvec, tvec;
	std::vector<cv::Vec3d> rvecs, tvecs;

	for (int i = 0; i < materialImages.size(); i++) {

		std::ostringstream stm;
		stm << i;
		std::string indexString = stm.str();

		cv::Mat undistortedCharuco;

		if ((i % numberOfLights) == 0) { // Only estimate the pose for the first image in each board position

			// Obtain the pose of the ChArUco boards
			undistortCharuco(charucoBoard, materialImages[i], charucoBoardImage, undistortedCharuco, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, false);


			/*glm::vec3 eulerAngles(rvec[0] / (2 * M_PI), rvec[1] / (2 * M_PI), rvec[2] / (2 * M_PI));
			glm::quat quaternion = glm::quat(eulerAngles);
			glm::mat4 RotationMatrix = glm::toMat4(quaternion);

			std::cout << "Euler angle: x: " << eulerAngles[0] << " y: " << eulerAngles[1] << " z: " << eulerAngles[2] << std::endl;
			std::cout << "Rotation matrix: " << RotationMatrix[0][0] << RotationMatrix[0][1] << RotationMatrix[0][2] << RotationMatrix[0][3] << std::endl <<
				RotationMatrix[1][0] << RotationMatrix[1][1] << RotationMatrix[1][2] << RotationMatrix[1][3] << std::endl <<
				RotationMatrix[2][0] << RotationMatrix[2][1] << RotationMatrix[2][2] << RotationMatrix[2][3] << std::endl <<
				RotationMatrix[3][0] << RotationMatrix[3][1] << RotationMatrix[3][2] << RotationMatrix[3][3] << std::endl;*/


			// x: Red: Roll, y: Green: Pitch, z: Blue: Yaw
			// A full rotation is denoted by a value of 2 pi. A ChArUco board laying flat would have an x rotation of 0, while if it was standing upright it would have an x rotation of pi.
			std::cout << "rvec " << i + 1 << ": x = " << rvec[0] - M_PI << ", y = " << rvec[1] << ", z = " << rvec[2] << std::endl;

			// x: Horizontal displacement, y: Vertical displacement, z: Displacement into the screen
			std::cout << "tvec " << i + 1 << ": x = " << tvec[0] << ", y = " << tvec[1] << ", z = " << tvec[2] << std::endl << std::endl;

			std::cout << "Rotation " << i + 1 << ": x = " << (rvec[0] - M_PI) / (2 * M_PI) << ", y = " << rvec[1] / (2 * M_PI) << ", z = " << rvec[2] / (2 * M_PI) << std::endl << std::endl;

			//std::cout << "Perspective transformation matrix:" << std::endl;
			//std::cout << "[" << perspectiveTransform.at<double>(0,0) << ", " << perspectiveTransform.at<double>(1,0) << ", " << perspectiveTransform.at<double>(2,0) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,1) << ", " << perspectiveTransform.at<double>(1,1) << ", " << perspectiveTransform.at<double>(2,1) << "]" << std::endl << "[" << perspectiveTransform.at<double>(0,2) << ", " << perspectiveTransform.at<double>(1,2) << ", " << perspectiveTransform.at<double>(2,2) << "]" << std::endl << std::endl;

			//cv::Mat rotationVector;
			//cv::Rodrigues(perspectiveTransform, rotationVector);

			//std::cout << "Rotation vector " << i+1 << ": x = " << rotationVector.at<double>(0) << ", y = " << rotationVector.at<double>(1) << ", z = " << rotationVector.at<double>(2) << std::endl << std::endl;

			rotationVector = (cv::Mat_<double>(1, 3) << rvec[0], rvec[1], rvec[2]);
			cv::Rodrigues(rotationVector, rotationMatrix);

			std::cout << "[" << rotationMatrix.at<double>(0, 0) << ", " << rotationMatrix.at<double>(1, 0) << ", " << rotationMatrix.at<double>(2, 0) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 1) << ", " << rotationMatrix.at<double>(1, 1) << ", " << rotationMatrix.at<double>(2, 1) << "]" << std::endl << "[" << rotationMatrix.at<double>(0, 2) << ", " << rotationMatrix.at<double>(1, 2) << ", " << rotationMatrix.at<double>(2, 2) << "]" << std::endl << std::endl;
				
				

		} else {
			//estimatePoseFromCharuco(charucoBoard, materialImages[i], boardImage, cameraMatrix, distortionCoefficients, rvec, tvec, perspectiveTransform, true);

			cv::Mat estimatedPose = materialImages[i].clone();
			cv::aruco::drawAxis(estimatedPose, cameraMatrix, distortionCoefficients, rvec, tvec, 0.44f);
			//cv::imshow("Estimated Pose", estimatedPose);

			cv::warpPerspective(materialImages[i], undistortedCharuco, perspectiveTransform, charucoBoardImage.size());
			//cv::imshow("Undistorted ChArUco", materialImages[i]);
		}

		rvecs.push_back(rvec);
		tvecs.push_back(tvec);
		int borderWidth = height * 2.0/ySquares;
		int clearance = borderWidth * 0.1;
		int windowWidth = height * (ySquares-4.0)/(ySquares-2.0);
		int windowHeight = height * (ySquares-4.0)/(xSquares-2.0);
		//cv::Rect crop = cv::Rect(borderWidth, borderWidth, windowWidth, windowHeight);
		cv::Rect crop = cv::Rect(borderWidth + clearance, borderWidth + clearance, windowWidth - 2*clearance, windowHeight - 2*clearance);

		//std::cout << "ChArUco board width: " << width << ", ChArUco board height: " << height << ", Border width: " << borderWidth << ", Window width: " << windowWidth << ", Window height: " << windowHeight << std::endl;

		cv::Mat croppedWindow(undistortedCharuco, crop);
		croppedWindow.convertTo(croppedWindow, CV_32F);
		charucoImages.push_back(croppedWindow);

		//cv::imshow("Undistorted ChArUco", croppedWindow);
		//cv::waitKey(0);
	}

	return charucoImages;
}

void createArucoDictionary(std::vector<cv::Mat>& arucoMarkers, int dictionaryId) {
	//int dictionaryId = 0;  // dictionary: DICT_4X4_50=0, DICT_4X4_100=1, DICT_4X4_250=2, DICT_4X4_1000=3, DICT_5X5_50=4, DICT_5X5_100=5, DICT_5X5_250=6, DICT_5X5_1000=7, DICT_6X6_50=8, DICT_6X6_100=9, DICT_6X6_250=10, DICT_6X6_1000=11, DICT_7X7_50=12, DICT_7X7_100=13, DICT_7X7_250=14, DICT_7X7_1000=15, DICT_ARUCO_ORIGINAL = 16
	int borderBits = 1;    // Number of bits in marker borders
	int markerSize = 200;  // Marker size in pixels
	bool showMarker = false; // Show generated image
	int numberOfMarkers = 4;

	for (int markerId = 0; markerId < numberOfMarkers; markerId++) {
		cv::Mat arucoMarker = createArucoMarker(dictionaryId, markerId, borderBits, markerSize, showMarker);
		arucoMarkers.push_back(arucoMarker);
	}
}

cv::Mat drawCharuco(cv::Ptr<cv::aruco::CharucoBoard> board, int rows, int cols, bool saveCharuco, bool displayCharuco) {
	cv::Mat boardImage;
	cv::Size resolution = cv::Size(cols, rows);
	int margin = 0; // Margin between the edge of the ChArUco board and the edge of the image in pixels
	int markerBorder = 1; // Thickness of the border around the ArUco markers. Must be >= 1 pixels
	board->draw(resolution, boardImage, margin, markerBorder);

	if (saveCharuco) {
		cv::imwrite("ChArUco.png", boardImage);
	}

	if (displayCharuco) {
		cv::namedWindow("ChArUco Board", cv::WINDOW_NORMAL);
		cv::imshow("ChArUco Board", boardImage);
	}

	return boardImage;
}

void undistortCharuco(cv::Ptr<cv::aruco::CharucoBoard> board, cv::Mat inputImage, cv::Mat boardImage, cv::Mat& undistortedCharuco, cv::Mat cameraMatrix, cv::Mat distortionCoefficients, cv::Vec3d& rvec, cv::Vec3d& tvec, cv::Mat& perspectiveTransform, bool displayUndistortedCharuco = false) {
	std::vector<int> markerIds, detectedMarkerIds;
	std::vector<std::vector<cv::Point2f>> markerCorners, detectedMarkerCorners;
	cv::aruco::detectMarkers(inputImage, board->dictionary, detectedMarkerCorners, detectedMarkerIds);
	cv::aruco::detectMarkers(boardImage, board->dictionary, markerCorners, markerIds);

	cv::Mat imageCopy, boardCopy, undistorted, estimatedPose;
	//cv::undistort(inputImage, undistorted, cameraMatrix, distortionCoefficients);
	//undistorted.copyTo(imageCopy);
	//inputImage.copyTo(imageCopy);
	inputImage.copyTo(estimatedPose);
	//boardImage.copyTo(boardCopy);

	// if at least one marker is detected
	if (markerIds.size() > 0) {
		std::vector< cv::Point2f > charucoCorners, detectedCharucoCorners, matchedCharucoCorners;
		std::vector< int > charucoIds, detectedCharucoIds, matchedCharucoIds;
		cv::aruco::interpolateCornersCharuco(detectedMarkerCorners, detectedMarkerIds, estimatedPose, board, detectedCharucoCorners, detectedCharucoIds, cameraMatrix, distortionCoefficients);
		//cv::aruco::interpolateCornersCharuco(detectedMarkerCorners, detectedMarkerIds, estimatedPose, board, detectedCharucoCorners, detectedCharucoIds);
		cv::aruco::interpolateCornersCharuco(markerCorners, markerIds, boardImage, board, charucoCorners, charucoIds);

		//std::cout << "Number of ChArUco corners: " << charucoCorners.size() << ", Number of ChArUco marker IDs: " << charucoIds.size() << std::endl;
		//std::cout << "Number of detected ChArUco corners: " << detectedCharucoCorners.size() << ", Number of detected ChArUco marker IDs: " << detectedCharucoIds.size() << std::endl;
		std::cout << "Number of ChArUco corners: " << charucoCorners.size() << std::endl;
		std::cout << "Number of detected ChArUco corners: " << detectedCharucoCorners.size() << std::endl;

		cv::aruco::drawDetectedCornersCharuco(estimatedPose, detectedCharucoCorners, detectedCharucoIds, cv::Scalar(255, 0, 0));
		//cv::aruco::drawDetectedCornersCharuco(boardCopy, charucoCorners, charucoIds, cv::Scalar(255, 0, 0));

		if (displayUndistortedCharuco) {
			cv::namedWindow("ChArUco Board", cv::WINDOW_NORMAL);
			cv::imshow("ChArUco Board", boardImage);
		}

		/*cv::namedWindow( "Detected ChArUco Board", cv::WINDOW_NORMAL );
		cv::imshow("Detected ChArUco Board", estimatedPose);
		cv::imwrite("DetectedChArUcoBoard.png", estimatedPose);*/

		//cv::waitKey(0);

		// if at least one charuco corner is detected
		if (detectedCharucoIds.size() > 0) {
			for (unsigned int i = 0; i < charucoIds.size(); i++) {
				for (unsigned int j = 0; j < detectedCharucoIds.size(); j++) {
					if (charucoIds[i] == detectedCharucoIds[j]) {
						matchedCharucoIds.push_back(charucoIds[i]);
						matchedCharucoCorners.push_back(charucoCorners[i]);
					}
				}
			}

			//std::cout << "Number of matched ChArUco corners: " << matchedCharucoCorners.size() << ", Number of matched ChArUco marker IDs: " << matchedCharucoIds.size() << std::endl << std::endl;

			//sub-pixel refinement
			// use cornerSubPix()

			perspectiveTransform = cv::findHomography(detectedCharucoCorners, matchedCharucoCorners, cv::RANSAC);
			//cv::Mat affine = cv::estimateAffine2D(detectedCharucoCorners, matchedCharucoCorners);
			//cv::Mat affine = cv::estimateRigidTransform(detectedCharucoCorners, matchedCharucoCorners, true);

			cv::warpPerspective(inputImage, undistortedCharuco, perspectiveTransform, boardImage.size());
			//cv::warpAffine(inputImage, undistoredCharuco, affine, cv::Size());

			if (displayUndistortedCharuco) {
				cv::namedWindow("Undistorted ChArUco", cv::WINDOW_NORMAL);
				cv::imshow("Undistorted ChArUco", undistortedCharuco);
			}



			bool valid = cv::aruco::estimatePoseCharucoBoard(detectedCharucoCorners, detectedCharucoIds, board, cameraMatrix, distortionCoefficients, rvec, tvec);
			// if charuco pose is valid
			if (valid) {
				cv::aruco::drawAxis(estimatedPose, cameraMatrix, distortionCoefficients, rvec, tvec, 0.44f);
			}

			if (displayUndistortedCharuco) {
				cv::namedWindow("Estimated Pose", cv::WINDOW_NORMAL);
				cv::imshow("Estimated Pose", estimatedPose);
			}
		}
	}
}

void estimatePoseFromCharuco(cv::Ptr<cv::aruco::CharucoBoard> board, cv::Mat inputImage, cv::Mat boardImage, cv::Mat cameraMatrix, cv::Mat distortionCoefficients, cv::Vec3d rvec, cv::Vec3d tvec, cv::Mat perspectiveTransform, bool displayUndistortedCharuco) {
	std::vector< int > markerIds, detectedMarkerIds;
	std::vector< std::vector < cv::Point2f > > markerCorners, detectedMarkerCorners;
	cv::aruco::detectMarkers(inputImage, board->dictionary, detectedMarkerCorners, detectedMarkerIds);
	cv::aruco::detectMarkers(boardImage, board->dictionary, markerCorners, markerIds);

	cv::Mat imageCopy, boardCopy, undistorted, estimatedPose;
	//cv::undistort(inputImage, undistorted, cameraMatrix, distortionCoefficients);
	//undistorted.copyTo(imageCopy);
	//inputImage.copyTo(imageCopy);
	inputImage.copyTo(estimatedPose);
	//boardImage.copyTo(boardCopy);

	// if at least one marker is detected
	if (markerIds.size() > 0) {
		std::vector< cv::Point2f > charucoCorners, detectedCharucoCorners, matchedCharucoCorners;
		std::vector< int > charucoIds, detectedCharucoIds, matchedCharucoIds;
		cv::aruco::interpolateCornersCharuco(detectedMarkerCorners, detectedMarkerIds, estimatedPose, board, detectedCharucoCorners, detectedCharucoIds, cameraMatrix, distortionCoefficients);
		//cv::aruco::interpolateCornersCharuco(detectedMarkerCorners, detectedMarkerIds, estimatedPose, board, detectedCharucoCorners, detectedCharucoIds);
		cv::aruco::interpolateCornersCharuco(markerCorners, markerIds, boardImage, board, charucoCorners, charucoIds);

		//std::cout << "Number of ChArUco corners: " << charucoCorners.size() << ", Number of ChArUco marker IDs: " << charucoIds.size() << std::endl;
		//std::cout << "Number of detected ChArUco corners: " << detectedCharucoCorners.size() << ", Number of detected ChArUco marker IDs: " << detectedCharucoIds.size() << std::endl;
		std::cout << "Number of ChArUco corners: " << charucoCorners.size() << std::endl;
		std::cout << "Number of detected ChArUco corners: " << detectedCharucoCorners.size() << std::endl;

		cv::aruco::drawDetectedCornersCharuco(estimatedPose, detectedCharucoCorners, detectedCharucoIds, cv::Scalar(255, 0, 0));
		//cv::aruco::drawDetectedCornersCharuco(boardCopy, charucoCorners, charucoIds, cv::Scalar(255, 0, 0));

		cv::namedWindow("ChArUco Board", cv::WINDOW_NORMAL);
		cv::imshow("ChArUco Board", boardImage);

		/*cv::namedWindow( "Detected ChArUco Board", cv::WINDOW_NORMAL );
		cv::imshow("Detected ChArUco Board", estimatedPose);
		cv::imwrite("DetectedChArUcoBoard.png", estimatedPose);*/

		//cv::waitKey(0);

		// if at least one charuco corner is detected
		if (detectedCharucoIds.size() > 0) {
			for (unsigned int i = 0; i < charucoIds.size(); i++) {
				for (unsigned int j = 0; j < detectedCharucoIds.size(); j++) {
					if (charucoIds[i] == detectedCharucoIds[j]) {
						matchedCharucoIds.push_back(charucoIds[i]);
						matchedCharucoCorners.push_back(charucoCorners[i]);
					}
				}
			}

			//std::cout << "Number of matched ChArUco corners: " << matchedCharucoCorners.size() << ", Number of matched ChArUco marker IDs: " << matchedCharucoIds.size() << std::endl << std::endl;

			//sub-pixel refinement
			// use cornerSubPix()

			perspectiveTransform = cv::findHomography(detectedCharucoCorners, matchedCharucoCorners, cv::RANSAC);
			//cv::Mat affine = cv::estimateAffine2D(detectedCharucoCorners, matchedCharucoCorners);
			//cv::Mat affine = cv::estimateRigidTransform(detectedCharucoCorners, matchedCharucoCorners, true);

			cv::Mat undistoredCharuco;
			cv::warpPerspective(inputImage, undistoredCharuco, perspectiveTransform, cv::Size());
			//cv::warpAffine(inputImage, undistoredCharuco, affine, cv::Size());

			if (displayUndistortedCharuco) {
				cv::namedWindow("Undistorted ChArUco", cv::WINDOW_NORMAL);
				cv::imshow("Undistorted ChArUco", undistoredCharuco);
			}



			bool valid = cv::aruco::estimatePoseCharucoBoard(detectedCharucoCorners, detectedCharucoIds, board, cameraMatrix, distortionCoefficients, rvec, tvec);
			// if charuco pose is valid
			if (valid) {
				cv::aruco::drawAxis(estimatedPose, cameraMatrix, distortionCoefficients, rvec, tvec, 0.44f);
			}

			cv::namedWindow("Estimated Pose", cv::WINDOW_NORMAL);
			cv::imshow("Estimated Pose", estimatedPose);
		}
	}
}

void calibrateCamera(int squaresX, int squaresY, float squareLength, float markerLength, int dictionaryId, std::vector<cv::Mat> charucoImages, cv::Mat& cameraMatrix, cv::Mat& distortionCoefficients) {

	std::string outputFile = "cameraCalibration";

	bool showChessboardCorners = false;

	int calibrationFlags = 0;
	float aspectRatio = 1;

	bool zt = true; // Assume zero tangential distortion
	bool pc = true; // Fix the principal point at the center

	if (zt) calibrationFlags |= cv::CALIB_ZERO_TANGENT_DIST;
	if (pc) calibrationFlags |= cv::CALIB_FIX_PRINCIPAL_POINT;

	cv::Ptr<cv::aruco::DetectorParameters> detectorParams = cv::aruco::DetectorParameters::create();

	bool refindStrategy = false; // Apply refind strategy
	int camId = 0; // Camera id if input doesnt come from video (-v)
	cv::String video;

	cv::VideoCapture inputVideo;
	int waitTime = 0;

	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

	// create charuco board object
	cv::Ptr<cv::aruco::CharucoBoard> charucoboard = cv::aruco::CharucoBoard::create(squaresX, squaresY, squareLength, markerLength, dictionary);
	cv::Ptr<cv::aruco::Board> board = charucoboard.staticCast<cv::aruco::Board>();

	// collect data from each frame
	std::vector< std::vector< std::vector< cv::Point2f > > > allCorners;
	std::vector< std::vector< int > > allIds;
	std::vector< cv::Mat > allImgs;
	cv::Size imgSize;

	for (unsigned int i = 0; i < charucoImages.size(); i++) {
		cv::Mat imageCopy;

		std::vector< int > ids;
		std::vector< std::vector< cv::Point2f > > corners, rejected;

		// detect markers
		cv::aruco::detectMarkers(charucoImages[i], dictionary, corners, ids, detectorParams, rejected);

		// refind strategy to detect more markers
		if (refindStrategy) cv::aruco::refineDetectedMarkers(charucoImages[i], board, corners, ids, rejected);

		// interpolate charuco corners
		cv::Mat currentCharucoCorners, currentCharucoIds;
		if (ids.size() > 0) {
			cv::aruco::interpolateCornersCharuco(corners, ids, charucoImages[i], charucoboard, currentCharucoCorners, currentCharucoIds);
		}

		// draw results
		charucoImages[i].copyTo(imageCopy);
		if (ids.size() > 0) {
			cv::aruco::drawDetectedMarkers(imageCopy, corners);
		}

		if (currentCharucoCorners.total() > 0) {
			cv::aruco::drawDetectedCornersCharuco(imageCopy, currentCharucoCorners, currentCharucoIds);
		}

		//cv::putText(charucoImages[i], "Press 'c' to add current frame. 'ESC' to finish and calibrate", cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0), 2);

		//cv::namedWindow( "Camera Calibration", cv::WINDOW_NORMAL );
		//cv::imshow("Camera Calibration", imageCopy);

		if (ids.size() > 0) {
			allCorners.push_back(corners);
			allIds.push_back(ids);
			allImgs.push_back(charucoImages[i]);
			imgSize = charucoImages[i].size();
		}

		//cv::waitKey(0);
	}

	if (allIds.size() < 1) {
		std::cerr << "Not enough captures for calibration" << std::endl;
		return;
	}

	std::vector< cv::Mat > rvecs, tvecs;
	double repError;

	if (calibrationFlags & cv::CALIB_FIX_ASPECT_RATIO) {
		cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
		cameraMatrix.at< double >(0, 0) = aspectRatio;
	}

	// prepare data for calibration
	std::vector< std::vector< cv::Point2f > > allCornersConcatenated;
	std::vector< int > allIdsConcatenated;
	std::vector< int > markerCounterPerFrame;
	markerCounterPerFrame.reserve(allCorners.size());
	for (unsigned int i = 0; i < allCorners.size(); i++) {
		markerCounterPerFrame.push_back((int)allCorners[i].size());
		for (unsigned int j = 0; j < allCorners[i].size(); j++) {
			allCornersConcatenated.push_back(allCorners[i][j]);
			allIdsConcatenated.push_back(allIds[i][j]);
		}
	}

	// calibrate camera using aruco markers
	double arucoRepErr;
	arucoRepErr = cv::aruco::calibrateCameraAruco(allCornersConcatenated, allIdsConcatenated, markerCounterPerFrame, board, imgSize, cameraMatrix, distortionCoefficients, cv::noArray(), cv::noArray(), calibrationFlags);

	// prepare data for charuco calibration
	int nFrames = (int)allCorners.size();
	std::vector< cv::Mat > allCharucoCorners;
	std::vector< cv::Mat > allCharucoIds;
	std::vector< cv::Mat > filteredImages;
	allCharucoCorners.reserve(nFrames);
	allCharucoIds.reserve(nFrames);

	for (int i = 0; i < nFrames; i++) {
		// interpolate using camera parameters
		cv::Mat currentCharucoCorners, currentCharucoIds;
		cv::aruco::interpolateCornersCharuco(allCorners[i], allIds[i], allImgs[i], charucoboard, currentCharucoCorners, currentCharucoIds, cameraMatrix, distortionCoefficients);

		allCharucoCorners.push_back(currentCharucoCorners);
		allCharucoIds.push_back(currentCharucoIds);
		filteredImages.push_back(allImgs[i]);
	}

	if (allCharucoCorners.size() < 4) {
		std::cerr << "Not enough corners for calibration" << std::endl;
		return;
	}

	// calibrate camera using charuco
	repError = cv::aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, charucoboard, imgSize, cameraMatrix, distortionCoefficients, rvecs, tvecs, calibrationFlags);

	//saveCameraParams(outputFile, imgSize, aspectRatio, calibrationFlags, cameraMatrix, distortionCoefficients, repError);

	std::cout << "Rep Error: " << repError << std::endl;
	std::cout << "Rep Error Aruco: " << arucoRepErr << std::endl << std::endl;
	//std::cout << "Calibration saved to " << outputFile << std::endl;

	// show interpolated charuco corners for debugging
	if (showChessboardCorners) {
		for (unsigned int frame = 0; frame < filteredImages.size(); frame++) {
			cv::Mat imageCopy = filteredImages[frame].clone();
			if (allIds[frame].size() > 0) {

				if (allCharucoCorners[frame].total() > 0) {
					cv::aruco::drawDetectedCornersCharuco(imageCopy, allCharucoCorners[frame], allCharucoIds[frame]);
				}
			}

			cv::namedWindow("Interpolated ChArUco Markers", cv::WINDOW_NORMAL);
			cv::imshow("Interpolated ChArUco Markers", imageCopy);
		}
	}
}


void calibrateCamera(std::string charucoPath, cv::Mat& cameraMatrix, cv::Mat& distortionCoefficients) {

	// Load a premade dictionary of ArUco markers
	int dictionaryId = 0;
	//cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

	// Create a ChArUco board
	int squaresX = 11;
	int squaresY = 9;
	float squareLength = 0.04f;
	float markerLength = 0.02f;
	//cv::Ptr<cv::aruco::CharucoBoard> charucoBoard = cv::aruco::CharucoBoard::create(squaresX, squaresY, squareLength, markerLength, dictionary);

	std::cout << "Loading ChArUco images.\n";
	std::vector<cv::Mat> charucoImages = loadCharucoImages(charucoPath, 1);

	std::string outputFile = "cameraCalibration";

	bool showChessboardCorners = false;

	int calibrationFlags = 0;
	float aspectRatio = 1;

	bool zt = true; // Assume zero tangential distortion
	bool pc = true; // Fix the principal point at the center

	if (zt) calibrationFlags |= cv::CALIB_ZERO_TANGENT_DIST;
	if (pc) calibrationFlags |= cv::CALIB_FIX_PRINCIPAL_POINT;

	cv::Ptr<cv::aruco::DetectorParameters> detectorParams = cv::aruco::DetectorParameters::create();

	bool refindStrategy = false; // Apply refind strategy
	int camId = 0; // Camera id if input doesnt come from video (-v)
	cv::String video;

	cv::VideoCapture inputVideo;
	int waitTime = 0;

	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

	// create charuco board object
	cv::Ptr<cv::aruco::CharucoBoard> charucoboard = cv::aruco::CharucoBoard::create(squaresX, squaresY, squareLength, markerLength, dictionary);
	cv::Ptr<cv::aruco::Board> board = charucoboard.staticCast<cv::aruco::Board>();

	// collect data from each frame
	std::vector< std::vector< std::vector< cv::Point2f > > > allCorners;
	std::vector< std::vector< int > > allIds;
	std::vector< cv::Mat > allImgs;
	cv::Size imgSize;

	for (unsigned int i = 0; i < charucoImages.size(); i++) {
		cv::Mat imageCopy;

		std::vector< int > ids;
		std::vector< std::vector< cv::Point2f > > corners, rejected;

		// detect markers
		cv::aruco::detectMarkers(charucoImages[i], dictionary, corners, ids, detectorParams, rejected);

		// refind strategy to detect more markers
		if (refindStrategy) cv::aruco::refineDetectedMarkers(charucoImages[i], board, corners, ids, rejected);

		// interpolate charuco corners
		cv::Mat currentCharucoCorners, currentCharucoIds;
		if (ids.size() > 0) {
			cv::aruco::interpolateCornersCharuco(corners, ids, charucoImages[i], charucoboard, currentCharucoCorners, currentCharucoIds);
		}

		// draw results
		charucoImages[i].copyTo(imageCopy);
		if (ids.size() > 0) {
			cv::aruco::drawDetectedMarkers(imageCopy, corners);
		}

		if (currentCharucoCorners.total() > 0) {
			cv::aruco::drawDetectedCornersCharuco(imageCopy, currentCharucoCorners, currentCharucoIds);
		}

		//cv::putText(charucoImages[i], "Press 'c' to add current frame. 'ESC' to finish and calibrate", cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 0, 0), 2);

		//cv::namedWindow( "Camera Calibration", cv::WINDOW_NORMAL );
		//cv::imshow("Camera Calibration", imageCopy);

		if (ids.size() > 0) {
			allCorners.push_back(corners);
			allIds.push_back(ids);
			allImgs.push_back(charucoImages[i]);
			imgSize = charucoImages[i].size();
		}

		//cv::waitKey(0);
	}

	if (allIds.size() < 1) {
		std::cerr << "Not enough captures for calibration" << std::endl;
		return;
	}

	std::vector< cv::Mat > rvecs, tvecs;
	double repError;

	if (calibrationFlags & cv::CALIB_FIX_ASPECT_RATIO) {
		cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
		cameraMatrix.at< double >(0, 0) = aspectRatio;
	}

	// prepare data for calibration
	std::vector< std::vector< cv::Point2f > > allCornersConcatenated;
	std::vector< int > allIdsConcatenated;
	std::vector< int > markerCounterPerFrame;
	markerCounterPerFrame.reserve(allCorners.size());
	for (unsigned int i = 0; i < allCorners.size(); i++) {
		markerCounterPerFrame.push_back((int)allCorners[i].size());
		for (unsigned int j = 0; j < allCorners[i].size(); j++) {
			allCornersConcatenated.push_back(allCorners[i][j]);
			allIdsConcatenated.push_back(allIds[i][j]);
		}
	}

	// calibrate camera using aruco markers
	double arucoRepErr;
	arucoRepErr = cv::aruco::calibrateCameraAruco(allCornersConcatenated, allIdsConcatenated, markerCounterPerFrame, board, imgSize, cameraMatrix, distortionCoefficients, cv::noArray(), cv::noArray(), calibrationFlags);

	// prepare data for charuco calibration
	int nFrames = (int)allCorners.size();
	std::vector< cv::Mat > allCharucoCorners;
	std::vector< cv::Mat > allCharucoIds;
	std::vector< cv::Mat > filteredImages;
	allCharucoCorners.reserve(nFrames);
	allCharucoIds.reserve(nFrames);

	for (int i = 0; i < nFrames; i++) {
		// interpolate using camera parameters
		cv::Mat currentCharucoCorners, currentCharucoIds;
		cv::aruco::interpolateCornersCharuco(allCorners[i], allIds[i], allImgs[i], charucoboard, currentCharucoCorners, currentCharucoIds, cameraMatrix, distortionCoefficients);

		allCharucoCorners.push_back(currentCharucoCorners);
		allCharucoIds.push_back(currentCharucoIds);
		filteredImages.push_back(allImgs[i]);
	}

	if (allCharucoCorners.size() < 4) {
		std::cerr << "Not enough corners for calibration" << std::endl;
		return;
	}

	// calibrate camera using charuco
	repError = cv::aruco::calibrateCameraCharuco(allCharucoCorners, allCharucoIds, charucoboard, imgSize, cameraMatrix, distortionCoefficients, rvecs, tvecs, calibrationFlags);

	//saveCameraParams(outputFile, imgSize, aspectRatio, calibrationFlags, cameraMatrix, distortionCoefficients, repError);

	std::cout << "Rep Error: " << repError << std::endl;
	std::cout << "Rep Error Aruco: " << arucoRepErr << std::endl;
	//std::cout << "Calibration saved to " << outputFile << std::endl;

	// show interpolated charuco corners for debugging
	if (showChessboardCorners) {
		for (unsigned int frame = 0; frame < filteredImages.size(); frame++) {
			cv::Mat imageCopy = filteredImages[frame].clone();
			if (allIds[frame].size() > 0) {

				if (allCharucoCorners[frame].total() > 0) {
					cv::aruco::drawDetectedCornersCharuco(imageCopy, allCharucoCorners[frame], allCharucoIds[frame]);
				}
			}

			cv::namedWindow("Interpolated ChArUco Markers", cv::WINDOW_NORMAL);
			cv::imshow("Interpolated ChArUco Markers", imageCopy);
		}
	}
}


void loadCharucoImages(std::vector< cv::Mat >& charucoImages, std::string charucoPath, std::string charucoName) {
	bool loadedImages = false;
	int imageNumber = 0;

	// Load all of the calibration and model images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the calibration and model images of the current index
		cv::Mat charucoImage = cv::imread(charucoPath + charucoName + "." + indexString + ".png", cv::IMREAD_COLOR);

		if (!charucoImage.data) {  // Check if any images failed to load
			std::cout << imageNumber << " ChArUco images have been loaded." << std::endl << std::endl;
			loadedImages = true;
		}
		else {
			charucoImages.push_back(charucoImage);
		}
		imageNumber++;
	}

	if (imageNumber == 0) {
		std::cerr << "Error: No images could be loaded." << std::endl;
		return;
	}
}

cv::Mat createArucoMarker(int dictionaryId, int markerId, int borderBits, int markerSize, bool showMarker) {

	std::ostringstream markerNumber;
	markerNumber << markerId;

	std::string arucoFileName = "ArUcoMarker" + markerNumber.str() + ".png";
	//std::string arucoPath = "./ArUcoMarkers/" + arucoFileName;

	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

	cv::Mat markerImage;
	cv::aruco::drawMarker(dictionary, markerId, markerSize, markerImage, borderBits);

	cv::imwrite(arucoFileName, markerImage);

	if (showMarker) {
		cv::namedWindow("ArUco Marker", cv::WINDOW_NORMAL);
		cv::imshow("ArUco Marker", markerImage);
		cv::waitKey(0);
	}

	return markerImage;
}

void detectArucoMarkers(int dictionaryId, bool showRejected, bool estimatePose, float markerLength, cv::Mat image, cv::Mat cameraMatrix, cv::Mat distortionCoefficients) {

	cv::Ptr<cv::aruco::DetectorParameters> detectorParams = cv::aruco::DetectorParameters::create();

	detectorParams->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX; // do corner refinement in markers

	int camId = 0;

	cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));

	//double totalTime = 0;
	//int totalIterations = 0;

	cv::Mat imageCopy;

	//double tick = (double)cv::getTickCount();

	std::vector< int > ids;
	std::vector< std::vector < cv::Point2f > > corners, rejected;
	std::vector< cv::Vec3d > rvecs, tvecs;

	// detect markers and estimate pose
	cv::aruco::detectMarkers(image, dictionary, corners, ids, detectorParams, rejected);
	if (estimatePose && ids.size() > 0) {
		cv::aruco::estimatePoseSingleMarkers(corners, markerLength, cameraMatrix, distortionCoefficients, rvecs, tvecs);
	}

	/*double currentTime = ((double)cv::getTickCount() - tick) / cv::getTickFrequency();
	totalTime += currentTime;
	totalIterations++;
	if(totalIterations % 30 == 0) {
	std::cout << "Detection Time = " << currentTime * 1000 << " ms " << "(Mean = " << 1000 * totalTime / double(totalIterations) << " ms)" << std::endl;
	}*/

	// draw results
	image.copyTo(imageCopy);
	if (ids.size() > 0) {
		cv::aruco::drawDetectedMarkers(imageCopy, corners, ids);

		if (estimatePose) {
			for (unsigned int i = 0; i < ids.size(); i++) {
				cv::aruco::drawAxis(imageCopy, cameraMatrix, distortionCoefficients, rvecs[i], tvecs[i], markerLength * 0.5f);
			}
		}
	}

	if (showRejected && rejected.size() > 0) {
		cv::aruco::drawDetectedMarkers(image, rejected, cv::noArray(), cv::Scalar(100, 0, 255));
	}

	cv::namedWindow("Detected ArUco Markers", cv::WINDOW_NORMAL);
	cv::imshow("Detected ArUco Markers", imageCopy);
	cv::imwrite("DetectedArUcoMarkers.png", imageCopy);
}

std::vector<cv::Mat> loadCharucoImages(std::string path, int imageScale = 1) {

	std::vector<cv::Mat> images;
	bool loadedImages = false;
	int imageNumber = 0;

	// Load the images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the image of the current index
		cv::Mat image = cv::imread(path + indexString + ".png", cv::IMREAD_COLOR);

		if (!image.data) {  // Check if any images failed to load
			std::cout << imageNumber << " images have been loaded." << std::endl;
			loadedImages = true;
		}
		else {
			if (imageScale != 1) {
				cv::resize(image, image, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}
			images.push_back(image);
			imageNumber++;
			//std::cout << "Loaded image " << imageNumber << std::endl;
		}
	}

	if (imageNumber < 1) {
		std::cerr << "Error: No images could be loaded." << std::endl;
	}

	return images;
}

#endif
