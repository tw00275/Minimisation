#pragma once
#ifndef OPENCV_H
#define OPENCV_H

//#include </user/HS222/tw00275/opencv-bin/include/opencv2/opencv.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/core.hpp> // Core contains the definitions of the basic building blocks of the OpenCV library
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/highgui.hpp> // HighGUI contains the functions for input and output operations
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/imgproc.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/tracking.hpp>
//#include </user/HS222/tw00275/opencv-bin/include/opencv2/calib3d.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp> // Core contains the definitions of the basic building blocks of the OpenCV library
#include <opencv2/highgui.hpp> // HighGUI contains the functions for input and output operations
#include <opencv2/imgproc.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/calib3d.hpp>

void photometricStereo(std::string imageName, std::string calibration, std::string modelPath, int imageScale, cv::Mat& Z, cv::Mat& normals, cv::Mat& texture, cv::Mat& lightDirections, cv::Mat& lightDirectionsPerspective, std::vector< cv::Mat > & textureImages, std::vector< cv::Mat > & modelImages, std::vector< cv::Mat > calibrationImages, cv::Rect calibrationBoundingBox, int& width, int& height, std::vector< cv::Vec3d > rvecs, std::vector< cv::Vec3d > tvecs);
cv::Mat loadCalibrationMask(std::string calibrationPath, int& originalHeight, int& originalWidth);
//cv::Mat loadModelMask(std::string modelPath, int imageScale, int originalHeight, int originalWidth);
cv::Rect getBoundingBox(cv::Mat Mask);
//void cropImages(cv::Rect& modelBoundingBox, cv::Mat modelMask, int& croppedHeight, int& croppedWidth, int cropping, std::string modelPath, int imageScale);
//void loadImages(std::vector< cv::Mat >& calibrationImages, std::vector< cv::Mat >& modelImages, std::vector< cv::Mat >& textureImages, std::string calibrationPath, std::string modelPath, int imageScale, cv::Mat modelMask, cv::Rect modelBoundingBox, int originalWidth, int originalHeight, int width, int height, int croppedWidth, int croppedHeight);
void loadCalibrationImages(std::vector< cv::Mat >& calibrationImages, std::string calibrationPath, int width, int height);
void loadModelImages(std::vector< cv::Mat >& modelImages, std::vector< cv::Mat >& textureImages, std::string modelPath, int imageScale, int& originalWidth, int& originalHeight, int& width, int& height);
std::vector<cv::Mat> loadMaterialImages(std::string modelPath, int& originalWidth, int& originalHeight, int& width, int& height, int imageScale);
std::vector<cv::Mat> loadImages(std::string path, int imageScale);
cv::Mat getLightDirections(std::vector< cv::Mat > calibrationImages, cv::Rect calibrationBoundingBox);
cv::Vec3f getLightDirectionFromSphere(cv::Mat Image, cv::Rect boundingBox);
cv::Mat createTexture(std::string modelPath, int imageScale, std::vector< cv::Mat > textureImages, int croppedWidth, int croppedHeight);
cv::Mat getMedianImage(std::vector< cv::Mat > modelImages, int height, int width);
void getSurfaceNormals(cv::Mat& normals, cv::Mat& pGradients, cv::Mat& qGradients, cv::Mat lightDirectionsInverted, std::vector< cv::Mat > modelImages);
cv::Mat globalHeights(cv::Mat pGradients, cv::Mat qGradients);
void displayTexture(cv::Mat texture, std::string modelPath, bool saveTexture);
void displayNormalMap(cv::Mat normals, std::string modelPath, bool saveNormalMap);
void displayHeightMap(cv::Mat Z, std::string modelPath, bool saveHeightMap);
cv::Mat scaleImageIntensity(cv::Mat image);
void loadSurfaceNormals(cv::Mat lightDirections, int width, int height, int imageScale, std::string modelPath, cv::Mat& normals, cv::Mat& Z, std::vector< cv::Mat > modelImages);
cv::Mat convertImageToNormals(cv::Mat normalMap);
cv::Mat addNoise(cv::Mat temp, double standardDeviation);
cv::Mat addNoise(cv::Mat image);
cv::Mat AddGaussianNoise(const cv::Mat mSrc, double Mean, double StdDev);
cv::Mat AddGaussianNoise_Opencv(const cv::Mat mSrc, double Mean, double StdDev);
double sigma(cv::Mat& m, int i, int j, int block_size);
double cov(cv::Mat& m1, cv::Mat& m2, int i, int j, int block_size);
double eqm(cv::Mat& img1, cv::Mat& img2);
double psnr(cv::Mat& img_src, cv::Mat& img_compressed, int block_size);
double ssim(cv::Mat& img_src, cv::Mat& img_compressed, int block_size, bool show_progress);
void compute_quality_metrics(char * file1, char * file2, int block_size);
double getMSSIM(const cv::Mat& i1, const cv::Mat& i2);
cv::Mat contrastBrightness(cv::Mat image, double gain, double bias);

cv::Mat contrastBrightness(cv::Mat image, double gain = 1.0, double bias = 0.0) {
	cv::Mat new_image = cv::Mat::zeros( image.size(), CV_64FC3 );
    for ( int y = 0; y < image.rows; y++ ) {
        for ( int x = 0; x < image.cols; x++ ) {
            for ( int c = 0; c < image.channels(); c++ ) {
                new_image.at<cv::Vec3d>(y,x)[c] = cv::saturate_cast<double>( gain*image.at<cv::Vec3d>(y,x)[c] + bias );
            }
        }
    }
	return new_image;
}

/*cv::Mat contrastBrightness(cv::Mat image, double gain = 1.0, double bias = 0.0) {
	cv::Mat new_image = cv::Mat::zeros( image.size(), image.type() );
    for ( int y = 0; y < image.rows; y++ ) {
        for ( int x = 0; x < image.cols; x++ ) {
            for ( int c = 0; c < image.channels(); c++ ) {
                new_image.at<cv::Vec3b>(y,x)[c] = cv::saturate_cast<uchar>( gain*image.at<cv::Vec3b>(y,x)[c] + bias*255 );
            }
        }
    }
	return new_image;
}*/

inline unsigned char Clamp(int n)
{
    n = n>255 ? 255 : n;
    return n<0 ? 0 : n;
}

void photometricStereo(std::string imageName, std::string calibration, std::string modelPath, int imageScale, cv::Mat& Z, cv::Mat& normals, cv::Mat& texture, cv::Mat& lightDirections, cv::Mat& lightDirectionsPerspective, std::vector<cv::Mat>& textureImages, std::vector<cv::Mat>& modelImages, std::vector<cv::Mat> calibrationImages, cv::Rect calibrationBoundingBox, int& width, int& height, std::vector<cv::Vec3d> rvecs, std::vector<cv::Vec3d> tvecs) {
	
	// The lightDirections Mat stores the x, y, and z coordinates of each light source in float format
	lightDirections = getLightDirections(calibrationImages, calibrationBoundingBox);

	/*for (int i = 0; i < int(lightDirections.rows); i++) {
		cv::Mat rotatedLightDirections = rotatePointcos(lightDirections(cv::Rect(0, i, 3, 1)), rvecs[i]);

		lightDirectionsPerspective.at<float>(i, 0) = rotatedLightDirections.at<float>(i, 0);
		lightDirectionsPerspective.at<float>(i, 1) = rotatedLightDirections.at<float>(i, 1);
		lightDirectionsPerspective.at<float>(i, 2) = rotatedLightDirections.at<float>(i, 2);
	}

	lightDirections = lightDirectionsPerspective;*/

	texture = createTexture(modelPath, imageScale, textureImages, width, height);

	loadSurfaceNormals(lightDirections, width, height, imageScale, modelPath, normals, Z, modelImages);
	
	//normals = cv::Mat(height, width, CV_32FC3, cv::Scalar(0,0,1));
	//Z = cv::Mat(height, width, CV_32FC2, cv::Scalar::all(0));
	
	cv::GaussianBlur(Z, Z, cv::Size(3, 3), 0, 0);
	//cv::resize(Z, Z, cv::Size(), 0.5, 0.5, cv::INTER_LINEAR);
	//cv::resize(Z, Z, cv::Size(), 2, 2, cv::INTER_LINEAR);

	//flip(Z, Z, 0);

	// Show the mesh, texture, normal map and height map.
	// Setting the last argument to true writes these images to the image folder.
	// displayTexture needs to be executed before displayMesh if there is not already a texture file.
	//displayTexture(texture, modelPath, showTexture);
	//displayNormalMap(normals, modelPath, true);
	//displayHeightMap(Z, modelPath, true);
	//displayMesh(width, height, -Z, lightDirections, imageName, modelPath, texturePath, true, showTexture);

	// Wait for a keystroke before closing the window
	//cv::waitKey(0);
}

void loadImages(std::string imageName, std::string calibration, int& width, int& height, int imageScale, std::vector<cv::Mat>& textureImages, std::string calibrationPath, std::vector<cv::Mat>& calibrationImages, std::string modelPath, std::vector< cv::Mat > & modelImages, cv::Rect & calibrationBoundingBox) {

	// Obtain the height and width of the images prior to resizing
	int originalHeight;
	int originalWidth;

	cv::Mat calibrationMask = loadCalibrationMask(calibrationPath, originalHeight, originalWidth);
	//cv::Mat modelMask = loadModelMask(modelPath, imageScale, originalHeight, originalWidth);

	// Obtain the height and width of the images
	//height = calibrationMask.rows / imageScale;
	//width  = calibrationMask.cols / imageScale;

	// Obtain the bounding box that contains the chrome sphere. Removing unnecessary space reduces the computation required to determine the incident light direction in each image
	calibrationBoundingBox = getBoundingBox(calibrationMask);

	//cropImages(modelBoundingBox, modelMask, croppedHeight, croppedWidth, cropping, modelPath, imageScale);

	//loadImages(calibrationImages, modelImages, textureImages, calibrationPath, modelPath, imageScale, modelMask, modelBoundingBox, originalWidth, originalHeight, width, height, croppedWidth, croppedHeight);
	loadCalibrationImages(calibrationImages, calibrationPath, originalWidth, originalHeight);
	loadModelImages(modelImages, textureImages, modelPath, imageScale, originalWidth, originalHeight, width, height);
}

void loadSurfaceNormals(cv::Mat lightDirections, int width, int height, int imageScale, std::string modelPath, cv::Mat& normals, cv::Mat& Z, std::vector< cv::Mat > modelImages) {
	// Create Mats to store the light directions, surface normals, and p, q gradients
	cv::Mat lightDirectionsInverted;
	cv::invert(lightDirections, lightDirectionsInverted, cv::DECOMP_SVD);
	cv::Mat pGradients(height, width, CV_32F, cv::Scalar::all(0));
	cv::Mat qGradients(height, width, CV_32F, cv::Scalar::all(0));

	/*
	normals = cv::Mat(height, width, CV_32FC3, cv::Scalar::all(0));
	// Estimate the surface normals and p, q gradients
	getSurfaceNormals(normals, pGradients, qGradients, lightDirectionsInverted, modelImages);
	std::cout << "Surface normals have been estimated.\n\n";

	Z = cv::Mat(height, width, CV_32FC2, cv::Scalar::all(0));
	// global integration of surface normals
	Z = globalHeights(pGradients, qGradients);
	std::cout << "Height map has been created.\n\n";
	*/
	
	std::ostringstream stm;
	stm << imageScale;
	std::string scale = stm.str();
	
	std::string normalMapName = modelPath + "normalMap" + scale + ".yml";
	std::string pGradientsName = modelPath + "pGradients" + scale + ".yml";
	std::string qGradientsName = modelPath + "qGradients" + scale + ".yml";


	cv::FileStorage storageRead;
	storageRead.open(normalMapName, cv::FileStorage::READ);
	storageRead["normals"] >> normals;
	storageRead.release();
	/*
	storageRead.open(pGradientsName, cv::FileStorage::READ);
	storageRead["pGradients"] >> Z.at<cv::Vec2f>[0];
	storageRead.release();
	
	storageRead.open(qGradientsName, cv::FileStorage::READ);
	storageRead["qGradients"] >> Z.at<cv::Vec2f>[1];
	storageRead.release();
	*/
	cv::FileStorage storageWrite;
	
	if (!normals.data) {		
		normals = cv::Mat(height, width, CV_32FC3, cv::Scalar::all(0));
		//normals = cv::Mat(height, width, CV_32FC3, cv::Scalar(0,0,1));
		// Estimate the surface normals and p, q gradients
		getSurfaceNormals(normals, pGradients, qGradients, lightDirectionsInverted, modelImages);
		std::cout << "Surface normals have been estimated.\n\n";
		
		storageWrite.open(normalMapName, cv::FileStorage::WRITE);
		storageWrite << "normals" << normals;
	}
	storageWrite.release();
	/*
	if (!Z.data) {
		Z = cv::Mat(height, width, CV_32FC2, cv::Scalar::all(0));
		// global integration of surface normals
		Z = globalHeights(pGradients, qGradients);
		std::cout << "Height map has been created.\n\n";
		
		storageWrite.open(pGradientsName, cv::FileStorage::WRITE);
		storageWrite << "pGradients" << Z.at<cv::Vec2f>[0];
		storageWrite.release();
		
		storageWrite.open(qGradientsName, cv::FileStorage::WRITE);
		storageWrite << "qGradients" << Z.at<cv::Vec2f>[1];
		storageWrite.release();
	}
	*/
	
	Z = cv::Mat(height, width, CV_32FC2, cv::Scalar::all(0));
	// global integration of surface normals
	Z = globalHeights(pGradients, qGradients);
	std::cout << "Height map has been created.\n\n";
	
	
	/*
	normals = cv::imread(modelPath + "normalMap" + scale + ".png");

	if (!normals.data) {
		normals = cv::Mat(height, width, CV_32FC3, cv::Scalar::all(0));

		// Estimate the surface normals and p, q gradients
		getSurfaceNormals(normals, pGradients, qGradients, lightDirectionsInverted, modelImages);
		std::cout << "Surface normals have been estimated.\n\n";
		
		cv::imwrite(modelPath + "normalMap" + scale + ".png", normals);
	}

	Z = cv::imread(modelPath + "globalHeights" + scale + ".png");

	if (!Z.data) {
		Z = cv::Mat(height, width, CV_32FC2, cv::Scalar::all(0));

		// global integration of surface normals
		Z = globalHeights(pGradients, qGradients);
		std::cout << "Height map has been created.\n\n";
		
		cv::imwrite(modelPath + "globalHeights" + scale + ".png", Z);
	}
	*/
}

// Return an image comprised of the median intensities
// From https://stackoverflow.com/questions/2114797/compute-median-of-values-stored-in-vector-c
cv::Mat getMedianImage(std::vector<cv::Mat> modelImages, int height, int width) {
	cv::Mat medianImage(height, width, CV_8UC3);

	int percentage = 0;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {

			cv::Vec3b medianColour;

			for (int k = 0; k < 3; k++) { // For each colour channel

				std::vector< int > scores;

				for (int l = 0; l < modelImages.size(); l++) { // For each input image

					cv::Vec3b intensity = modelImages[l].at<cv::Vec3b>(i, j);

					scores.push_back(intensity.val[k]);
				}

				double median;
				size_t size = scores.size();

				sort(scores.begin(), scores.end());

				if (size % 2 == 0)
					median = (scores[size / 2 - 1] + scores[size / 2]) / 2;
				else
					median = scores[size / 2];

				medianColour[k] = uchar(median);
			}

			medianImage.at<cv::Vec3b>(i, j) = medianColour;
		}

		if (((i * 100) / height) > percentage) {
			percentage++;
			std::cout << percentage << "%\n";
		}
	}

	return medianImage;
}

cv::Mat scaleImageIntensity(cv::Mat image) {
	cv::Mat scaledImage;
	double minVal, maxVal;
	minMaxLoc(image, &minVal, &maxVal); //find minimum and maximum intensities
	image.convertTo(scaledImage, CV_8U, 255.0 / (maxVal - minVal), -minVal * 255.0 / (maxVal - minVal));
	return scaledImage;
}

cv::Mat globalHeights(cv::Mat pGradients, cv::Mat qGradients) {

	cv::Mat P(pGradients.rows, pGradients.cols, CV_32FC2, cv::Scalar::all(0));
	cv::Mat Q(pGradients.rows, pGradients.cols, CV_32FC2, cv::Scalar::all(0));
	cv::Mat Z(pGradients.rows, pGradients.cols, CV_32FC2, cv::Scalar::all(0));

	float lambda = 1.0f;
	float mu = 1.0f;

	cv::dft(pGradients, P, cv::DFT_COMPLEX_OUTPUT);
	cv::dft(qGradients, Q, cv::DFT_COMPLEX_OUTPUT);
	for (int i = 0; i < pGradients.rows; i++) {
		for (int j = 0; j < pGradients.cols; j++) {
			if (i != 0 || j != 0) {
				float u = sin((float)(i * 2 * CV_PI / pGradients.rows));
				float v = sin((float)(j * 2 * CV_PI / pGradients.cols));

				float uv = pow(u, 2) + pow(v, 2);
				float d = (1.0f + lambda)*uv + mu*pow(uv, 2);
				Z.at<cv::Vec2f>(i, j)[0] = ( u*P.at<cv::Vec2f>(i, j)[1] + v*Q.at<cv::Vec2f>(i, j)[1]) / d;
				Z.at<cv::Vec2f>(i, j)[1] = (-u*P.at<cv::Vec2f>(i, j)[0] - v*Q.at<cv::Vec2f>(i, j)[0]) / d;
			}
		}
	}

	// setting unknown average height to zero
	Z.at<cv::Vec2f>(0, 0)[0] = 0.0f;
	Z.at<cv::Vec2f>(0, 0)[1] = 0.0f;

	cv::dft(Z, Z, cv::DFT_INVERSE | cv::DFT_SCALE | cv::DFT_REAL_OUTPUT);

	return Z;
}

cv::Vec3f getLightDirectionFromSphere(cv::Mat Image, cv::Rect boundingBox) {

	const int THRESH = 254;
	const float radius = boundingBox.width / 2.0f;

	cv::Mat Binary;
	threshold(Image, Binary, THRESH, 255, CV_THRESH_BINARY);
	cv::Mat SubImage(Binary, boundingBox);

	// calculate center of pixels
	cv::Moments m = moments(SubImage, false);
	cv::Point center(int(m.m10 / m.m00), int(m.m01 / m.m00));

	// x,y are swapped here // TODO check if necessary
	float x = (center.x - radius) / radius;
	float y = (center.y - radius) / radius;
	float z = sqrt(1.0f - pow(x, 2.0f) - pow(y, 2.0f));

	return cv::Vec3f(x, y, z);
}

cv::Rect getBoundingBox(cv::Mat Mask) {

	std::vector< std::vector< cv::Point > > v;
	cv::findContours(Mask.clone(), v, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
	assert(v.size() > 0);
	return cv::boundingRect(v[0]);
}

// Estimate the surface normals and p, q gradients
void getSurfaceNormals(cv::Mat& normals, cv::Mat& pGradients, cv::Mat& qGradients, cv::Mat lightDirectionsInverted, std::vector<cv::Mat> modelImages) {

	std::cout << "\nEstimating Surface Normals.\n";

	int percentage = 0;
	
	for (int x = 0; x < modelImages[0].cols; x++) {
		for (int y = 0; y < modelImages[0].rows; y++) {
			
			// Create a Mat image to store the intensities
			cv::Mat Id = cv::Mat::zeros(int(modelImages.size()), 1, CV_32F);
			for (int i = 0; i < modelImages.size(); i++) {
				Id.at<float>(i) = float(modelImages[i].at<uchar>(cv::Point(x, y)));
			}

			// Premultiply the intensities with the inverted light directions matrix to obtain the surface normals multiplied by the albedo and the light intensity
			cv::Mat n = lightDirectionsInverted * Id;
			
			float p = float(sqrt(cv::Mat(n).dot(n)));
			if (p > 0) {
				n = n / p;
			}
			if (!n.at<float>(2, 0)) {
				n.at<float>(2, 0) = 1.0;
			}

			// avoid spikes at edges
			int legit = 1;
			for (int i = 0; i < modelImages.size(); i++) {
				legit *= (modelImages[i].at<uchar>(cv::Point(x, y)) >= 0);
			}
			if (legit) {
				normals.at<cv::Vec3f>(cv::Point(x, y)) = n;
				pGradients.at<float>(cv::Point(x, y)) = n.at<float>(0, 0) / n.at<float>(2, 0);
				qGradients.at<float>(cv::Point(x, y)) = n.at<float>(1, 0) / n.at<float>(2, 0);
			}
			else {
				cv::Vec3f nullvec(0.0f, 0.0f, 1.0f);
				normals.at<cv::Vec3f>(cv::Point(x, y)) = nullvec;
				pGradients.at<float>(cv::Point(x, y)) = 0.0f;
				qGradients.at<float>(cv::Point(x, y)) = 0.0f;
			}
		}

		if (((x * 100) / modelImages[0].cols) > percentage) {
			percentage++;
			std::cout << percentage << "%\n";
		}
	}
}
/*
void cropImages(cv::Rect& modelBoundingBox, cv::Mat modelMask, int& croppedHeight, int& croppedWidth, int cropping, std::string modelPath, int imageScale) {
	if (cropping == 1) {
		modelBoundingBox = getBoundingBox(modelMask);
		croppedHeight = modelBoundingBox.height;
		croppedWidth = modelBoundingBox.width;
	}
	else if (cropping == 2) { // Select ROI (Region of Interest)

		cv::Mat regionOfInterest = cv::imread(modelPath + "0.png", cv::IMREAD_COLOR);
		if (imageScale != 1) {
			cv::resize(regionOfInterest, regionOfInterest, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
		}

		croppedWidth = 0;
		croppedHeight = 0;

		bool showCrosshair = false;
		bool fromCenter = false;

		// Keep displaying the ROI selection until the user makes a valid selection
		while ((croppedWidth == 0) && (croppedHeight == 0)) {

			cv::namedWindow("Select the Region of Interest, then press the space bar to confirm. Press C to reset the selection.", cv::WINDOW_NORMAL);
			//cvSetWindowProperty("Select the Region of Interest, then press the space bar to confirm", CV_WND_PROP_AUTOSIZE, cv::WINDOW_FULLSCREEN);

			//cv::Rect2d modelBoundingBox = selectROI("Select Region of Interest, then press the space bar or Enter to confirm", regionOfInterest, fromCenter, showCrosshair);
			modelBoundingBox = selectROI("Select the Region of Interest, then press the space bar to confirm. Press C to reset the selection.", regionOfInterest, fromCenter, showCrosshair);
			cv::destroyWindow("Select the Region of Interest, then press the space bar to confirm. Press C to reset the selection.");

			croppedWidth = modelBoundingBox.width;
			croppedHeight = modelBoundingBox.height;
		}

		std::cout << std::endl << "The region of interest is from (" << modelBoundingBox.x << ", " << modelBoundingBox.y << ") to (" << modelBoundingBox.x + modelBoundingBox.width << ", " << modelBoundingBox.y + modelBoundingBox.height << ")." << std::endl;

		//std::cout << "This is " << (croppedWidth*croppedHeight*imageScale*100)/(width*height) << "% of the image area." << std::endl << std::endl;
	}
}
*/
/*void loadImages(std::vector< cv::Mat >& calibrationImages, std::vector< cv::Mat >& modelImages, std::vector< cv::Mat >& textureImages, std::string calibrationPath, std::string modelPath, int imageScale, cv::Mat modelMask, cv::Rect modelBoundingBox, int originalWidth, int originalHeight, int width, int height, int croppedWidth, int croppedHeight) {
	bool loadedImages = false;
	int imageNumber = 0;

	// Load all of the calibration and model images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the calibration and model images of the current index
		cv::Mat calibrationImage = cv::imread(calibrationPath + indexString + ".png", cv::IMREAD_GRAYSCALE);
		cv::Mat temporary = cv::imread(modelPath + indexString + ".png", cv::IMREAD_GRAYSCALE);
		cv::Mat texture = cv::imread(modelPath + indexString + ".png", cv::IMREAD_COLOR);

		if (!calibrationImage.data || !temporary.data || !texture.data) {  // Check if any images failed to load
			std::cout << imageNumber << " images have been loaded." << std::endl << std::endl;
			loadedImages = true;
		}
		else {

			if (imageScale != 1) {
				cv::resize(calibrationImage, calibrationImage, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
				cv::resize(temporary, temporary, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
				cv::resize(texture, texture, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}

			// Use the mask to remove the background from the model image
			cv::Mat model;
			temporary.copyTo(model, modelMask);

			// Add the calibration images, masked model images and model images to the corresponding vector Mats.
			calibrationImages.push_back(calibrationImage);
			cv::Mat croppedModel(model, modelBoundingBox);
			modelImages.push_back(croppedModel);
			cv::Mat croppedTexture(texture, modelBoundingBox);
			textureImages.push_back(croppedTexture);

			imageNumber++;
		}
	}

	if (imageNumber == 0) {
		std::cerr << "Error: No images could be loaded." << std::endl;
		return;
	}

	if (imageScale != 1) {
		std::cout << "Original image resolution = " << originalWidth << " x " << originalHeight << std::endl << "Scaled image resolution = " << width << " x " << height << std::endl;
	}
	else {
		std::cout << "Image resolution = " << width << " x " << height << std::endl;
	}

	if ((croppedHeight != height) || (croppedWidth != width)) {
		std::cout << "Cropped image resolution = " << croppedWidth << " x " << croppedHeight << std::endl;
	}

	std::cout << std::endl;
}*/

void loadCalibrationImages(std::vector< cv::Mat >& calibrationImages, std::string calibrationPath, int width, int height) {
	bool loadedImages = false;
	int imageNumber = 0;

	// Load all of the calibration and model images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the calibration and model images of the current index
		cv::Mat calibrationImage = cv::imread(calibrationPath + indexString + ".png", cv::IMREAD_GRAYSCALE);

		if (!calibrationImage.data) {  // Check if any images failed to load
			std::cout << imageNumber << " calibration images have been loaded." << std::endl;
			loadedImages = true;
		}
		else {

			// Add the calibration images, masked model images and model images to the corresponding vector Mats.
			calibrationImages.push_back(calibrationImage);

			imageNumber++;
		}
	}

	if (imageNumber == 0) {
		std::cerr << "Error: No calibration images could be loaded." << std::endl;
		return;
	}

	std::cout << "Calibration image resolution = " << width << " x " << height << std::endl;

	std::cout << std::endl;
}

void loadModelImages(std::vector<cv::Mat>& modelImages, std::vector<cv::Mat>& textureImages, std::string modelPath, int imageScale, int& originalWidth, int& originalHeight, int& width, int& height) {
	bool loadedImages = false;
	int imageNumber = 0;

	// Load all of the calibration and model images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the calibration and model images of the current index
		cv::Mat temporary = cv::imread(modelPath + indexString + ".png", cv::IMREAD_GRAYSCALE);
		cv::Mat texture = cv::imread(modelPath + indexString + ".png", cv::IMREAD_COLOR);

		if (!temporary.data || !texture.data) {  // Check if any images failed to load
			std::cout << imageNumber << " model images have been loaded." << std::endl;
			loadedImages = true;
		}
		else {

			if (imageScale > 1) {
				cv::resize(temporary, temporary, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
				cv::resize(texture, texture, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}

			modelImages.push_back(temporary);
			textureImages.push_back(texture);

			imageNumber++;

			std::cout << "Loaded model image " << imageNumber << std::endl;
		}
	}

	if (imageNumber == 0) {
		std::cerr << "Error: No model images could be loaded." << std::endl;
		return;
	}
	
	width  = textureImages[0].cols;
	height = textureImages[0].rows;
	
	if (imageScale > 1) {
		std::cout << "Original image resolution = " << originalWidth << " x " << originalHeight << std::endl << "Scaled image resolution = " << width << " x " << height << std::endl;
	}
	else {
		std::cout << "Image resolution = " << width << " x " << height << std::endl;
	}

	std::cout << std::endl;
}

std::vector<cv::Mat> loadMaterialImages(std::string modelPath, int& originalWidth, int& originalHeight, int& width, int& height, int imageScale = 1) {
	
	std::vector<cv::Mat> materialImages;
	bool loadedImages = false;
	int imageNumber = 0;

	// Load all of the calibration and model images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the calibration and model images of the current index
		cv::Mat materialImage = cv::imread(modelPath + indexString + ".png", cv::IMREAD_COLOR);

		if (!materialImage.data) {  // Check if any images failed to load
			std::cout << imageNumber << " model images have been loaded." << std::endl;
			loadedImages = true;
		}
		else {

			originalWidth  = materialImage.cols;
			originalHeight = materialImage.rows;

			if (imageScale > 1) {
				cv::resize(materialImage, materialImage, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}

			materialImages.push_back(materialImage);
			imageNumber++;
			std::cout << "Loaded material image " << imageNumber << std::endl;
		}
	}

	if (imageNumber < 1) {
		std::cerr << "Error: No material images could be loaded." << std::endl;
		return materialImages;
	}
	
	width  = materialImages[0].cols;
	height = materialImages[0].rows;
	
	if (imageScale > 1) {
		std::cout << "Original image resolution = " << originalWidth << " x " << originalHeight << std::endl << "Scaled image resolution = " << width << " x " << height << std::endl;
	}
	else {
		std::cout << "Image resolution = " << width << " x " << height << std::endl;
	}

	std::cout << std::endl;

	return materialImages;
}

std::vector<cv::Mat> loadImages(std::string path, int imageScale = 1) {

	std::vector<cv::Mat> images;
	bool loadedImages = false;
	int imageNumber = 0;

	// Load the images
	while (!loadedImages) {

		// The index of the loop is converted into a stringstream, then to the string called "indexString"
		// Using 'to_string' directly gives the error "'to_string' is not a member of 'std'"
		std::ostringstream stm;
		stm << imageNumber;
		std::string indexString = stm.str();

		// Read the image of the current index
		//cv::Mat image = cv::imread(path + indexString + ".png", cv::IMREAD_COLOR);
		cv::Mat image = cv::imread(path + indexString + ".png", cv::IMREAD_GRAYSCALE);

		if (!image.data) {  // Check if any images failed to load
			std::cout << imageNumber << " images have been loaded." << std::endl;
			loadedImages = true;
		}
		else {
			if (imageScale != 1) {
				cv::resize(image, image, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}
			images.push_back(image);
			imageNumber++;
			//std::cout << "Loaded image " << imageNumber << std::endl;
		}
	}

	if (imageNumber < 1) {
		std::cerr << "Error: No images could be loaded." << std::endl;
	}

	return images;
}

cv::Mat getLightDirections(std::vector<cv::Mat> calibrationImages, cv::Rect calibrationBoundingBox) {

	cv::Mat lightDirections(int(calibrationImages.size()), 3, CV_32F);

	// Determine the light direction from each image of the chrome sphere
	for (unsigned int i = 0; i < calibrationImages.size(); i++) {
		// Write the coordinates of the light directions into the lightDirections Mat
		cv::Vec3f light = getLightDirectionFromSphere(calibrationImages[i], calibrationBoundingBox);
		lightDirections.at<float>(i, 0) = light[0];
		lightDirections.at<float>(i, 1) = light[1];
		lightDirections.at<float>(i, 2) = light[2];

		std::cout << "Light " << i + 1 << " position: x = " << light[0] << ", y = " << light[1] << ", z = " << light[2] << std::endl;
	}

	std::cout << std::endl;

	return lightDirections;
}

cv::Mat createTexture(std::string modelPath, int imageScale, std::vector< cv::Mat > textureImages, int croppedWidth, int croppedHeight) {
	
	std::ostringstream stm;
	stm << imageScale;
	std::string scale = stm.str();
	
	bool textureExists = false;

	// If there is no imageName.texture.png file, then create the texture from a median image
	// Attempt to open the texture in /images/imageName/imageName.texture.png
	cv::Mat texture = cv::imread(modelPath + "texture" + scale + ".png", cv::IMREAD_COLOR);
	if (!texture.data) {  // Check if the texture failed to load
		std::cout << "The texture could not be found. A texture will be created by calculating the median image." << std::endl;
		textureExists = true;

		// Load the image containing the model illuminated with all lights and add it to the texture images vector.
		cv::Mat lightsImage = cv::imread(modelPath + "lights.png", cv::IMREAD_COLOR);
		if (lightsImage.data) {
			std::cout << "The fully illuminated object has been loaded from " << modelPath << "lights.png" << std::endl;
			if (imageScale != 1) {
				cv::resize(lightsImage, lightsImage, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_AREA);
			}
			//cv::Mat croppedLightsImage (lightsImage, modelBoundingBox);
			//textureImages.push_back(croppedLightsImage);
			textureImages.push_back(lightsImage);
		}

		texture = getMedianImage(textureImages, croppedHeight, croppedWidth);
		std::cout << "Median image has been calculated." << std::endl;
		cv::imwrite(modelPath + "texture" + scale + ".png", texture);
	}
	else {
		std::cout << "The texture has been loaded from " << modelPath << "texture.png" << std::endl;
	}

	return texture;
}

void displayTexture(cv::Mat texture, std::string modelPath, bool saveTexture) {
	cv::namedWindow("Texture", cv::WINDOW_NORMAL);
	cv::imshow("Texture", texture);

	if (saveTexture)
		cv::imwrite(modelPath + "texture.png", texture);
}

void displayNormalMap(cv::Mat normals, std::string modelPath, bool saveNormalMap) {
	cv::Mat normalMap;
	cv::cvtColor(normals, normalMap, CV_BGR2RGB);
	cv::namedWindow("Normal Map", cv::WINDOW_NORMAL);
	cv::imshow("Normal Map", normalMap);

	if (saveNormalMap)
		cv::imwrite(modelPath + "normalMap.png", normalMap);
}

void displayHeightMap(cv::Mat Z, std::string modelPath, bool saveHeightMap) {
	cv::Mat white(Z.size(), CV_8U, cv::Scalar(255, 255, 255));
	cv::Mat zGlobalHeights = scaleImageIntensity(Z);
	zGlobalHeights.convertTo(zGlobalHeights, CV_8U);
	zGlobalHeights = white - zGlobalHeights;

	cv::namedWindow("Global Heights", cv::WINDOW_NORMAL);
	cv::imshow("Global Heights", zGlobalHeights);

	if (saveHeightMap)
		cv::imwrite(modelPath + "globalHeights.png", zGlobalHeights);
}

cv::Mat loadCalibrationMask(std::string calibrationPath, int& originalHeight, int& originalWidth) {
	// Open the calibration chrome sphere mask in /images/chrome/chrome.mask.png
	cv::Mat calibrationMask = cv::imread(calibrationPath + "mask.png", cv::IMREAD_GRAYSCALE);
	if (!calibrationMask.data) {  // Check if the calibration mask has been read. If not, the program exits.
		std::cout << "The calibration mask could not be found." << std::endl;
		calibrationMask = cv::Mat(originalHeight, originalWidth, CV_8UC1, cv::Scalar(255));
	}
	else {
		std::cout << "The calibration mask has been loaded from " << calibrationPath << "mask.png" << std::endl;
		originalHeight = calibrationMask.rows;
		originalWidth = calibrationMask.cols;
	}
	return calibrationMask;
}

cv::Mat loadModelMask(std::string modelPath, int imageScale, cv::Mat calibrationMask, int originalHeight, int originalWidth) {
	// Open the model mask in /images/imageName/imageName.mask.png
	cv::Mat modelMask = cv::imread(modelPath + "mask.png", cv::IMREAD_GRAYSCALE);
	if (!modelMask.data) {  // Check if the model mask has been loaded. If not, no mask will be used.
		std::cout << "The model mask could not be found. No mask will be used." << std::endl << std::endl;
		modelMask = cv::Mat(originalHeight, originalWidth, CV_8UC1, cv::Scalar(255));
	}
	else {
		std::cout << "The model mask has been loaded from " << modelPath << "mask.png" << std::endl << std::endl;
		// Resize the image
		if (imageScale != 1) {
			cv::resize(modelMask, modelMask, cv::Size(), 1.0 / imageScale, 1.0 / imageScale, cv::INTER_NEAREST);
		}
	}
	return modelMask;
}

cv::Mat convertImageToNormals(cv::Mat normalMap) {
	/*
    cv::Mat result = cv::Mat::zeros(normalMap.rows, normalMap.cols, CV_16SC3);
	cv::Mat subtract = cv::Mat(normalMap.rows, normalMap.cols, CV_8UC3, cv::Scalar(127, 127, 127));
	cv::Mat difference = normalMap - subtract;
	//result = cv::Mat::mul(difference, 2);
	result = difference*2;
	*/
	
    cv::Mat result = cv::Mat::zeros(normalMap.rows, normalMap.cols, CV_32FC3);
    for (int x = 0; x < normalMap.cols; x++) {
		for (int y = 0; y < normalMap.rows; y++) {
			int xi = normalMap.at<cv::Vec3b>(y, x)[0];
			int yi = normalMap.at<cv::Vec3b>(y, x)[1];
			int zi = normalMap.at<cv::Vec3b>(y, x)[2];

			float nxi = (xi - 128.0) / 256.0;
			float nyi = (yi - 128.0) / 256.0;
			//float nzinyi = (zi - 128.0) / 256.0;
			float nzi = sqrt(1.0f - pow(nxi, 2.0f) - pow(nyi, 2.0f));

			result.at<cv::Vec3f>(y, x)[0] = nxi;
			result.at<cv::Vec3f>(y, x)[1] = nyi;
			result.at<cv::Vec3f>(y, x)[2] = nzi;
		}
    }
    return result;
}

/*cv::Mat addNoise(cv::Mat temp, double standardDeviation) {
	if (standardDeviation <= 0)
		return temp;
	cv::Mat noise  = cv::Mat(temp.rows, temp.cols, CV_64F);
	cv::Mat result = cv::Mat(temp.rows, temp.cols, CV_64F);
	cv::cvtColor(temp, temp, cv::COLOR_BGR2GRAY);
	temp.convertTo(temp, CV_64F);
    normalize(temp, result, 0.0, 1.0, CV_MINMAX, CV_64F);
    randn(noise, 0, standardDeviation);
    result += noise;
    normalize(result, result, 0.0, 1.0, CV_MINMAX, CV_64F);
    result.convertTo(result, CV_8U, 255, 0);
	cv::cvtColor(result, result, cv::COLOR_GRAY2BGR);
	return result;
}*/

cv::Mat addNoise(cv::Mat temp, double standardDeviation) {
	if (standardDeviation <= 0)
		return temp;
	cv::Mat noise  = cv::Mat(temp.rows, temp.cols, CV_64F);
	cv::Mat result = cv::Mat(temp.rows, temp.cols, CV_64F);
	cv::cvtColor(temp, temp, cv::COLOR_BGR2GRAY);
	temp.convertTo(temp, CV_64F);
    normalize(temp, result, 0.0, 1.0, CV_MINMAX, CV_64F);
    randn(noise, 0, standardDeviation);
    result += noise;
    normalize(result, result, 0.0, 1.0, CV_MINMAX, CV_64F);
    result.convertTo(result, CV_8U, 255, 0);
	cv::cvtColor(result, result, cv::COLOR_GRAY2BGR);
	return result;
}

// http://opencvexamples.blogspot.com/
cv::Mat addNoise(cv::Mat image) {
    rand();
    cv::RNG rng(rand());
    int i,j;
    for (int n = 0; n < 100; n++) {
        i = rand() % image.cols;
        j = rand() % image.rows;
 
        cv::Point center(i, j);
        circle(image, center, rng.uniform(1, 3),     // radius,
            cv::Scalar(rng.uniform(0, 256),rng.uniform(0, 256),rng.uniform(0, 256)),  // color of noise points
            -2,                  // thickness, negative for filled
            16);                
    }

	return image;
}

// https://stackoverflow.com/questions/32005094/how-to-add-noise-to-color-image-opencv
cv::Mat AddGaussianNoise(const cv::Mat mSrc, double Mean=0.0, double StdDev=10.0)
{
	cv::Mat mDst;
    if(mSrc.empty())
    {
        std::cout<<"[Error]! Input Image Empty!";
        return mSrc;
    }

    cv::Mat mGaussian_noise = cv::Mat(mSrc.size(), CV_16SC3);
    cv::randn(mGaussian_noise, cv::Scalar::all(Mean), cv::Scalar::all(StdDev));

    for (int Rows = 0; Rows < mSrc.rows; Rows++)
    {
        for (int Cols = 0; Cols < mSrc.cols; Cols++)
        {
            cv::Vec3b Source_Pixel = mSrc.at<cv::Vec3b>(Rows,Cols);
            cv::Vec3b &Des_Pixel   = mDst.at<cv::Vec3b>(Rows,Cols);
            cv::Vec3s Noise_Pixel  = mGaussian_noise.at<cv::Vec3s>(Rows,Cols);

            for (int i = 0; i < 3; i++)
            {
                int Dest_Pixel = Source_Pixel.val[i] + Noise_Pixel.val[i];
                Des_Pixel.val[i] = Clamp(Dest_Pixel);
            }
        }
    }

    return mDst;
}

cv::Mat AddGaussianNoise_Opencv(const cv::Mat mSrc, double Mean=0.0, double StdDev=10.0)
{
	cv::Mat mDst;
    if(mSrc.empty())
    {
        std::cout<<"[Error]! Input Image Empty!";
        return mSrc;
    }
    cv::Mat mSrc_16SC;
    cv::Mat mGaussian_noise = cv::Mat(mSrc.size(),CV_16SC3);
    cv::randn(mGaussian_noise, cv::Scalar::all(Mean), cv::Scalar::all(StdDev));

    mSrc.convertTo(mSrc_16SC,CV_16SC3);
    cv::addWeighted(mSrc_16SC, 1.0, mGaussian_noise, 1.0, 0.0, mSrc_16SC);
    mSrc_16SC.convertTo(mDst, mSrc.type());

    return mDst;
}

// Quality Metric https://gist.github.com/Bibimaw/8873663
// sigma on block_size
	double sigma(cv::Mat& m, int i, int j, int block_size)
	{
		double sd = 0;

		cv::Mat m_tmp = m(cv::Range(i, i + block_size), cv::Range(j, j + block_size));
		cv::Mat m_squared(block_size, block_size, CV_64F);

		multiply(m_tmp, m_tmp, m_squared);

		// E(x)
		double avg = mean(m_tmp)[0];
		// E(x²)
		double avg_2 = mean(m_squared)[0];


		sd = sqrt(avg_2 - avg * avg);

		return sd;
	}

	// Covariance
	double cov(cv::Mat& m1, cv::Mat& m2, int i, int j, int block_size)
	{
		cv::Mat m3 = cv::Mat::zeros(block_size, block_size, m1.depth());
		cv::Mat m1_tmp = m1(cv::Range(i, i + block_size), cv::Range(j, j + block_size));
		cv::Mat m2_tmp = m2(cv::Range(i, i + block_size), cv::Range(j, j + block_size));


		multiply(m1_tmp, m2_tmp, m3);

		double avg_ro 	= mean(m3)[0]; // E(XY)
		double avg_r 	= mean(m1_tmp)[0]; // E(X)
		double avg_o 	= mean(m2_tmp)[0]; // E(Y)


		double sd_ro = avg_ro - avg_o * avg_r; // E(XY) - E(X)E(Y)

		return sd_ro;
	}

	// Mean squared error
	double eqm(cv::Mat& img1, cv::Mat& img2)
	{
		int i, j;
		double eqm = 0;
		int height = img1.rows;
		int width = img1.cols;

		for (i = 0; i < height; i++)
			for (j = 0; j < width; j++)
				eqm += (img1.at<double>(i, j) - img2.at<double>(i, j)) * (img1.at<double>(i, j) - img2.at<double>(i, j));

		eqm /= height * width;

		return eqm;
	}



	/**
	 *	Compute the PSNR between 2 images
	 */
	double psnr(cv::Mat& img_src, cv::Mat& img_compressed, int block_size)
	{
		int D = 255;
		return (10 * log10((D*D)/eqm(img_src, img_compressed)));
	}


	/**
	 * Compute the SSIM between 2 images
	 */
	double ssim(cv::Mat& img_src, cv::Mat& img_compressed, int block_size, bool show_progress = false)
	{
		float C1 = 0.01 * 255 * 0.01  * 255, C2 = 0.03 * 255 * 0.03  * 255;

		double ssim = 0;

		int nbBlockPerHeight 	= img_src.rows / block_size;
		int nbBlockPerWidth 	= img_src.cols / block_size;

		for (int k = 0; k < nbBlockPerHeight; k++)
		{
			for (int l = 0; l < nbBlockPerWidth; l++)
			{
				int m = k * block_size;
				int n = l * block_size;

				double avg_o 	= mean(img_src(cv::Range(k, k + block_size), cv::Range(l, l + block_size)))[0];
				double avg_r 	= mean(img_compressed(cv::Range(k, k + block_size), cv::Range(l, l + block_size)))[0];
				double sigma_o 	= sigma(img_src, m, n, block_size);
				double sigma_r 	= sigma(img_compressed, m, n, block_size);
				double sigma_ro	= cov(img_src, img_compressed, m, n, block_size);

				ssim += ((2 * avg_o * avg_r + C1) * (2 * sigma_ro + C2)) / ((avg_o * avg_o + avg_r * avg_r + C1) * (sigma_o * sigma_o + sigma_r * sigma_r + C2));
				
			}
			// Progress
			if (show_progress)
				std::cout << "\r>>SSIM [" << (int) ((( (double)k) / nbBlockPerHeight) * 100) << "%]";
		}
		ssim /= nbBlockPerHeight * nbBlockPerWidth;

		if (show_progress)
		{
			std::cout << "\r>>SSIM [100%]" << std::endl;
			std::cout << "SSIM : " << ssim << std::endl;
		}

		return ssim;
	}

	void compute_quality_metrics(char * file1, char * file2, int block_size)
	{

		cv::Mat img_src;
		cv::Mat img_compressed;

		// Loading pictures
		img_src = cv::imread(file1, cv::IMREAD_GRAYSCALE);
		img_compressed = cv::imread(file2, cv::IMREAD_GRAYSCALE);


		img_src.convertTo(img_src, CV_64F);
		img_compressed.convertTo(img_compressed, CV_64F);

		int height_o = img_src.rows;
		int height_r = img_compressed.rows;
		int width_o = img_src.cols;
		int width_r = img_compressed.cols;

		// Check pictures size
		if (height_o != height_r || width_o != width_r)
		{
			std::cout << "Images must have the same dimensions" << std::endl;
			return;
		}

		// Check if the block size is a multiple of height / width
		if (height_o % block_size != 0 || width_o % block_size != 0)
		{
			std::cout 	<< "WARNING : Image WIDTH and HEIGHT should be divisible by BLOCK_SIZE for the maximum accuracy" << std::endl
					<< "HEIGHT : " 		<< height_o 	<< std::endl
					<< "WIDTH : " 		<< width_o	<< std::endl
					<< "BLOCK_SIZE : " 	<< block_size 	<< std::endl
					<< std::endl;
		}

		double ssim_val = ssim(img_src, img_compressed, block_size);
		double psnr_val = psnr(img_src, img_compressed, block_size);

		std::cout << "SSIM : " << ssim_val << std::endl;
		std::cout << "PSNR : " << psnr_val << std::endl;
	}

double getMSSIM(const cv::Mat& i1, const cv::Mat& i2)
{
    const double C1 = 6.5025, C2 = 58.5225;
    /***************************** INITS **********************************/
    int d     = CV_32F;

    cv::Mat I1, I2;
    i1.convertTo(I1, d);           // cannot calculate on one byte large values
    i2.convertTo(I2, d);

    cv::Mat I2_2   = I2.mul(I2);        // I2^2
    cv::Mat I1_2   = I1.mul(I1);        // I1^2
    cv::Mat I1_I2  = I1.mul(I2);        // I1 * I2

    /*************************** END INITS **********************************/

    cv::Mat mu1, mu2;   // PRELIMINARY COMPUTING
    GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);
    GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);

    cv::Mat mu1_2   =   mu1.mul(mu1);
    cv::Mat mu2_2   =   mu2.mul(mu2);
    cv::Mat mu1_mu2 =   mu1.mul(mu2);

    cv::Mat sigma1_2, sigma2_2, sigma12;

    GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);
    sigma1_2 -= mu1_2;

    GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);
    sigma2_2 -= mu2_2;

    GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);
    sigma12 -= mu1_mu2;

    ///////////////////////////////// FORMULA ////////////////////////////////
    cv::Mat t1, t2, t3;

    t1 = 2 * mu1_mu2 + C1;
    t2 = 2 * sigma12 + C2;
    t3 = t1.mul(t2);              // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))

    t1 = mu1_2 + mu2_2 + C1;
    t2 = sigma1_2 + sigma2_2 + C2;
    t1 = t1.mul(t2);               // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))

    cv::Mat ssim_map;
    divide(t3, t1, ssim_map);      // ssim_map =  t3./t1;

    cv::Scalar mssim = mean( ssim_map ); // mssim = average of ssim map
	double ssim = mssim[0] + mssim[1] + mssim[2];
    return ssim;
}

/*cv::Mat convertImageToNormals(cv::Mat normalMap) {
    cv::Mat result = cv::Mat::zeros(normalMap.rows, normalMap.cols, CV_64FC3);

    for (int x = 0; x < normalMap.cols; x++) {
		for (int y = 0; y < normalMap.rows; y++) {
			float nxf = normalMap.at<cv::Vec3f>(y, x)[0];
			float nyf = normalMap.at<cv::Vec3f>(y, x)[1];
			float nzf = normalMap.at<cv::Vec3f>(y, x)[2];

			double rootsquareSum = sqrt((0.25 * nxf * nxf) + (0.25 * nyf * nyf) + (0.25 * nzf * nzf));

			double nx = 2*nxf*rootsquareSum - 1;
			double ny = 2*nyf*rootsquareSum - 1;
			double nz = 2*nzf*rootsquareSum - 1;

			result.at<cv::Vec3f>(y, x)[0] = nx;
			result.at<cv::Vec3f>(y, x)[1] = ny;
			result.at<cv::Vec3f>(y, x)[2] = nz;
		}
    }
    return result;
}*/

/*cv::Mat convertImageToNormals(cv::Mat normalMap) {
    cv::Mat result = cv::Mat::zeros(3, normalMap.rows*normalMap.cols, CV_64F);

	normalMap = normalMap.reshape(3, 1);

    for (int i = 0; i < normalMap.rows*normalMap.cols; i++) {
		float nxf = normalMap.at<cv::Vec3f>(i)[0];
		float nyf = normalMap.at<cv::Vec3f>(i)[1];
		float nzf = normalMap.at<cv::Vec3f>(i)[2];

		double rootsquareSum = sqrt((0.25 * nxf * nxf) + (0.25 * nyf * nyf) + (0.25 * nzf * nzf));

		double nx = 2*nxf*rootsquareSum - 1;
		double ny = 2*nyf*rootsquareSum - 1;
		double nz = 2*nzf*rootsquareSum - 1;

		result.at<double>(0, i) = nx;
		result.at<double>(1, i) = ny;
		result.at<double>(2, i) = nz;
    }
    return result;
}*/

#endif
