#ifndef PCVASSIGNMENT1_PHOTOMETRICSTEREO_H_
#define PCVASSIGNMENT1_PHOTOMETRICSTEREO_H_

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "PFMAccess.h"

namespace phoSte {
struct circle {
    circle(double x = 0, double y = 0, double r = 0): mCenterX(x), mCenterY(y), mR(r) {
    }
    double mCenterX;
    double mCenterY;
    double mR;
};

struct light {
    light(double x, double y, double z): mx(x), my(y), mz(z) {
    }
    double mx;
    double my;
    double mz;
    double mIntensity;
};

class photometricStereo {
  public:
    // fill the imageNames and maskNames
    // use image with equal distance
    //photometricStereo(int n, int startI, int endI, std::string path, std::string metal1Phere1Name, std::string metal2Phere1Name, std::string lambertPhereName, std::string objectName, double discardRatio = 0.1);
    photometricStereo(int imageNumber, std::string modelPath, std::string calibrationPath, std::string charucoPath, std::string macbethPath, std::string imageName, std::string calibration, double discardRatio = 0.1);
    photometricStereo(std::vector<cv::Mat> mp2Images, std::vector<cv::Mat> mp2CalibrationImages, std::vector<cv::Mat> mp2Mask, int imageNumber = 0, double discardRatio = 0.1);
    photometricStereo(int imageNumber, int imageScale, std::vector<cv::Mat> materialImages, std::string calibrationPath, std::string macbethPath, std::string imageName, std::string calibration, double discardRatio = 0.1);
    photometricStereo(const photometricStereo&) = delete;
    photometricStereo& operator = (const photometricStereo&) = delete;
    ~photometricStereo();
    //bool readImage(); // read the images and masks according to the ImageNames
    bool readImage(std::string modelPath, std::string calibrationPath, std::string macbethPath);
    std::vector<cv::Mat> readImages(std::string path, int imageScale);
    void getLightInformation(const int metalIndex, const int lambIndex);
    void getLightInformation(const int metalIndex);
    void getPixelNormAndAlbedo(const int objectIndex);
    void getPixelNormAndAlbedo(const int objectIndex, cv::Mat lightDirections);
    cv::Mat outputNormalImage(int objectIndex);
    cv::Mat outputAlbedoImage(int objectIndex);
    cv::Mat outputNormalWithAlbedo(int objectIndex);
    cv::Mat getHeightMap(int mode, double parameter);
    // below are functions for test
    //
    void outputImage();
    // build one mask with size*size in the middle to test the N;
    void addSmallMaskForObject(int size, int midX, int midY);
  private:
    const int imageNum; // the num of images used to calculate;
    std::vector<std::string> mImageNames;       // the file names of the model images
    std::vector<std::string> mMaskNames;        // the file name of the mask image
    std::vector<std::string> mCalibrationNames; // the file names of the chrome sphere images
    std::vector<std::string> mMacbethNames;     // the file names of the Macbeth images
    std::vector<cv::Mat> mp2Images;
    std::vector<cv::Mat> mp2CalibrationImages;
    std::vector<cv::Mat> mp2MacbethImages;
    std::vector<cv::Mat> mp2Mask;
    // when the PFMACCess destructs, the data will be destroyed
    // so it's very dangerous, it should not be copied
    //std::vector<CPFMAccess*> mImageStorage;
    // tht circle data for the metalSphere
    phoSte::circle m_metalSphere;
    //phoSte::circle m_lambSpere;
    std::vector<phoSte::light> m_light;
    cv::Mat mN;
    cv::Mat mAlbedo;
    std::vector<int> mObjectX;
    std::vector<int> mObjectY;
    std::vector<int> mInvalidIndex;
    const double mDiscardRatio;

    std::string modelPath;
    std::string calibrationPath;
    std::string imageName;
    std::string calibration;
    int width;
    int height;
};
}

#endif
