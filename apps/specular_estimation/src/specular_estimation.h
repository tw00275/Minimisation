#pragma once
#ifndef SPECULAR_ESTIMATION_H
#define SPECULAR_ESTIMATION_H

#include "stdafx.h"

class SpecularEstimation {
	public:
		SpecularEstimation(std::string imageName, std::string calibration, bool denseSample, double Roughness, double Metallic);
		SpecularEstimation(std::string imageName, std::string calibration, bool denseSample, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic);
		//SpecularEstimation(std::string imageName, std::string calibration, bool denseSample, double Roughness, double Metallic, double RoughnessSynthetic, double MetallicSynthetic, double LightDistance, double LightIntensity);
		~SpecularEstimation();

		void loadTextureImages();
		void PhotometricStereo();
		void populateLightInvDirs();
		void DenseSample();

	private:
		// OpenCV
		std::string imageName, modelPath, calibrationPath, calibration = "chrome", imagesPath = "/home/thomas/Documents/", folderPath = "2018-08-31", macbethPath = imagesPath + folderPath + "/macbeth/macbeth.", albedoPath = modelPath + "albedo.png", normalPath = modelPath + "normal.png", texturePath = modelPath + "texture.png";
		int width = 1092, height = 728, numberOfLights = 6, lightNumber = 0;
		double Roughness, Metallic, RoughnessSynthetic, MetallicSynthetic, LightDistance = 0.5, LightIntensity = 0.5, totalResidual, residualValue;
		bool calculateResidual = false, perspectiveProjection = false, shadowControl = false, denseSample = false;
		std::vector<unsigned int> indices;

		// ChArUco board
		int dictionaryId = 0, xSquares = 9, ySquares = 11;
		cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionaryId));
		float squareSideLength = 0.04f, markerSideLength = 0.02f;
		cv::Ptr<cv::aruco::CharucoBoard> charucoBoard = cv::aruco::CharucoBoard::create(xSquares, ySquares, squareSideLength, markerSideLength, dictionary);
		cv::Mat charucoBoardImage = drawCharuco(charucoBoard, height, ((height * xSquares) / ySquares), false, false);

		cv::Mat residualImage, albedo, normalMap, heightMap, normalAlbedo, lightDirections = (cv::Mat_<float>(6,3) << 0.447712, 0.138562, 0.883377, 0.228758, -0.106536, 0.967636, 0.1, 0.0705882, 0.99248, 0.000653595, -0.0718954, 0.997412, -0.139216, -0.12549, 0.982279, -0.494771, 0.115033, 0.861376);
		cv::Vec3d residual;
		std::vector<cv::Mat> textureImages;
		std::vector<glm::vec3> lightInvDirs;

		// OpenGL
		GLuint programID, MatrixID, ModelMatrixID, ViewMatrixID, DepthBiasID, lightInvDirID, Texture, TextureID, depthTexture, ShadowMapID, vertexbuffer, uvbuffer, normalbuffer, elementbuffer, depthProgramID, quad_programID, FramebufferName, quad_vertexbuffer, VertexArrayID, RoughnessID, MetallicID, LightDistanceID, LightIntensityID;
		glm::vec3 position, lightInvDir;
		glm::mat4 depthProjectionMatrix, depthViewMatrix, depthModelMatrix = glm::mat4(1.0), depthMVP, ModelMatrix = glm::mat4(1.0), MVP, ViewMatrix, depthBiasMVP, ProjectionMatrix, biasMatrix = glm::mat4(0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.5, 0.5, 0.5, 1.0);
		float horizontalAngle, verticalAngle, FoV;
};

#endif