# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/user/HS222/tw00275/PhD/Minimisation/apps/specular_estimation/src/objloader.cpp" "/user/HS222/tw00275/PhD/Minimisation/build/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/objloader.cpp.o"
  "/user/HS222/tw00275/PhD/Minimisation/apps/specular_estimation/src/shader.cpp" "/user/HS222/tw00275/PhD/Minimisation/build/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/shader.cpp.o"
  "/user/HS222/tw00275/PhD/Minimisation/apps/specular_estimation/src/specular_estimation.cc" "/user/HS222/tw00275/PhD/Minimisation/build/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/specular_estimation.cc.o"
  "/user/HS222/tw00275/PhD/Minimisation/apps/specular_estimation/src/texture.cpp" "/user/HS222/tw00275/PhD/Minimisation/build/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/texture.cpp.o"
  "/user/HS222/tw00275/PhD/Minimisation/apps/specular_estimation/src/vboindexer.cpp" "/user/HS222/tw00275/PhD/Minimisation/build/apps/specular_estimation/CMakeFiles/specular_estimation.dir/src/vboindexer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "/usr/include/OpenEXR"
  "/user/HS222/tw00275/opencv-bin/include"
  "/user/HS222/tw00275/opencv-bin/include/opencv"
  "/user/HS222/tw00275/glfw3-bin/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
